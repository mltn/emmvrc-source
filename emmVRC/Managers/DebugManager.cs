using System;
using System.Collections.Generic;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using UnityEngine;

namespace emmVRC.Managers
{
	public class DebugManager : MelonLoaderEvents, IWithFixedUpdate
	{
		public static List<DebugAction> DebugActions = new List<DebugAction>();

		public override void OnUiManagerInit()
		{
			emmVRCLoader.Logger.LogDebug("Initializing debug manager");
			if (Environment.CommandLine.Contains("--emmvrc.debug"))
			{
				Attributes.Debug = true;
			}
		}

		public void OnFixedUpdate()
		{
			if (!Attributes.Debug)
			{
				return;
			}
			foreach (DebugAction debugAction in DebugActions)
			{
				if (Input.GetKeyDown(debugAction.ActionKey))
				{
					debugAction.ActionAction();
				}
			}
		}
	}
}
