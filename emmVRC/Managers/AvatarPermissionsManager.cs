using System;
using System.Collections.Generic;
using System.IO;
using emmVRC.Objects;
using emmVRC.TinyJSON;

namespace emmVRC.Managers
{
	public class AvatarPermissionsManager
	{
		public static readonly string AvatarPermissionsFolder = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions");

		private static readonly Dictionary<string, CachedValue<AvatarPermissions>> cache = new Dictionary<string, CachedValue<AvatarPermissions>>();

		public static bool TryGetAvatarPermissions(string avatarId, out AvatarPermissions permissions)
		{
			if (cache.TryGetValue(avatarId, out var value))
			{
				if (value.Validate())
				{
					permissions = value.value;
					return true;
				}
				cache.Remove(avatarId);
			}
			if (!Directory.Exists(AvatarPermissionsFolder))
			{
				Directory.CreateDirectory(AvatarPermissionsFolder);
			}
			if (!File.Exists(Path.Combine(AvatarPermissionsFolder, avatarId)))
			{
				permissions = null;
				return false;
			}
			string text = File.ReadAllText(Path.Combine(AvatarPermissionsFolder, avatarId));
			if (text.Contains("emmVRC.Managers.AvatarPermissions"))
			{
				text.Replace("emmVRC.Managers.AvatarPermissions", "emmVRC.Objects.AvatarPermissions");
				File.WriteAllText(Path.Combine(AvatarPermissionsFolder, avatarId), text);
			}
			permissions = Decoder.Decode(text).Make<AvatarPermissions>();
			cache.Add(avatarId, permissions);
			return true;
		}

		public static void SaveAvatarPermissions(AvatarPermissions avatarPermission)
		{
			if (!Directory.Exists(AvatarPermissionsFolder))
			{
				Directory.CreateDirectory(AvatarPermissionsFolder);
			}
			if (!string.IsNullOrEmpty(avatarPermission.AvatarId))
			{
				if (cache.ContainsKey(avatarPermission.AvatarId))
				{
					cache[avatarPermission.AvatarId] = avatarPermission;
				}
				else
				{
					cache.Add(avatarPermission.AvatarId, avatarPermission);
				}
				File.WriteAllText(Path.Combine(AvatarPermissionsFolder, avatarPermission.AvatarId), Encoder.Encode(avatarPermission, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
			}
		}
	}
}
