using System;
using System.Collections.Generic;
using System.Linq;
using emmVRC.Components;
using emmVRC.Functions.Core;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using Il2CppSystem.Collections;
using UnhollowerBaseLib;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Managers.VRChat
{
	public class NotificationDotManager : MelonLoaderEvents
	{
		private static readonly List<GameObject> vanillaIcons = new List<GameObject>();

		private static GameObject emmVRCIcon;

		private static bool _initialized = false;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			GameObject gameObject = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/NotificationDotParent");
			emmVRCIcon = null;
			IEnumerator enumerator = gameObject.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					GameObject gameObject2 = ((Il2CppObjectBase)(object)enumerator.get_Current()).Cast<Transform>().gameObject;
					vanillaIcons.Add(gameObject2);
					EnableDisableListener enableDisableListener = gameObject2.AddComponent<EnableDisableListener>();
					enableDisableListener.OnEnabled += delegate
					{
						emmVRCIcon.SetActive(value: false);
					};
					enableDisableListener.OnDisabled += delegate
					{
						emmVRCIcon.SetActive(emmVRCNotificationsManager.Notifications.Count > 0);
					};
				}
			}
			finally
			{
				(enumerator as IDisposable)?.Dispose();
			}
			emmVRCIcon = UnityEngine.Object.Instantiate(gameObject.transform.GetChild(0).gameObject, gameObject.transform);
			emmVRCIcon.name = "emmVRCIcon";
			Image emmVRCIconImage = emmVRCIcon.GetComponent<Image>();
			emmVRCNotificationsManager.OnNotificationAdded += delegate(Notification notification)
			{
				emmVRCIconImage.sprite = notification.icon;
				emmVRCIconImage.gameObject.SetActive(value: true);
			};
			emmVRCNotificationsManager.OnNotificationRemoved += delegate
			{
				emmVRCLoader.Logger.LogDebug("OnNotificationRemoved called");
				emmVRCLoader.Logger.LogDebug("Boolean logic is " + (emmVRCNotificationsManager.Notifications.Count > 0 && emmVRCNotificationsManager.Notifications.LastOrDefault() != null && emmVRCNotificationsManager.Notifications.LastOrDefault().icon != null));
				if (emmVRCNotificationsManager.Notifications.Count > 0 && emmVRCNotificationsManager.Notifications.LastOrDefault() != null)
				{
					emmVRCIconImage.sprite = ((emmVRCNotificationsManager.Notifications.LastOrDefault().icon != null) ? emmVRCNotificationsManager.Notifications.LastOrDefault().icon : global::emmVRC.Functions.Core.Resources.alertSprite);
				}
				else
				{
					emmVRCIconImage.gameObject.SetActive(value: false);
				}
			};
			_initialized = true;
		}
	}
}
