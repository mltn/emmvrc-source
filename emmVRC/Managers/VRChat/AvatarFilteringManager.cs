using System;
using System.Diagnostics;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem.Collections;
using UnhollowerBaseLib;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Managers.VRChat
{
	public class AvatarFilteringManager : MelonLoaderEvents
	{
		public static Action<MonoBehaviour> CheckTransformAction;

		private static readonly Shader diffuse = Shader.Find("Diffuse");

		public static readonly Action<Transform> CheckAudioSource = delegate(Transform parent)
		{
			foreach (AudioSource componentsInChild in parent.GetComponentsInChildren<AudioSource>())
			{
				if (componentsInChild != null)
				{
					UnityEngine.Object.DestroyImmediate(componentsInChild);
				}
			}
		};

		public static readonly Action<Transform> CheckCloth = delegate(Transform parent)
		{
			foreach (Cloth componentsInChild2 in parent.GetComponentsInChildren<Cloth>())
			{
				if (componentsInChild2 != null)
				{
					UnityEngine.Object.DestroyImmediate(componentsInChild2);
				}
			}
		};

		public static readonly Action<Transform> CheckDynamicBone = delegate(Transform parent)
		{
			foreach (DynamicBone componentsInChild3 in parent.GetComponentsInChildren<DynamicBone>())
			{
				if (componentsInChild3 != null)
				{
					UnityEngine.Object.DestroyImmediate(componentsInChild3);
				}
			}
			foreach (DynamicBoneCollider componentsInChild4 in parent.GetComponentsInChildren<DynamicBoneCollider>())
			{
				if (componentsInChild4 != null)
				{
					UnityEngine.Object.DestroyImmediate(componentsInChild4);
				}
			}
		};

		public static readonly Action<Transform> CheckParticleSystem = delegate(Transform parent)
		{
			foreach (ParticleSystem componentsInChild5 in parent.GetComponentsInChildren<ParticleSystem>())
			{
				if (componentsInChild5 != null)
				{
					componentsInChild5.maxParticles = 0;
				}
			}
		};

		public static readonly Action<Transform> CheckShader = delegate(Transform parent)
		{
			foreach (Renderer componentsInChild6 in parent.GetComponentsInChildren<Renderer>())
			{
				if (componentsInChild6 != null)
				{
					foreach (Material material in componentsInChild6.materials)
					{
						material.shader = diffuse;
					}
				}
			}
		};

		public override void OnApplicationStart()
		{
			NetworkEvents.OnAvatarInstantiated += OnAvatarInstantiated;
		}

		public static void OnAvatarInstantiated(VRCPlayer player, ApiAvatar avatar, GameObject gameObject)
		{
			Stopwatch stopwatch = Stopwatch.StartNew();
			if (!AvatarPermissionsManager.TryGetAvatarPermissions(avatar.id, out var permissions))
			{
				return;
			}
			Action<Transform> action = null;
			if (!permissions.AudioSourcesEnabled)
			{
				action = (Action<Transform>)Delegate.Combine(action, CheckAudioSource);
			}
			if (!permissions.ClothEnabled)
			{
				action = (Action<Transform>)Delegate.Combine(action, CheckCloth);
			}
			if (!permissions.DynamicBonesEnabled)
			{
				action = (Action<Transform>)Delegate.Combine(action, CheckDynamicBone);
			}
			if (!permissions.ParticleSystemsEnabled)
			{
				action = (Action<Transform>)Delegate.Combine(action, CheckParticleSystem);
			}
			if (!permissions.ShadersEnabled)
			{
				action = (Action<Transform>)Delegate.Combine(action, CheckShader);
			}
			if (action == null)
			{
				return;
			}
			IEnumerator enumerator = gameObject.transform.parent.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					Transform transform = ((Il2CppObjectBase)(object)enumerator.get_Current()).Cast<Transform>();
					if (transform.name.Contains("Avatar"))
					{
						action(transform);
					}
				}
			}
			finally
			{
				(enumerator as IDisposable)?.Dispose();
			}
			emmVRCLoader.Logger.LogDebug($"Filtering avatar took {(float)stopwatch.ElapsedTicks / 10000f:n3}ms");
		}
	}
}
