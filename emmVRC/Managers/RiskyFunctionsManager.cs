using System;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Managers
{
	public class RiskyFunctionsManager : MelonLoaderEvents
	{
		private static bool _areRiskyFunctionsAllowed;

		public static bool AreRiskyFunctionsAllowed
		{
			get
			{
				return _areRiskyFunctionsAllowed;
			}
			private set
			{
				_areRiskyFunctionsAllowed = value;
				RiskyFunctionsManager.OnRiskyFunctionCheckCompleted?.Invoke(value);
			}
		}

		public static event Action<bool> RiskyFuncsProcessed;

		public static event Action<bool> OnRiskyFunctionCheckCompleted;

		public override void OnApplicationStart()
		{
			NetworkEvents.OnInstanceChanged += OnInstanceChange;
		}

		private static void OnInstanceChange(ApiWorld world, ApiWorldInstance instance)
		{
			AreRiskyFunctionsAllowed = false;
			UnityWebRequestUtils.Get("https://dl.emmvrc.com/riskyfuncs.php?worldid=" + world.id, delegate(string result)
			{
				OnFinish(result, world);
			});
		}

		private static void OnFinish(string result, ApiWorld world)
		{
			if (!string.IsNullOrWhiteSpace(result))
			{
				if (result == "allowed")
				{
					AreRiskyFunctionsAllowed = true;
					RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
					return;
				}
				if (result == "denied")
				{
					AreRiskyFunctionsAllowed = false;
					RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
					return;
				}
			}
			if (GameObject.Find("eVRCRiskFuncEnable") != null || GameObject.Find("UniversalRiskyFuncEnable") != null)
			{
				AreRiskyFunctionsAllowed = true;
				RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
				return;
			}
			if (GameObject.Find("eVRCRiskFuncDisable") != null || GameObject.Find("UniversalRiskyFuncDisable") != null)
			{
				AreRiskyFunctionsAllowed = false;
				RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
				return;
			}
			if ((GameObject.Find("eVRCRiskFuncEnable") != null || GameObject.Find("eVRCRiskFuncDisable") != null) && RoomManager.field_Internal_Static_ApiWorld_0.authorId == APIUser.CurrentUser.id && !Configuration.JSONConfig.IgnoreWorldCreatorTips)
			{
				emmVRCLoader.Logger.Log("[NOTICE] The eVRCRiskFuncDisable/Enable objects are soon to be deprecated. Instead, please use \"UniversalRiskyFuncDisable\" and \"UniversalRiskyFuncEnable\"");
			}
			Enumerator<string> enumerator = world.tags.GetEnumerator();
			while (enumerator.MoveNext())
			{
				string current = enumerator.get_Current();
				if (current.ToLower().Contains("game") || current.ToLower().Contains("club"))
				{
					AreRiskyFunctionsAllowed = false;
					RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
					return;
				}
			}
			AreRiskyFunctionsAllowed = true;
			RiskyFunctionsManager.RiskyFuncsProcessed?.DelegateSafeInvoke(AreRiskyFunctionsAllowed);
		}
	}
}
