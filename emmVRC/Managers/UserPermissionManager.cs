using System;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using Il2CppSystem.IO;

namespace emmVRC.Managers
{
	[Priority(50)]
	public class UserPermissionManager : MelonLoaderEvents
	{
		public static UserPermissions selectedUserPermissions;

		public override void OnUiManagerInit()
		{
			if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/")))
			{
				Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/"));
			}
		}

		private static void ReloadUsers()
		{
			VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
		}
	}
}
