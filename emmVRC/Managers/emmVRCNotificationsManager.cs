using System;
using System.Collections.Generic;
using emmVRC.Objects;

namespace emmVRC.Managers
{
	public static class emmVRCNotificationsManager
	{
		private static readonly List<Notification> _notifications = new List<Notification>();

		public static IReadOnlyList<Notification> Notifications => _notifications;

		public static int RecentNotificationCount { get; set; }

		public static bool HasRecentNotifications => RecentNotificationCount > 0;

		public static event Action<Notification> OnNotificationAdded;

		public static event Action<Notification> OnNotificationRemoved;

		public static void AddNotification(Notification notification)
		{
			_notifications.Insert(0, notification);
			RecentNotificationCount++;
			emmVRCNotificationsManager.OnNotificationAdded?.Invoke(notification);
		}

		public static void RemoveNotification(Notification notification)
		{
			_notifications.Remove(notification);
			if (!notification.canIgnore)
			{
				RecentNotificationCount--;
			}
			emmVRCNotificationsManager.OnNotificationRemoved?.Invoke(notification);
		}

		public static void RemoveNotificationAt(int index)
		{
			Notification obj = _notifications[index];
			_notifications.RemoveAt(index);
			emmVRCNotificationsManager.OnNotificationRemoved?.Invoke(obj);
		}
	}
}
