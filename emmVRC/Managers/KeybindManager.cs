using emmVRC.Functions.PlayerHacks;
using emmVRC.Functions.View;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Managers
{
	public class KeybindManager : MelonLoaderEvents, IWithUpdate
	{
		private static bool keyFlag;

		public void OnUpdate()
		{
			if (!Configuration.JSONConfig.EnableKeybinds)
			{
				return;
			}
			if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
			{
				if ((Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[1]) || Configuration.JSONConfig.FlightKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[0]) && !keyFlag)
				{
					if (Flight.IsNoClipEnabled && Flight.IsFlyEnabled)
					{
						Flight.SetNoClipActive(active: false);
					}
					Flight.SetFlyActive(!Flight.IsFlyEnabled);
					keyFlag = true;
				}
				if ((Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[1]) || Configuration.JSONConfig.NoclipKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[0]) && !keyFlag)
				{
					if (!Flight.IsFlyEnabled && !Flight.IsNoClipEnabled)
					{
						Flight.SetFlyActive(active: true);
					}
					Flight.SetNoClipActive(!Flight.IsNoClipEnabled);
					keyFlag = true;
				}
				if ((Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[1]) || Configuration.JSONConfig.SpeedKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[0]) && !keyFlag)
				{
					Speed.SetActive(!Speed.IsEnabled);
					keyFlag = true;
				}
			}
			if ((Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[1]) || Configuration.JSONConfig.ThirdPersonKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[0]) && !keyFlag)
			{
				if (ThirdPerson.CameraSetup != 2)
				{
					ThirdPerson.CameraSetup++;
					ThirdPerson.TPCameraBack.transform.position -= ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
					ThirdPerson.TPCameraFront.transform.position += ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
					ThirdPerson.zoomOffset = 0f;
					ThirdPerson.ChangeCameraView();
					keyFlag = true;
				}
				else
				{
					ThirdPerson.CameraSetup = 0;
					ThirdPerson.TPCameraBack.transform.position -= ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
					ThirdPerson.TPCameraFront.transform.position += ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
					ThirdPerson.zoomOffset = 0f;
					ThirdPerson.ChangeCameraView();
					keyFlag = true;
				}
			}
			if ((Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[1]) || Configuration.JSONConfig.GoHomeKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[0]) && !keyFlag)
			{
				ButtonAPI.menuPageBase.transform.Find("ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions").Find("Button_GoHome").GetComponentInChildren<Button>()
					.Press();
				keyFlag = true;
			}
			if ((Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[1]) || Configuration.JSONConfig.RespawnKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[0]) && !keyFlag)
			{
				ButtonAPI.menuPageBase.transform.Find("ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions").Find("Button_Respawn").GetComponentInChildren<Button>()
					.Press();
				keyFlag = true;
			}
			if (!Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[0]) && keyFlag)
			{
				keyFlag = false;
			}
		}
	}
}
