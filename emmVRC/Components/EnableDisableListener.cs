using System;
using UnhollowerBaseLib.Attributes;
using UnityEngine;

namespace emmVRC.Components
{
	public class EnableDisableListener : MonoBehaviour
	{
		[method: HideFromIl2Cpp]
		public event Action OnDisabled;

		[method: HideFromIl2Cpp]
		public event Action OnEnabled;

		public EnableDisableListener(IntPtr obj)
			: base(obj)
		{
		}

		private void OnDisable()
		{
			this.OnDisabled?.Invoke();
		}

		private void OnEnable()
		{
			this.OnEnabled?.Invoke();
		}
	}
}
