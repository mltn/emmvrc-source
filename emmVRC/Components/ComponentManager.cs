using System;
using System.Reflection;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using UnhollowerRuntimeLib;
using UnityEngine;

namespace emmVRC.Components
{
	public class ComponentManager : MelonLoaderEvents
	{
		public override void OnApplicationStart()
		{
			emmVRCLoader.Logger.LogDebug("Registering types in Il2Cpp");
			Type[] types = Assembly.GetExecutingAssembly().GetTypes();
			for (int i = 0; i < types.Length; i++)
			{
				RegisterTypeRecursive(types[i]);
			}
		}

		private static void RegisterTypeRecursive(Type t)
		{
			if (!(t == null) && t.IsSubclassOf(typeof(MonoBehaviour)))
			{
				emmVRCLoader.Logger.LogDebug(t.FullName);
				ClassInjector.RegisterTypeInIl2Cpp(t, logSuccess: false);
			}
		}
	}
}
