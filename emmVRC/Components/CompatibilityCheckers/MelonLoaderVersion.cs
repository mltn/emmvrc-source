using System;
using System.Windows.Forms;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;

namespace emmVRC.Components.CompatibilityCheckers
{
	public class MelonLoaderVersion : CompatibilityCheck
	{
		public override bool RunCheck()
		{
			string text = (string)typeof(MelonLoader.BuildInfo).GetField("Version").GetValue(null);
			if (new Version(text) < Attributes.MinimumMelonLoaderVersion)
			{
				Logger.LogError("You are using an incompatible version of MelonLoader: v" + text + ". Please install v" + Attributes.MinimumMelonLoaderVersion.ToString(3) + " or newer, via the instructions in our Discord under the #how-to channel. emmVRC will not start.");
				MessageBox.Show("You are using an incompatible version of MelonLoader: v" + text + ". Please install v" + Attributes.MinimumMelonLoaderVersion.ToString(3) + " or newer, via the instructions in our Discord under the #how-to channel. emmVRC will not start.", "emmVRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}
	}
}
