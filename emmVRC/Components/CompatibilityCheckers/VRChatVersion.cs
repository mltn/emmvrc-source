using System.Windows.Forms;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Reflection;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Components.CompatibilityCheckers
{
	public class VRChatVersion : CompatibilityCheck
	{
		public static int buildNumber;

		public override bool RunCheck()
		{
			int num = 0;
			foreach (MonoBehaviour component in GameObject.Find("_Application/ApplicationSetup").GetComponents<MonoBehaviour>())
			{
				if (((Il2CppObjectBase)(object)component).TryCast<Transform>() != null || ((Il2CppObjectBase)(object)component).TryCast<ApiCache>() != null)
				{
					continue;
				}
				emmVRCLoader.Logger.LogDebug(((object)component).GetType().Name);
				bool flag = false;
				foreach (FieldInfo field in ((Object)component).GetIl2CppType().GetFields())
				{
					if (!flag && field.get_FieldType() == Il2CppType.Of<int>())
					{
						num = ((Il2CppObjectBase)(object)field.GetValue((Object)(object)component)).Unbox<int>();
						flag = true;
					}
				}
			}
			emmVRCLoader.Logger.Log("VRChat build is: " + num);
			buildNumber = num;
			if (num < 1151)
			{
				emmVRCLoader.Logger.LogError("You are using an older version of VRChat than supported by emmVRC: " + num + ". Please update VRChat through Steam or Oculus to build " + 1151 + ".");
				MessageBox.Show("You are using an older version of VRChat than supported by emmVRC: " + num + ". Please update VRChat through Steam or Oculus to build " + 1151 + ".", "emmVRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}
	}
}
