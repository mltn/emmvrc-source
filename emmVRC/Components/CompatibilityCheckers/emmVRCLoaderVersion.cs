using System;
using System.IO;
using System.Net;
using System.Windows.Forms;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;

namespace emmVRC.Components.CompatibilityCheckers
{
	public class emmVRCLoaderVersion : CompatibilityCheck
	{
		public override bool RunCheck()
		{
			string text = (string)typeof(BuildInfo).GetField("Version").GetValue(null);
			if (new Version(text) < Attributes.MinimumemmVRCLoaderVersion)
			{
				try
				{
					WebClient webClient = new WebClient();
					string text2 = "";
					string[] files = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Mods"));
					foreach (string text3 in files)
					{
						if (text3.Contains("emmVRC"))
						{
							text2 = text3;
							File.Delete(text3);
						}
					}
					webClient.DownloadFile("https://dl.emmvrc.com/downloads/emmVRCLoader.dll", (text2 != "") ? text2 : Path.Combine(Environment.CurrentDirectory, "Mods/emmVRCLoader.dll"));
					MessageBox.Show("The newest emmVRCLoader has been downloaded to your Mods folder. To use emmVRC, restart your game. If the problem persists, remove any current emmVRCLoader files, and download the latest from #loader-updates in the emmVRC Discord.", "emmVRC", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					return false;
				}
				catch (Exception ex)
				{
					Logger.LogError("Attempt to download the new loader failed. You must download the latest from https://dl.emmvrc.com/downloads/emmVRCLoader.dll manually.");
					Logger.LogError("Error: " + ex.ToString());
					MessageBox.Show("You are using an incompatible version of emmVRCLoader: v" + text + ". Please install v" + Attributes.MinimumemmVRCLoaderVersion.ToString(3) + " or greater, from the #loader-updates channel in the emmVRC Discord.", "emmVRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}
			return true;
		}
	}
}
