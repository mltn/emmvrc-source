using System;
using emmVRC.Functions.Core;
using UnityEngine;

namespace emmVRC.Components
{
	public class emmVRCPanel : MonoBehaviour
	{
		public TextMesh activeUsers;

		public TextMesh totalUsers;

		public TextMesh globalChat;

		public bool updateRequested;

		public static bool classInjected;

		public emmVRCPanel(IntPtr ptr)
			: base(ptr)
		{
		}

		public void Start()
		{
			Mesh mesh2 = (base.gameObject.AddComponent<MeshFilter>().mesh = new Mesh());
			Vector3[] array = new Vector3[4]
			{
				Vector3.zero,
				new Vector3(1.65f, 0f, 0f),
				new Vector3(0f, 1f, 0f),
				new Vector3(1.65f, 1f, 0f)
			};
			mesh2.vertices = array;
			int[] array2 = new int[6] { 0, 2, 1, 2, 3, 1 };
			mesh2.triangles = array2;
			Vector3[] array3 = new Vector3[4]
			{
				-Vector3.forward,
				-Vector3.forward,
				-Vector3.forward,
				-Vector3.forward
			};
			mesh2.normals = array3;
			Vector2[] array4 = new Vector2[4]
			{
				new Vector2(0f, 0f),
				new Vector2(1f, 0f),
				new Vector2(0f, 1f),
				new Vector2(1f, 1f)
			};
			mesh2.uv = array4;
			MeshRenderer meshRenderer = base.gameObject.AddComponent<MeshRenderer>();
			meshRenderer.material = new Material(Shader.Find("Standard"));
			meshRenderer.material.SetTexture("_MainTex", global::emmVRC.Functions.Core.Resources.panelTexture);
			GameObject gameObject = UnityEngine.Object.Instantiate(new GameObject(), base.gameObject.transform);
			gameObject.transform.localPosition = new Vector3(0.325f, 0.65f, 0f);
			gameObject.transform.localScale = new Vector3(0.05f, 0.05f, 0.25f);
			activeUsers = gameObject.AddComponent<TextMesh>();
			activeUsers.anchor = TextAnchor.UpperCenter;
			activeUsers.color = Color.black;
			GameObject gameObject2 = UnityEngine.Object.Instantiate(new GameObject(), base.gameObject.transform);
			gameObject2.transform.localPosition = new Vector3(1.3f, 0.65f, 0f);
			gameObject2.transform.localScale = new Vector3(0.05f, 0.05f, 0.25f);
			totalUsers = gameObject2.AddComponent<TextMesh>();
			totalUsers.anchor = TextAnchor.UpperCenter;
			totalUsers.color = Color.black;
			GameObject gameObject3 = UnityEngine.Object.Instantiate(new GameObject(), base.gameObject.transform);
			gameObject3.transform.localPosition = new Vector3(0.0125f, 0.325f, 0f);
			gameObject3.transform.localScale = new Vector3(0.03f, 0.03f, 0.25f);
			globalChat = gameObject3.AddComponent<TextMesh>();
			globalChat.color = Color.black;
		}
	}
}
