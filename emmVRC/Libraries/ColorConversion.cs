using System;
using System.Globalization;
using UnityEngine;

namespace emmVRC.Libraries
{
	public class ColorConversion
	{
		public static Color HexToColor(string hexColor)
		{
			if (hexColor.IndexOf('#') != -1)
			{
				hexColor = hexColor.Replace("#", "");
			}
			float num = 0f;
			float r = (float)int.Parse(hexColor.Substring(0, 2), NumberStyles.AllowHexSpecifier) / 255f;
			float g = (float)int.Parse(hexColor.Substring(2, 2), NumberStyles.AllowHexSpecifier) / 255f;
			num = (float)int.Parse(hexColor.Substring(4, 2), NumberStyles.AllowHexSpecifier) / 255f;
			return new Color(r, g, num);
		}

		public static string ColorToHex(Color baseColor, bool hash = false)
		{
			int num = Convert.ToInt32(baseColor.r * 255f);
			int num2 = Convert.ToInt32(baseColor.g * 255f);
			string text = string.Concat(str2: Convert.ToInt32(baseColor.b * 255f).ToString("X2"), str0: num.ToString("X2"), str1: num2.ToString("X2"));
			if (hash)
			{
				text = "#" + text;
			}
			return text;
		}
	}
}
