using UnityEngine;

namespace emmVRC.Libraries
{
	public class KeyCodeConversion
	{
		public static string Stringify(KeyCode keyCode, KeyCodeStringStyle style = KeyCodeStringStyle.Clean)
		{
			if (style == KeyCodeStringStyle.Unity)
			{
				return keyCode.ToString();
			}
			return keyCode switch
			{
				KeyCode.LeftControl => "LeftCTRL", 
				KeyCode.RightControl => "RightCTRL", 
				KeyCode.LeftCommand => "LeftWin", 
				KeyCode.RightCommand => "RightWin", 
				KeyCode.Alpha0 => "0", 
				KeyCode.Alpha1 => "1", 
				KeyCode.Alpha2 => "2", 
				KeyCode.Alpha3 => "3", 
				KeyCode.Alpha4 => "4", 
				KeyCode.Alpha5 => "5", 
				KeyCode.Alpha6 => "6", 
				KeyCode.Alpha7 => "7", 
				KeyCode.Alpha8 => "8", 
				KeyCode.Alpha9 => "9", 
				_ => keyCode.ToString(), 
			};
		}
	}
}
