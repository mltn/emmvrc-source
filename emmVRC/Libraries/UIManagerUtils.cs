using System;
using System.Linq;
using System.Reflection;
using Il2CppSystem;
using UnhollowerRuntimeLib.XrefScans;

namespace emmVRC.Libraries
{
	public static class UIManagerUtils
	{
		public delegate VRCUiPage ShowScreenAction(VRCUiPage page);

		public delegate VRCUiPage GetPageAction(string page);

		private static ShowScreenAction ourShowScreenAction;

		private static GetPageAction ourGetPageAction;

		public static ShowScreenAction ShowScreenActionAction
		{
			get
			{
				if (ourShowScreenAction != null)
				{
					return ourShowScreenAction;
				}
				MethodInfo method = typeof(VRCUiManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it.ReturnType == typeof(VRCUiPage) && it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(VRCUiPage) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "Screen Not Found - ";
					}
					return false;
				}));
				ourShowScreenAction = (ShowScreenAction)Delegate.CreateDelegate(typeof(ShowScreenAction), VRCUiManager.prop_VRCUiManager_0, method);
				return ourShowScreenAction;
			}
		}

		public static GetPageAction GetPage
		{
			get
			{
				if (ourGetPageAction != null)
				{
					return ourGetPageAction;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it.GetParameters().Length == 1 && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "Screen Not Found - ";
					}
					return false;
				}));
				ourGetPageAction = (GetPageAction)Delegate.CreateDelegate(typeof(GetPageAction), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourGetPageAction;
			}
		}

		public static void QueueHUDMessage(this VRCUiManager manager, string message)
		{
			manager.field_Private_List_1_String_0.Add(message);
		}

		public static VRCUiPage ShowScreen(this VRCUiManager manager, VRCUiPage page)
		{
			return ShowScreenActionAction(page);
		}

		public static void ShowScreen(this VRCUiManager manager, string pageName, bool otherThing)
		{
			VRCUiPage vRCUiPage = GetPage(pageName);
			if (vRCUiPage != null && otherThing)
			{
				manager.ShowScreen(vRCUiPage);
			}
		}
	}
}
