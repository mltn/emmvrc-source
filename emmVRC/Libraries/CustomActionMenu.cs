using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using HarmonyLib;
using Il2CppSystem;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;

namespace emmVRC.Libraries
{
	[Priority(25)]
	public class CustomActionMenu : MelonLoaderEvents
	{
		public enum BaseMenu
		{
			MainMenu = 1
		}

		public class Page
		{
			public List<Button> buttons = new List<Button>();

			public Page previousPage;

			public Button menuEntryButton;

			public Page(BaseMenu baseMenu, string buttonText, Texture2D buttonIcon = null)
			{
				if (baseMenu == BaseMenu.MainMenu)
				{
					menuEntryButton = new Button(BaseMenu.MainMenu, buttonText, delegate
					{
						OpenMenu(null);
					}, buttonIcon);
				}
			}

			public Page(Page basePage, string buttonText, Texture2D buttonIcon = null)
			{
				Page page = this;
				previousPage = basePage;
				menuEntryButton = new Button(previousPage, buttonText, delegate
				{
					page.OpenMenu(basePage);
				}, buttonIcon);
			}

			public void OpenMenu(Page currentPage)
			{
				GetActionMenuOpener().field_Public_ActionMenu_0.Method_Public_Page_Action_Action_Texture2D_String_0(Action.op_Implicit((Action)delegate
				{
					foreach (Button btn in buttons)
					{
						PedalOption pedalOption = GetActionMenuOpener().field_Public_ActionMenu_0.Method_Private_PedalOption_0();
						pedalOption.prop_String_0 = btn.ButtonText;
						pedalOption.field_Public_Func_1_Boolean_0 = Func<bool>.op_Implicit((Func<bool>)delegate
						{
							btn.ButtonAction();
							return true;
						});
						pedalOption.prop_Boolean_0 = btn.IsEnabled;
						if (btn.ButtonIcon != null)
						{
							pedalOption.prop_Texture2D_0 = btn.ButtonIcon;
						}
						btn.currentPedalOption = pedalOption;
					}
				}));
			}
		}

		public class Button
		{
			public string ButtonText;

			public bool IsEnabled;

			public Action ButtonAction;

			public Texture2D ButtonIcon;

			public PedalOption currentPedalOption;

			public Button(BaseMenu baseMenu, string buttonText, Action buttonAction, Texture2D buttonIcon = null)
			{
				ButtonText = buttonText;
				ButtonAction = buttonAction;
				ButtonIcon = buttonIcon;
				if (baseMenu == BaseMenu.MainMenu)
				{
					mainMenuButtons.Add(this);
				}
			}

			public Button(Page basePage, string buttonText, Action buttonAction, Texture2D buttonIcon = null)
			{
				ButtonText = buttonText;
				ButtonAction = buttonAction;
				ButtonIcon = buttonIcon;
				basePage.buttons.Add(this);
			}

			public void SetButtonText(string newText)
			{
				ButtonText = newText;
				if (currentPedalOption != null)
				{
					currentPedalOption.prop_String_0 = newText;
				}
			}

			public void SetIcon(Texture2D newTexture)
			{
				ButtonIcon = newTexture;
				if (currentPedalOption != null)
				{
					currentPedalOption.prop_Texture2D_0 = newTexture;
				}
			}

			public void SetEnabled(bool enabled)
			{
				IsEnabled = enabled;
				if (currentPedalOption != null)
				{
					currentPedalOption.prop_Boolean_0 = !enabled;
				}
			}
		}

		private static List<Page> customPages = new List<Page>();

		private static List<Button> mainMenuButtons = new List<Button>();

		public static ActionMenu activeActionMenu;

		public static Texture2D ToggleOffTexture;

		public static Texture2D ToggleOnTexture;

		private static ActionMenuOpener GetActionMenuOpener()
		{
			if (!ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0.field_Private_Boolean_0 && ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1.field_Private_Boolean_0)
			{
				return ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1;
			}
			if (ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0.field_Private_Boolean_0 && !ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1.field_Private_Boolean_0)
			{
				return ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0;
			}
			return null;
		}

		public override void OnUiManagerInit()
		{
			emmVRCLoaderMod.instance.HarmonyInstance.Patch(typeof(ActionMenu).GetMethods().FirstOrDefault((MethodInfo it) => XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
			{
				if (jt.Type == XrefType.Global)
				{
					Object obj = jt.ReadAsObject();
					return ((obj != null) ? obj.ToString() : null) == "Emojis";
				}
				return false;
			})), null, new HarmonyMethod(typeof(CustomActionMenu).GetMethod("OpenMainPage", BindingFlags.Static | BindingFlags.NonPublic)));
			ToggleOffTexture = Resources.Load<Texture2D>("GUI_Toggle_OFF");
			ToggleOnTexture = Resources.Load<Texture2D>("GUI_Toggle_ON");
		}

		private static void OpenMainPage(ActionMenu __instance)
		{
			activeActionMenu = __instance;
			if (!Configuration.JSONConfig.ActionMenuIntegration)
			{
				return;
			}
			foreach (Button btn in mainMenuButtons)
			{
				PedalOption pedalOption = activeActionMenu.Method_Private_PedalOption_0();
				pedalOption.prop_String_0 = btn.ButtonText;
				Button button = btn;
				button.ButtonAction = (Action)Delegate.Combine(button.ButtonAction, (Action)delegate
				{
					emmVRCLoader.Logger.LogDebug("Button was pushed :CatShrug:");
				});
				pedalOption.field_Public_Func_1_Boolean_0 = Func<bool>.op_Implicit((Func<bool>)delegate
				{
					btn.ButtonAction();
					return true;
				});
				if (btn.ButtonIcon != null)
				{
					pedalOption.prop_Texture2D_0 = btn.ButtonIcon;
				}
				btn.currentPedalOption = pedalOption;
			}
		}
	}
}
