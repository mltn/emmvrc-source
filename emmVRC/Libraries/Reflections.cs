using System;
using System.Linq;
using System.Reflection;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using Il2CppSystem.Reflection;
using TMPro;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using VRC;
using VRC.Animation;
using VRC.Core;
using VRC.DataModel;
using VRC.UI.Elements;

namespace emmVRC.Libraries
{
	public static class Reflections
	{
		public delegate void ResetLastPositionAction(InputStateController @this);

		public delegate void ResetAction(VRCMotionState @this);

		public delegate void ReloadAvatarAction(VRCPlayer @this, bool something = false);

		public delegate void TriggerEmoteAction(VRCPlayer @this, int emote);

		public delegate void ApplyParametersAction(AvatarPlayableController @this, int value);

		private static ResetLastPositionAction ourResetLastPositionAction;

		private static ResetAction ourResetAction;

		private static ReloadAvatarAction ourReloadAvatarAction;

		private static MethodInfo reloadAvatarsMethod;

		private static TriggerEmoteAction ourTriggerEmoteAction;

		private static ApplyParametersAction ourApplyParametersAction;

		private static MethodInfo ourGetPlayerHeightMethod;

		private static MethodInfo ourSetPlayerHeightMethod;

		private static MethodInfo ourSetControllerVisibilityMethod;

		private static MethodInfo ourViewUserInfoMethod;

		private static MethodInfo enterWorldMethod;

		private static Type transitionInfoEnum;

		internal static Type _selectedUserManagerType;

		internal static object _selectedUserManagerObject;

		private static MethodInfo _selectUserMethod;

		public static ResetLastPositionAction ResetLastPositionAct
		{
			get
			{
				if (ourResetLastPositionAction != null)
				{
					return ourResetLastPositionAction;
				}
				MethodInfo method = typeof(InputStateController).GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => XrefScanner.XrefScan(it).Any((XrefInstance jt) => jt.Type == XrefType.Method && jt.TryResolve() != null && jt.TryResolve().Name == "get_transform"));
				ourResetLastPositionAction = (ResetLastPositionAction)Delegate.CreateDelegate(typeof(ResetLastPositionAction), method);
				return ourResetLastPositionAction;
			}
		}

		public static ResetAction ResetAct
		{
			get
			{
				if (ourResetAction != null)
				{
					return ourResetAction;
				}
				MethodInfo method = typeof(VRCMotionState).GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => XrefScanner.XrefScan(it).Count((XrefInstance jt) => jt.Type == XrefType.Method && jt.TryResolve() != null && jt.TryResolve().ReflectedType == typeof(Vector3)) == 4);
				ourResetAction = (ResetAction)Delegate.CreateDelegate(typeof(ResetAction), method);
				return ourResetAction;
			}
		}

		public static ReloadAvatarAction ReloadAvatarAct
		{
			get
			{
				if (ourReloadAvatarAction != null)
				{
					return ourReloadAvatarAction;
				}
				MethodInfo method = typeof(VRCPlayer).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(bool) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "Switching {0} to avatar {1}";
					}
					return false;
				}));
				ourReloadAvatarAction = (ReloadAvatarAction)Delegate.CreateDelegate(typeof(ReloadAvatarAction), method);
				return ourReloadAvatarAction;
			}
		}

		public static TriggerEmoteAction TriggerEmoteAct
		{
			get
			{
				if (ourTriggerEmoteAction != null)
				{
					return ourTriggerEmoteAction;
				}
				MethodInfo method = typeof(VRCPlayer).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(int) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "PlayEmoteRPC";
					}
					return false;
				}));
				ourTriggerEmoteAction = (TriggerEmoteAction)Delegate.CreateDelegate(typeof(TriggerEmoteAction), method);
				return ourTriggerEmoteAction;
			}
		}

		public static ApplyParametersAction ApplyParametersAct
		{
			get
			{
				if (ourApplyParametersAction != null)
				{
					return ourApplyParametersAction;
				}
				MethodInfo method = typeof(AvatarPlayableController).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(int) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "Tried to clear an unassigned puppet channel!";
					}
					return false;
				}));
				ourApplyParametersAction = (ApplyParametersAction)Delegate.CreateDelegate(typeof(ApplyParametersAction), method);
				return ourApplyParametersAction;
			}
		}

		public static MethodInfo getPlayerHeightMethod
		{
			get
			{
				if (ourGetPlayerHeightMethod != null)
				{
					return ourGetPlayerHeightMethod;
				}
				ourGetPlayerHeightMethod = typeof(VRCTrackingManager).GetMethods().Single((MethodInfo it) => it != null && it.ReturnType == typeof(float) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "PlayerHeight";
					}
					return false;
				}));
				return ourGetPlayerHeightMethod;
			}
		}

		public static MethodInfo SetPlayerHeightMethod
		{
			get
			{
				if (ourSetPlayerHeightMethod != null)
				{
					return ourSetPlayerHeightMethod;
				}
				ourSetPlayerHeightMethod = typeof(VRCTrackingManager).GetMethods().Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 1 && it.GetParameters().First().ParameterType == typeof(float) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "PlayerHeight";
					}
					return false;
				}));
				return ourSetPlayerHeightMethod;
			}
		}

		public static MethodInfo SetControllerVisibilityMethod
		{
			get
			{
				if (ourSetControllerVisibilityMethod != null)
				{
					return ourSetControllerVisibilityMethod;
				}
				ourSetControllerVisibilityMethod = typeof(VRCTrackingManager).GetMethods().Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 1 && it.GetParameters().First().ParameterType == typeof(bool) && XrefScanner.XrefScan(it).Any((XrefInstance jt) => jt.Type == XrefType.Method && jt.TryResolve() != null && jt.TryResolve().ReflectedType != null && jt.TryResolve().ReflectedType.Name == "Whiteboard"));
				return ourSetControllerVisibilityMethod;
			}
		}

		public static MethodInfo ViewUserInfoMethod
		{
			get
			{
				if (ourViewUserInfoMethod != null)
				{
					return ourViewUserInfoMethod;
				}
				ourViewUserInfoMethod = typeof(MenuController).GetMethods().First((MethodInfo it) => it != null && it.GetParameters().Length == 1 && it.GetParameters().First().ParameterType == typeof(string) && XrefScanner.XrefScan(it).Any((XrefInstance jt) => jt.Type == XrefType.Method && jt.TryResolve()?.Name == "FetchUser"));
				return ourViewUserInfoMethod;
			}
		}

		public static MethodInfo SelectUserMethod
		{
			get
			{
				//IL_002c: Unknown result type (might be due to invalid IL or missing references)
				if (_selectUserMethod == null)
				{
					_selectedUserManagerObject = GameObject.Find("_Application/UIManager/SelectedUserManager").GetComponents<MonoBehaviour>()[0];
					_selectedUserManagerType = XrefUtils.GetTypeFromObfuscatedName(((MemberInfo)((Object)_selectedUserManagerObject).GetIl2CppType()).get_Name());
					_selectUserMethod = _selectedUserManagerType.GetMethods().First((MethodInfo method) => method.Name.StartsWith("Method_Public_Void_APIUser_") && !method.Name.Contains("_PDM_") && XrefUtils.CheckUsedBy(method, "Method_Public_Virtual_Final_New_Void_IUser_"));
				}
				return _selectUserMethod;
			}
		}

		public static void ResetLastPosition(this InputStateController instance)
		{
			ResetLastPositionAct(instance);
		}

		public static void Reset(this VRCMotionState instance, bool something = false)
		{
			ResetAct(instance);
		}

		public static void ReloadAvatar(this VRCPlayer instance)
		{
			emmVRCLoader.Logger.LogDebug("ReloadAvatar called");
			if (reloadAvatarsMethod == null)
			{
				reloadAvatarsMethod = typeof(VRCPlayer).GetMethods().First((MethodInfo mi) => mi.Name.StartsWith("Method_Private_Void_Boolean_") && mi.Name.Length < 31 && mi.GetParameters().Any((ParameterInfo pi) => pi.IsOptional));
			}
			reloadAvatarsMethod.Invoke(instance, new object[1] { true });
		}

		public static void ReloadAllAvatars(this VRCPlayer instance)
		{
			Enumerator<Player> enumerator = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.GetEnumerator();
			while (enumerator.MoveNext())
			{
				enumerator.get_Current()._vrcplayer.ReloadAvatar();
			}
		}

		public static void TriggerEmote(this VRCPlayer instance, int emote)
		{
			TriggerEmoteAct(instance, emote);
		}

		public static void ApplyParameters(this AvatarPlayableController instance, int value)
		{
			ApplyParametersAct(instance, value);
		}

		public static float GetPlayerHeight(this VRCTrackingManager instance)
		{
			return (float)getPlayerHeightMethod.Invoke(instance, null);
		}

		public static void SetPlayerHeight(this VRCTrackingManager instance, float height)
		{
			SetPlayerHeightMethod.Invoke(instance, new object[1] { height });
		}

		public static void SetControllerVisibility(this VRCTrackingManager instance, bool value)
		{
			SetControllerVisibilityMethod.Invoke(instance, new object[1] { value });
		}

		public static void ViewUserInfo(this MenuController instance, string userID)
		{
			ViewUserInfoMethod.Invoke(instance, new object[1] { userID });
		}

		public static TextMeshProUGUI GetNameplateText(this VRCPlayer player)
		{
			return player.field_Public_PlayerNameplate_0.gameObject.transform.Find("Contents/Main/Text Container/Name").GetComponent<TextMeshProUGUI>();
		}

		public static ImageThreeSlice GetNameplateBackground(this VRCPlayer player)
		{
			return player.field_Public_PlayerNameplate_0.gameObject.transform.Find("Contents/Main/Background").GetComponent<ImageThreeSlice>();
		}

		public static float GetFramerate(this PlayerNet net)
		{
			_ = net.field_Private_Byte_0;
			if ((float)(int)net.field_Private_Byte_0 == 0f)
			{
				return -1f;
			}
			return Mathf.Floor(1000f / (float)(int)net.field_Private_Byte_0);
		}

		public static Color LerpFramerateColor(this PlayerNet net)
		{
			if (net.GetFramerate() == -1f)
			{
				return Color.grey;
			}
			return Color.Lerp(Color.red, Color.green, net.GetFramerate() / 100f);
		}

		public static GameObject menuContent(this VRCUiManager mngr)
		{
			return mngr.field_Public_GameObject_0;
		}

		public static void EnterWorld(this VRCFlowManager mngr, string id, string tags)
		{
			if (enterWorldMethod == null || transitionInfoEnum == null)
			{
				transitionInfoEnum = typeof(WorldTransitionInfo).GetNestedTypes().First();
				enterWorldMethod = typeof(VRCFlowManager).GetMethod("Method_Public_Void_String_String_WorldTransitionInfo_Action_1_String_Boolean_0");
			}
			object obj = Activator.CreateInstance(typeof(WorldTransitionInfo), Enum.Parse(transitionInfoEnum, "Menu"), "WorldInfo_Go");
			obj.GetType().GetProperty("field_Public_" + transitionInfoEnum.Name + "_0").SetValue(obj, transitionInfoEnum.GetEnumValues().GetValue(3));
			enterWorldMethod.Invoke(VRCFlowManager.prop_VRCFlowManager_0, new object[5] { id, tags, obj, null, false });
		}

		public static void OpenUser(this VRC.UI.Elements.QuickMenu menu, APIUser playerToSelect)
		{
			SelectUserMethod.Invoke(UserSelectionManager.prop_UserSelectionManager_0, new object[1] { playerToSelect });
		}
	}
}
