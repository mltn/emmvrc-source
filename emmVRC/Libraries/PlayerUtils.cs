using System;
using Il2CppSystem.Collections.Generic;
using VRC;

namespace emmVRC.Libraries
{
	public class PlayerUtils
	{
		private static Action<Player> requestedAction;

		public static void GetEachPlayer(Action<Player> act)
		{
			requestedAction = act;
			Enumerator<Player> enumerator = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.GetEnumerator();
			while (enumerator.MoveNext())
			{
				Player current = enumerator.get_Current();
				requestedAction(current);
			}
		}
	}
}
