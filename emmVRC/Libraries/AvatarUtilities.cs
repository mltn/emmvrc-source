using System;
using System.Collections;
using System.Collections.Generic;
using emmVRCLoader;
using Il2CppSystem;
using MelonLoader;
using UnhollowerBaseLib;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Libraries
{
	public class AvatarUtilities
	{
		public static List<ApiAvatar> processingList;

		public static bool requestFinished = true;

		public static bool errored = false;

		public static IEnumerator fetchAvatars(List<string> avatars, Action<List<ApiAvatar>, bool> callBack)
		{
			processingList = new List<ApiAvatar>();
			requestFinished = true;
			errored = false;
			foreach (string avatarId in avatars)
			{
				while (!requestFinished)
				{
					yield return new WaitForEndOfFrame();
				}
				requestFinished = false;
				API.Fetch<ApiAvatar>(avatarId, Action<ApiContainer>.op_Implicit((Action<ApiContainer>)delegate(ApiContainer container)
				{
					processingList.Add(((Il2CppObjectBase)(object)container.Model).Cast<ApiAvatar>());
					emmVRCLoader.Logger.LogDebug("Found avatar " + ((Il2CppObjectBase)(object)container.Model).Cast<ApiAvatar>().name);
					MelonCoroutines.Start(Delay());
				}), Action<ApiContainer>.op_Implicit((Action<ApiContainer>)delegate
				{
					errored = true;
					emmVRCLoader.Logger.LogDebug("Current avatar is not available.");
					MelonCoroutines.Start(Delay());
				}));
			}
			while (!requestFinished)
			{
				yield return new WaitForEndOfFrame();
			}
			callBack(processingList, errored);
		}

		public static IEnumerator Delay()
		{
			yield return new WaitForSeconds(2.5f);
			requestFinished = true;
		}
	}
}
