using UnhollowerBaseLib;
using UnityEngine;

namespace emmVRC.Libraries
{
	public class TextureUtilities
	{
		public static Texture2D FlipTextureVertically(Texture2D original)
		{
			Il2CppStructArray<Color> pixels = original.GetPixels();
			Color[] array = new Color[pixels.Length];
			int width = original.width;
			int height = original.height;
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					array[i + j * width] = pixels[i + (height - j - 1) * width];
				}
			}
			original.SetPixels(array);
			original.Apply();
			return original;
		}

		public static Texture2D FlipTextureHorizontally(Texture2D original)
		{
			Il2CppStructArray<Color> pixels = original.GetPixels();
			Color[] array = new Color[pixels.Length];
			int width = original.width;
			int height = original.height;
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					array[i * height + j] = pixels[width - i - 1 + j * height];
				}
			}
			original.SetPixels(array);
			original.Apply();
			return original;
		}
	}
}
