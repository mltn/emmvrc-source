using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using emmVRCLoader;

namespace emmVRC.Libraries
{
	public class AwaitProvider
	{
		public readonly struct YieldAwaitable : INotifyCompletion
		{
			private readonly Queue<Action> myToMainThreadQueue;

			public bool IsCompleted => false;

			public YieldAwaitable(Queue<Action> toMainThreadQueue)
			{
				myToMainThreadQueue = toMainThreadQueue;
			}

			public YieldAwaitable GetAwaiter()
			{
				return this;
			}

			public void GetResult()
			{
			}

			public void OnCompleted(Action continuation)
			{
				lock (myToMainThreadQueue)
				{
					myToMainThreadQueue.Enqueue(continuation);
				}
			}
		}

		private readonly Queue<Action> myToMainThreadQueue = new Queue<Action>();

		public void Flush()
		{
			if (myToMainThreadQueue.Count == 0)
			{
				return;
			}
			List<Action> list;
			lock (myToMainThreadQueue)
			{
				list = myToMainThreadQueue.ToList();
				myToMainThreadQueue.Clear();
			}
			foreach (Action item in list)
			{
				try
				{
					item();
				}
				catch (Exception arg)
				{
					Logger.LogError($"Exception in task: {arg}");
				}
			}
		}

		public YieldAwaitable Yield()
		{
			return new YieldAwaitable(myToMainThreadQueue);
		}
	}
}
