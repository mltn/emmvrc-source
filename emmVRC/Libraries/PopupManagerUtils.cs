using System;
using System.Linq;
using System.Reflection;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
	public static class PopupManagerUtils
	{
		public delegate void ShowUiInputPopupAction(string title, string initialText, InputField.InputType inputType, bool isNumeric, string confirmButtonText, Action<string, List<KeyCode>, Text> onComplete, Action onCancel, string placeholderText = "Enter text...", bool closeAfterInput = true, Action<VRCUiPopup> onPopupShown = null, bool startOnLeft = false, int characterLimit = 0);

		public delegate void ShowUiStandardPopup1Action(string title, string body, Action<VRCUiPopup> onPopupShown = null);

		public delegate void ShowUiStandardPopup2Action(string title, string body, string middleButtonText, Action middleButtonAction, Action<VRCUiPopup> onPopupShown = null);

		public delegate void ShowUiStandardPopup3Action(string title, string body, string leftButtonText, Action leftButtonAction, string rightButtonText, Action rightButtonAction, Action<VRCUiPopup> onPopupShown = null);

		public delegate void ShowUiStandardPopupV21Action(string title, string body, string middleButtonText, Action middleButtonAction, Action<VRCUiPopup> onPopupShown = null);

		public delegate void ShowUiStandardPopupV22Action(string title, string body, string leftButtonText, Action leftButtonAction, string rightButtonText, Action rightButtonAction, Action<VRCUiPopup> onPopupShown = null);

		public delegate void ShowUiAlertPopupAction(string title, string body, float timeout);

		private static ShowUiInputPopupAction ourShowUiInputPopupAction;

		private static ShowUiStandardPopup1Action ourShowUiStandardPopup1Action;

		private static ShowUiStandardPopup2Action ourShowUiStandardPopup2Action;

		private static ShowUiStandardPopup3Action ourShowUiStandardPopup3Action;

		private static ShowUiStandardPopupV21Action ourShowUiStandardPopupV21Action;

		private static ShowUiStandardPopupV22Action ourShowUiStandardPopupV22Action;

		private static ShowUiAlertPopupAction ourShowUiAlertPopupAction;

		public static ShowUiInputPopupAction ShowUiInputPopup
		{
			get
			{
				if (ourShowUiInputPopupAction != null)
				{
					return ourShowUiInputPopupAction;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 12 && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/InputPopup";
					}
					return false;
				}));
				ourShowUiInputPopupAction = (ShowUiInputPopupAction)Delegate.CreateDelegate(typeof(ShowUiInputPopupAction), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiInputPopupAction;
			}
		}

		public static ShowUiStandardPopup1Action ShowUiStandardPopup1
		{
			get
			{
				if (ourShowUiStandardPopup1Action != null)
				{
					return ourShowUiStandardPopup1Action;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 3 && !it.Name.Contains("PDM") && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/StandardPopup";
					}
					return false;
				}));
				ourShowUiStandardPopup1Action = (ShowUiStandardPopup1Action)Delegate.CreateDelegate(typeof(ShowUiStandardPopup1Action), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiStandardPopup1Action;
			}
		}

		public static ShowUiStandardPopup2Action ShowUiStandardPopup2
		{
			get
			{
				if (ourShowUiStandardPopup2Action != null)
				{
					return ourShowUiStandardPopup2Action;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 5 && !it.Name.Contains("PDM") && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/StandardPopup";
					}
					return false;
				}));
				ourShowUiStandardPopup2Action = (ShowUiStandardPopup2Action)Delegate.CreateDelegate(typeof(ShowUiStandardPopup2Action), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiStandardPopup2Action;
			}
		}

		public static ShowUiStandardPopup3Action ShowUiStandardPopup3
		{
			get
			{
				if (ourShowUiStandardPopup3Action != null)
				{
					return ourShowUiStandardPopup3Action;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 7 && !it.Name.Contains("PDM") && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/StandardPopup";
					}
					return false;
				}));
				ourShowUiStandardPopup3Action = (ShowUiStandardPopup3Action)Delegate.CreateDelegate(typeof(ShowUiStandardPopup3Action), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiStandardPopup3Action;
			}
		}

		public static ShowUiStandardPopupV21Action ShowUiStandardPopupV21
		{
			get
			{
				if (ourShowUiStandardPopupV21Action != null)
				{
					return ourShowUiStandardPopupV21Action;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 5 && !it.Name.Contains("PDM") && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/StandardPopupV2";
					}
					return false;
				}));
				ourShowUiStandardPopupV21Action = (ShowUiStandardPopupV21Action)Delegate.CreateDelegate(typeof(ShowUiStandardPopupV21Action), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiStandardPopupV21Action;
			}
		}

		public static ShowUiStandardPopupV22Action ShowUiStandardPopupV22
		{
			get
			{
				if (ourShowUiStandardPopupV22Action != null)
				{
					return ourShowUiStandardPopupV22Action;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 7 && !it.Name.Contains("PDM") && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/StandardPopupV2";
					}
					return false;
				}));
				ourShowUiStandardPopupV22Action = (ShowUiStandardPopupV22Action)Delegate.CreateDelegate(typeof(ShowUiStandardPopupV22Action), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiStandardPopupV22Action;
			}
		}

		public static ShowUiAlertPopupAction ShowUiAlertPopup
		{
			get
			{
				if (ourShowUiAlertPopupAction != null)
				{
					return ourShowUiAlertPopupAction;
				}
				MethodInfo method = typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault((MethodInfo it) => it.GetParameters().Length == 3 && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "UserInterface/MenuContent/Popups/AlertPopup";
					}
					return false;
				}));
				ourShowUiAlertPopupAction = (ShowUiAlertPopupAction)Delegate.CreateDelegate(typeof(ShowUiAlertPopupAction), VRCUiPopupManager.prop_VRCUiPopupManager_0, method);
				return ourShowUiAlertPopupAction;
			}
		}

		public static void HideCurrentPopup(this VRCUiPopupManager vrcUiPopupManager)
		{
			VRCUiManager.prop_VRCUiManager_0.HideScreen("POPUP");
		}

		public static void ShowStandardPopup(this VRCUiPopupManager vrcUiPopupManager, string title, string content, Action<VRCUiPopup> onCreated = null)
		{
			ShowUiStandardPopup1(title, content, Action<VRCUiPopup>.op_Implicit(onCreated));
		}

		public static void ShowStandardPopup(this VRCUiPopupManager vrcUiPopupManager, string title, string content, string buttonText, Action buttonAction, Action<VRCUiPopup> onCreated = null)
		{
			ShowUiStandardPopup2(title, content, buttonText, Action.op_Implicit(buttonAction), Action<VRCUiPopup>.op_Implicit(onCreated));
		}

		public static void ShowStandardPopup(this VRCUiPopupManager vrcUiPopupManager, string title, string content, string button1Text, Action button1Action, string button2Text, Action button2Action, Action<VRCUiPopup> onCreated = null)
		{
			ShowUiStandardPopup3(title, content, button1Text, Action.op_Implicit(button1Action), button2Text, Action.op_Implicit(button2Action), Action<VRCUiPopup>.op_Implicit(onCreated));
		}

		public static void ShowStandardPopupV2(this VRCUiPopupManager vrcUiPopupManager, string title, string content, string buttonText, Action buttonAction, Action<VRCUiPopup> onCreated = null)
		{
			ShowUiStandardPopupV21(title, content, buttonText, Action.op_Implicit(buttonAction), Action<VRCUiPopup>.op_Implicit(onCreated));
		}

		public static void ShowStandardPopupV2(this VRCUiPopupManager vrcUiPopupManager, string title, string content, string button1Text, Action button1Action, string button2Text, Action button2Action, Action<VRCUiPopup> onCreated = null)
		{
			ShowUiStandardPopupV22(title, content, button1Text, Action.op_Implicit(button1Action), button2Text, Action.op_Implicit(button2Action), Action<VRCUiPopup>.op_Implicit(onCreated));
		}

		public static void ShowInputPopup(this VRCUiPopupManager vrcUiPopupManager, string title, string preFilledText, InputField.InputType inputType, bool keypad, string buttonText, Action<string, List<KeyCode>, Text> buttonAction, Action cancelAction, string boxText = "Enter text....", bool closeOnAccept = true, Action<VRCUiPopup> onCreated = null, bool startOnLeft = false, int characterLimit = 0)
		{
			ShowUiInputPopup(title, preFilledText, inputType, keypad, buttonText, buttonAction, cancelAction, boxText, closeOnAccept, Action<VRCUiPopup>.op_Implicit(onCreated), startOnLeft, characterLimit);
		}

		public static void ShowAlert(this VRCUiPopupManager vrcUiPopupManager, string title, string content, float timeout)
		{
			ShowUiAlertPopup(title, content, timeout);
		}
	}
}
