using System.Threading.Tasks;
using emmVRCLoader;
using VRC.Core;

namespace emmVRC
{
	public static class Extensions
	{
		public static bool HasCustomName(this APIUser user)
		{
			return !string.IsNullOrWhiteSpace(user.displayName);
		}

		public static string GetName(this APIUser user)
		{
			if (user.HasCustomName())
			{
				return user.displayName;
			}
			return user.username;
		}

		public static void NoAwait(this Task task, string taskDescription = null)
		{
			task.ContinueWith(delegate(Task tsk)
			{
				if (tsk.IsFaulted)
				{
					emmVRCLoader.Logger.LogError(string.Format("Free-floating Task {0}}} failed with exception: {1}", (taskDescription == null) ? "" : ("(" + taskDescription + ")"), tsk.Exception));
				}
			});
		}
	}
}
