using System.Collections;
using System.Collections.Generic;
using System.Linq;
using emmVRC.Functions.Core;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;
using UnityEngine;

namespace emmVRC.Menus
{
	public class ActionMenuFunctions : MelonLoaderEvents, IWithLateUpdate
	{
		public static CustomActionMenu.Page functionsMenu;

		public static CustomActionMenu.Page favouriteEmojisMenu;

		public static CustomActionMenu.Page riskyFunctionsMenu;

		public static List<CustomActionMenu.Button> favouriteEmojiButtons;

		public static CustomActionMenu.Button flightButton;

		public static CustomActionMenu.Button noclipButton;

		public static CustomActionMenu.Button speedButton;

		public static CustomActionMenu.Button espButton;

		public static CustomActionMenu.Page avatarParametersMenu;

		public static CustomActionMenu.Button saveAvatarParameters;

		public static CustomActionMenu.Button clearAvatarParameters;

		private static EmojiMenu emojiMenu;

		private static bool playingEmoji = false;

		private static int currentEmojiPlaying = -1;

		public override void OnUiManagerInit()
		{
			MelonCoroutines.Start(Initialize());
		}

		public static IEnumerator Initialize()
		{
			while (global::emmVRC.Functions.Core.Resources.onlineSprite == null || global::emmVRC.Functions.Core.Resources.onlineSprite.texture == null)
			{
				yield return new WaitForEndOfFrame();
			}
			functionsMenu = new CustomActionMenu.Page(CustomActionMenu.BaseMenu.MainMenu, "emmVRC\nFunctions", global::emmVRC.Functions.Core.Resources.onlineSprite.texture);
			riskyFunctionsMenu = new CustomActionMenu.Page(functionsMenu, "Risky\nFunctions", global::emmVRC.Functions.Core.Resources.flyTexture);
			favouriteEmojisMenu = new CustomActionMenu.Page(functionsMenu, "Favorite\nEmojis", global::emmVRC.Functions.Core.Resources.rpSprite.texture);
			favouriteEmojiButtons = new List<CustomActionMenu.Button>();
			for (int i = 0; i < 8; i++)
			{
				int currentEmojiButtonOption = i;
				favouriteEmojiButtons.Add(new CustomActionMenu.Button(favouriteEmojisMenu, "", delegate
				{
					emmVRCLoader.Logger.LogDebug("Trying to spawn Emoji " + Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption]);
					emojiMenu.TriggerEmoji(Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption]);
					playingEmoji = true;
					currentEmojiPlaying = Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption];
					MelonCoroutines.Start(EmojiTimeout());
				}));
			}
			flightButton = new CustomActionMenu.Button(riskyFunctionsMenu, "Flight:\nOff", delegate
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (Flight.IsNoClipEnabled && Flight.IsFlyEnabled)
					{
						Flight.SetNoClipActive(active: false);
					}
					Flight.SetFlyActive(!Flight.IsFlyEnabled);
				}
			}, CustomActionMenu.ToggleOffTexture);
			noclipButton = new CustomActionMenu.Button(riskyFunctionsMenu, "Noclip:\nOff", delegate
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (!Flight.IsFlyEnabled && !Flight.IsNoClipEnabled)
					{
						Flight.SetFlyActive(active: true);
					}
					Flight.SetNoClipActive(!Flight.IsNoClipEnabled);
				}
			}, CustomActionMenu.ToggleOffTexture);
			speedButton = new CustomActionMenu.Button(riskyFunctionsMenu, "Speed:\nOff", delegate
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					Speed.SetActive(!Speed.IsEnabled);
				}
			}, CustomActionMenu.ToggleOffTexture);
			espButton = new CustomActionMenu.Button(riskyFunctionsMenu, "ESP:\nOff", delegate
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					ESP.SetActive(!ESP.IsEnabled);
				}
			}, CustomActionMenu.ToggleOffTexture);
		}

		public void LateUpdate()
		{
			if (functionsMenu == null)
			{
				return;
			}
			_ = emojiMenu == null;
			if (flightButton.currentPedalOption != null)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					flightButton.SetButtonText("Flight:\n" + (Flight.IsFlyEnabled ? "On" : "Off"));
					flightButton.SetIcon(Flight.IsFlyEnabled ? global::emmVRC.Functions.Core.Resources.toggleOnTexture : global::emmVRC.Functions.Core.Resources.toggleOffTexture);
					noclipButton.SetButtonText("Noclip:\n" + (Flight.IsNoClipEnabled ? "On" : "Off"));
					noclipButton.SetIcon(Flight.IsNoClipEnabled ? global::emmVRC.Functions.Core.Resources.toggleOnTexture : global::emmVRC.Functions.Core.Resources.toggleOffTexture);
					speedButton.SetButtonText("Speed:\n" + (Speed.IsEnabled ? "On" : "Off"));
					speedButton.SetIcon(Speed.IsEnabled ? global::emmVRC.Functions.Core.Resources.toggleOnTexture : global::emmVRC.Functions.Core.Resources.toggleOffTexture);
					espButton.SetButtonText("ESP:\n" + (ESP.IsEnabled ? "On" : "Off"));
					espButton.SetIcon(ESP.IsEnabled ? global::emmVRC.Functions.Core.Resources.toggleOnTexture : global::emmVRC.Functions.Core.Resources.toggleOffTexture);
					flightButton.SetEnabled(enabled: true);
					noclipButton.SetEnabled(enabled: true);
					speedButton.SetEnabled(enabled: true);
					espButton.SetEnabled(enabled: true);
				}
				else
				{
					flightButton.SetEnabled(enabled: false);
					noclipButton.SetEnabled(enabled: false);
					speedButton.SetEnabled(enabled: false);
					espButton.SetEnabled(enabled: false);
				}
			}
			if (!favouriteEmojiButtons.All((CustomActionMenu.Button a) => a.currentPedalOption != null))
			{
				return;
			}
			for (int i = 0; i < Configuration.JSONConfig.FavouritedEmojis.Count; i++)
			{
				if (favouriteEmojiButtons[i].currentPedalOption != null)
				{
					favouriteEmojiButtons[i].SetEnabled(!playingEmoji);
				}
			}
			for (int j = 0; j < 8 - Configuration.JSONConfig.FavouritedEmojis.Count; j++)
			{
				if (favouriteEmojiButtons[Configuration.JSONConfig.FavouritedEmojis.Count + j].currentPedalOption != null)
				{
					favouriteEmojiButtons[Configuration.JSONConfig.FavouritedEmojis.Count + j].SetEnabled(enabled: false);
				}
			}
		}

		private static IEnumerator EmojiTimeout()
		{
			yield return new WaitForSeconds(2f);
			playingEmoji = false;
			currentEmojiPlaying = -1;
		}
	}
}
