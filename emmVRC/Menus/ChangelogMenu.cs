using emmVRC.Functions.Core;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class ChangelogMenu : MelonLoaderEvents
	{
		private static bool _initialized;

		internal static MenuPage changelogPage;

		private static SingleButton changelogPageButton;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized)
			{
				changelogPage = new MenuPage("emmVRC_Changelog", "Changelog", root: false, backButton: true, extButton: true, delegate
				{
					ButtonAPI.GetQuickMenuInstance().AskConfirmOpenURL("https://discord.gg/emmVRC");
				});
				changelogPage.menuContents.GetComponent<VerticalLayoutGroup>().childControlHeight = true;
				changelogPageButton = new SingleButton(FunctionsMenu.otherGroup, "Changelog", changelogPage.OpenMenu, "View what's new with the current version of emmVRC", global::emmVRC.Functions.Core.Resources.ChangelogIcon);
				GameObject gameObject = new GameObject("ChangelogText");
				gameObject.transform.SetParent(changelogPage.menuContents);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
				gameObject.transform.localScale = Vector3.one;
				TextMeshProUGUI textMeshProUGUI = gameObject.AddComponent<TextMeshProUGUI>();
				textMeshProUGUI.margin = new Vector4(25f, 0f, 50f, 0f);
				textMeshProUGUI.text = "<size=50><color=#FF69B4>emmVRC</color> version " + Attributes.Version.ToString(3) + " (11/21/2021)</size>\n\n<b>v3.0.5</b>\n• Fixed support for mods such as ReModCE that would cause the menus to misbehave\n• Fixed notification icons being colored slightly blue\n\n<b>v3.0.4</b>\n• Fixed the Social Menu functions button not working on VRChat build 1151\n• Fixed Oculus support\n\n<b>v3.0.3</b>\n• Fixed the \"Grey Square\" bug (for real for real this time)\n• Added a toggle for UIExpansionKit integration to Settings\n• Added a version and emmVRC Network connection status indicator to the emmVRC Functions menu. This behaves identically to the info bar from prior versions\n• The new update notification now has a button to view the changelog\n• Added emmVRC Actions to the menu displayed when you select a user\n• Master Icon settings now actually work\n• Changed the behavior of the quit icon in the Quick Menu's Settings page to now offer Instant Restart\n• Adjusted the Player Tweaks menu so that speed is available without needing to scroll down a tiny amount\n• Fixed the persistent notification icon issue\n• The EULA prompt has been moved to the emmVRC tab action, so it will behave better on all platforms\n• Fixed the VR flying speeds being switched (left and right would use run speed, and forward/back would use strafe speed)\n• Fixed the Nameplate Coloring setting not updating correctly\n• Fixed search being accessible despite not being connected to the emmVRC Network\n• Global Dynamic Bones should now be functional as it was before the UI update\n• Fixed being able to enable NoClip without flight via the action menu and keybinds\n\n<b>v3.0.2</b>\n• Fixed the \"Grey Square\" bug (for real this time)\n• Fixed prompts and dialog boxes not showing (resulting in certain functions like Risky Functions being unavailable)\n• Removed the Show Author button, as it is now a part of VRChat\n\n<b>v3.0.1</b>\n• Fixed the \"Grey Square\" bug\n• Fixed selecting users in the Player History menu\n• Fixed all toggles not updating to their proper toggle states\n• Fixed being able to enable NoClip and not Flight, resulting in the player becoming stuck\n\n<b>v3.0.0</b>\n• Interaction with pickup triggers (e.g. flashlights) now works like the vanilla game\n• emmVRC's menus have been completely remade for the new VRChat UI\n• Added more exclusions and inclusions to the color module\n• Several modules of emmVRC have been rewritten for better performance and expandability\n• Errors in specific modules of emmVRC will no longer cause the rest of the mod to stop loading or working\n\n<b><color=#FF5555>Notice</color></b>\nThe following features are <i>known</i> to be broken or unavailable, and will be available in a later update:\n- Alarm Clocks\n- Keybind configuration via the UI\n\nThe following features have been removed, and will <i>not</i> be returning to emmVRC:\n- Info spoofing\n- Stealth mode\n";
				_initialized = true;
			}
		}
	}
}
