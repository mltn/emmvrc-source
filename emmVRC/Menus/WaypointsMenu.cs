using System;
using System.Collections.Generic;
using System.Linq;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class WaypointsMenu : MelonLoaderEvents
	{
		private static bool _initialized = false;

		private static MenuPage waypointsPage;

		private static ButtonGroup waypointsGroup;

		private static List<SimpleSingleButton> waypointButtons = new List<SimpleSingleButton>();

		private static MenuPage selectedWaypointPage;

		private static ButtonGroup optionsGroup;

		private static SimpleSingleButton teleportButton;

		private static SimpleSingleButton renameButton;

		private static SimpleSingleButton setLocationButton;

		private static SimpleSingleButton removeButton;

		private static int selectedWaypoint = 0;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			waypointsPage = new MenuPage("emmVRC_Waypoints", "Waypoints", root: false, backButton: true, extButton: true, delegate
			{
				Waypoints.CurrentWaypoints.Add(new Waypoint
				{
					Name = "Waypoint " + (Waypoints.CurrentWaypoints.Count((Waypoint a) => a != null) + 1),
					x = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.x,
					y = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.y,
					z = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.z,
					rx = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.x,
					ry = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.y,
					rz = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.z,
					rw = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.w
				});
				Waypoints.SaveWaypoints();
				LoadMenu();
			}, "Add a new waypoint for your current location", ButtonAPI.plusIconSprite);
			waypointsPage.menuContents.GetComponent<VerticalLayoutGroup>().childControlHeight = true;
			waypointsPage.AddExtButton(delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowConfirmDialog("Waypoints", "Are you sure you want to remove all your waypoints for this world? You cannot undo this!", delegate
				{
					Waypoints.CurrentWaypoints.Clear();
					Waypoints.SaveWaypoints();
					LoadMenu();
				});
			}, "Removes all of the waypoints for the current world", ButtonAPI.xIconSprite);
			waypointsGroup = new ButtonGroup(waypointsPage, "");
			selectedWaypointPage = new MenuPage("emmVRC_SelectedWaypoint", "Selected waypoint", root: false);
			optionsGroup = new ButtonGroup(selectedWaypointPage, "Waypoint Options");
			teleportButton = new SimpleSingleButton(optionsGroup, "Teleport", delegate
			{
				Waypoints.CurrentWaypoints[selectedWaypoint].Goto();
			}, "Teleport to this waypoint");
			renameButton = new SimpleSingleButton(optionsGroup, "Rename", delegate
			{
				selectedWaypointPage.CloseMenu();
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Type a name (or none for default)", "", InputField.InputType.Standard, keypad: false, "Accept", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate(string name, List<KeyCode> keyk, Text tx)
				{
					Waypoints.CurrentWaypoints[selectedWaypoint].Name = name;
					Waypoints.SaveWaypoints();
					selectedWaypointPage.CloseMenu();
					OpenMenu();
				}), null, "Enter name...");
			}, "Rename this waypoint");
			setLocationButton = new SimpleSingleButton(optionsGroup, "Set Location", delegate
			{
				Waypoints.CurrentWaypoints[selectedWaypoint].x = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.x;
				Waypoints.CurrentWaypoints[selectedWaypoint].y = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.y;
				Waypoints.CurrentWaypoints[selectedWaypoint].z = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.z;
				Waypoints.CurrentWaypoints[selectedWaypoint].rx = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.x;
				Waypoints.CurrentWaypoints[selectedWaypoint].ry = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.y;
				Waypoints.CurrentWaypoints[selectedWaypoint].rz = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.z;
				Waypoints.CurrentWaypoints[selectedWaypoint].rw = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.w;
				Waypoints.SaveWaypoints();
				selectedWaypointPage.CloseMenu();
				LoadMenu();
			}, "Set the waypoint's location to where you are standing");
			removeButton = new SimpleSingleButton(optionsGroup, "Remove", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowConfirmDialog("Waypoint", "Are you sure you want to remove '" + Waypoints.CurrentWaypoints[selectedWaypoint].Name + "'?", delegate
				{
					Waypoints.CurrentWaypoints.RemoveAt(selectedWaypoint);
					Waypoints.SaveWaypoints();
					selectedWaypointPage.CloseMenu();
					selectedWaypointPage.CloseMenu();
					LoadMenu();
				});
			}, "Remove this waypoint");
			_initialized = true;
		}

		public static void OpenMenu()
		{
			waypointsPage.OpenMenu();
			LoadMenu();
		}

		public static void LoadMenu()
		{
			if (!_initialized || !RiskyFunctionsManager.AreRiskyFunctionsAllowed || !Configuration.JSONConfig.RiskyFunctionsEnabled)
			{
				return;
			}
			waypointsGroup.gameObject.transform.DestroyChildren();
			foreach (Waypoint waypoint in Waypoints.CurrentWaypoints)
			{
				if (waypoint != null)
				{
					new SimpleSingleButton(waypointsGroup, (waypoint.Name == null) ? "" : waypoint.Name, delegate
					{
						selectedWaypoint = Waypoints.CurrentWaypoints.IndexOf(waypoint);
						selectedWaypointPage.OpenMenu();
					}, "See options for this waypoint");
				}
			}
		}
	}
}
