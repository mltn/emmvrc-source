using System.Linq;
using emmVRC.Components;
using emmVRC.Functions.UI;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using Il2CppSystem.Collections.Generic;
using VRC;
using VRC.Core;
using VRC.DataModel;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class SelectedUserLocalMenu : MelonLoaderEvents
	{
		private static ButtonGroup selectedUserGroup;

		private static SimpleSingleButton avatarOptionsButton;

		private static SimpleSingleButton playerNotesButton;

		private static SimpleSingleButton teleportButton;

		private static SimpleSingleButton favouriteAvatarButton;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			selectedUserGroup = new ButtonGroup(ButtonAPI.menuPageBase.transform.parent.Find("Menu_SelectedUser_Local/ScrollRect/Viewport/VerticalLayoutGroup"), "emmVRC Actions");
			avatarOptionsButton = new SimpleSingleButton(selectedUserGroup, "Avatar\nOptions", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowAlert("Not yet implemented");
			}, "Shows various options for the selected avatar, including toggling components and global dynamic bones");
			playerNotesButton = new SimpleSingleButton(selectedUserGroup, "Player\nNotes", delegate
			{
				APIUser field_Private_APIUser_ = UserSelectionManager.field_Private_Static_UserSelectionManager_0.field_Private_APIUser_1;
				PlayerNotes.LoadNoteQM(field_Private_APIUser_.id, field_Private_APIUser_.GetName());
			}, "View the notes for the selected player");
			teleportButton = new SimpleSingleButton(selectedUserGroup, "Teleport", delegate
			{
				Player player = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.ToArray().FirstOrDefault((Player a) => a.field_Private_APIUser_0 != null && a.field_Private_APIUser_0.id == UserSelectionManager.field_Private_Static_UserSelectionManager_0.field_Private_APIUser_1.id);
				if (!(player == null) && Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed)
				{
					VRCPlayer.field_Internal_Static_VRCPlayer_0.field_Private_VRCPlayerApi_0.TeleportTo(player.transform.position, player.transform.rotation);
				}
			}, "Teleport to the selected player (requires Risky Functions to be enabled and allowed)");
			teleportButton.gameObject.AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				teleportButton.SetInteractable(RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled);
			};
			favouriteAvatarButton = new SimpleSingleButton(selectedUserGroup, "Favorite\nAvatar", delegate
			{
				if (!PlayerUtils.DoesUserHaveVRCPlus())
				{
					ButtonAPI.GetQuickMenuInstance().ShowOKDialog("VRChat Plus Required", "VRChat, like emmVRC, relies on the support of their users to keep the platform free. Please support VRChat to unlock these features.");
				}
				else
				{
					bool flag = false;
					APIUser selectedAPIUser = UserSelectionManager.field_Private_Static_UserSelectionManager_0.field_Private_APIUser_1;
					if (selectedAPIUser == null)
					{
						selectedAPIUser = UserSelectionManager.field_Private_Static_UserSelectionManager_0.field_Private_APIUser_0;
					}
					if (selectedAPIUser != null)
					{
						VRCPlayer vRCPlayer = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.ToArray().FirstOrDefault((Player a) => a.field_Private_APIUser_0 != null && a.field_Private_APIUser_0.id == selectedAPIUser.id)?._vrcplayer;
						if (!(vRCPlayer == null))
						{
							if (selectedAPIUser.allowAvatarCopying && vRCPlayer._player.prop_ApiAvatar_0.releaseStatus == "public")
							{
								Enumerator<ApiAvatar> enumerator = CustomAvatarFavorites.LoadedAvatars.GetEnumerator();
								while (enumerator.MoveNext())
								{
									if (enumerator.get_Current().id == vRCPlayer._player.prop_ApiAvatar_0.id)
									{
										flag = true;
									}
								}
								if (flag)
								{
									ButtonAPI.GetQuickMenuInstance().ShowOKDialog("emmVRC", "You already have this avatar favorited");
								}
								else
								{
									CustomAvatarFavorites.FavoriteAvatar(vRCPlayer._player.prop_ApiAvatar_0).NoAwait("FavoriteAvatar");
								}
							}
							else
							{
								ButtonAPI.GetQuickMenuInstance().ShowOKDialog("emmVRC", "This avatar is not public, or the user does not have cloning turned on.");
							}
						}
					}
				}
			}, "Favorite this user's avatar to your emmVRC Favorite list (Requires VRC+ and cloning to be on)");
			_initialized = true;
		}
	}
}
