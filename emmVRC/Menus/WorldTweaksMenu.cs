using emmVRC.Functions.Core;
using emmVRC.Functions.WorldHacks;
using emmVRC.Hacks;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class WorldTweaksMenu : MelonLoaderEvents
	{
		public static MenuPage worldTweaksPage;

		private static SingleButton worldTweaksButton;

		private static ButtonGroup mirrorsGroup;

		private static SimpleSingleButton optimizeMirrorsButton;

		private static SimpleSingleButton beautifyMirrorsButton;

		private static SimpleSingleButton revertMirrorsButton;

		public static ButtonGroup objectsGroup;

		private static ButtonGroup componentTogglesGroup;

		private static ToggleButton portalsToggle;

		private static ToggleButton chairsToggle;

		private static ToggleButton pedestalsToggle;

		private static ToggleButton videoPlayersToggle;

		private static ButtonGroup componentTogglesGroup2;

		private static ToggleButton pickupsToggle;

		private static ToggleButton pickupVisibilityToggle;

		private static ButtonGroup espTogglesGroup;

		private static ToggleButton itemEspToggle;

		private static ToggleButton triggerEspToggle;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			worldTweaksPage = new MenuPage("emmVRC_WorldTweaks", "World Tweaks", root: false);
			worldTweaksButton = new SingleButton(FunctionsMenu.tweaksGroup, "World", OpenMenu, "Functions that affect the current world you're in", Resources.WorldIcon);
			mirrorsGroup = new ButtonGroup(worldTweaksPage, "Mirrors");
			optimizeMirrorsButton = new SimpleSingleButton(mirrorsGroup, "Optimize", MirrorTweaks.Optimize, "Optimizes all the mirrors in the current world by only rendering players");
			beautifyMirrorsButton = new SimpleSingleButton(mirrorsGroup, "Beautify", MirrorTweaks.Beautify, "Improves all the mirrors in the current world by rendering <i>everything</i>");
			revertMirrorsButton = new SimpleSingleButton(mirrorsGroup, "Revert", MirrorTweaks.Revert, "Reverts all the mirrors in the current world to their original state");
			objectsGroup = new ButtonGroup(worldTweaksPage, "Objects");
			componentTogglesGroup = new ButtonGroup(worldTweaksPage, "Components");
			portalsToggle = new ToggleButton(componentTogglesGroup, "Portals", delegate(bool val)
			{
				Configuration.WriteConfigOption("PortalBlockingEnable", !val);
			}, "Allow entering portals", "Prevent entering portals");
			chairsToggle = new ToggleButton(componentTogglesGroup, "Chairs", delegate(bool val)
			{
				Configuration.WriteConfigOption("ChairBlockingEnable", !val);
			}, "Allow interacting with chairs", "Prevent interaction with chairs");
			pedestalsToggle = new ToggleButton(componentTogglesGroup, "Avatar\nPedestals", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarPedestals", !val);
				if (val)
				{
					PedestalTweaks.Disable();
				}
				else
				{
					PedestalTweaks.Revert();
				}
			}, "Show all avatar pedestals", "Hide all avatar pedestals");
			videoPlayersToggle = new ToggleButton(componentTogglesGroup, "Video\nPlayers", delegate(bool val)
			{
				ComponentToggle.videoplayers = val;
				ComponentToggle.Toggle();
			}, "Enable Video Players", "Disable Video Players");
			componentTogglesGroup2 = new ButtonGroup(worldTweaksPage, "");
			pickupsToggle = new ToggleButton(componentTogglesGroup, "Pickups", delegate(bool val)
			{
				ComponentToggle.pickupable = val;
				ComponentToggle.Toggle();
			}, "Allow picking up objects", "Prevent picking up objects");
			pickupVisibilityToggle = new ToggleButton(componentTogglesGroup, "Pickups\nVisible", delegate(bool val)
			{
				ComponentToggle.pickup_object = val;
				ComponentToggle.Toggle();
			}, "Show pickup objects", "Hide pickup objects");
			espTogglesGroup = new ButtonGroup(worldTweaksPage, "ESP");
			itemEspToggle = new ToggleButton(espTogglesGroup, "Items", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					WorldESP.ToggleItemESP(val);
				}
			}, "Turn on glow for items in the world", "Turn off glow for items in the world");
			triggerEspToggle = new ToggleButton(espTogglesGroup, "Triggers", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					WorldESP.ToggleTriggerESP(val);
				}
			}, "Turn on glow for triggers in the world", "Turn off glow for triggers in the world (SDK2 only)");
			RiskyFunctionsManager.OnRiskyFunctionCheckCompleted += delegate(bool val)
			{
				itemEspToggle.SetInteractable(val);
				triggerEspToggle.SetInteractable(val);
				pickupsToggle.SetInteractable(val);
				pickupVisibilityToggle.SetInteractable(val);
			};
			_initialized = true;
		}

		private static void OpenMenu()
		{
			chairsToggle.SetToggleState(!Configuration.JSONConfig.ChairBlockingEnable);
			pedestalsToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarPedestals);
			portalsToggle.SetToggleState(!Configuration.JSONConfig.PortalBlockingEnable);
			pickupsToggle.SetToggleState(ComponentToggle.pickupable);
			pickupVisibilityToggle.SetToggleState(ComponentToggle.pickup_object);
			videoPlayersToggle.SetToggleState(ComponentToggle.videoplayers);
			itemEspToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			triggerEspToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			pickupsToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			pickupVisibilityToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			worldTweaksPage.OpenMenu();
		}
	}
}
