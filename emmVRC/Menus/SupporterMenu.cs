using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using emmVRC.Functions.Core;
using emmVRC.Network;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using emmVRCLoader;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class SupporterMenu : MelonLoaderEvents
	{
		private static MenuPage supporterPage;

		private static SingleButton supporterPageButton;

		private static TextMeshProUGUI text;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized)
			{
				supporterPage = new MenuPage("emmVRC_Supporters", "Supporters", root: false);
				supporterPageButton = new SingleButton(FunctionsMenu.otherGroup, "Supporters", LoadMenu, "View all of the donators to the emmVRC project", global::emmVRC.Functions.Core.Resources.SupporterIcon);
				supporterPage.menuContents.GetComponent<VerticalLayoutGroup>().childControlHeight = true;
				GameObject gameObject = new GameObject("SupportersText");
				gameObject.transform.SetParent(supporterPage.menuContents);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
				gameObject.transform.localScale = Vector3.one;
				TextMeshProUGUI textMeshProUGUI = gameObject.AddComponent<TextMeshProUGUI>();
				textMeshProUGUI.margin = new Vector4(25f, 0f, 50f, 0f);
				textMeshProUGUI.text = "";
				text = textMeshProUGUI;
				_initialized = true;
			}
		}

		public static void LoadMenu()
		{
			try
			{
				supporterPage.OpenMenu();
				EnterMenu(text).NoAwait("SupporterMenu");
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError(ex.ToString());
			}
		}

		public static async Task EnterMenu(TextMeshProUGUI text)
		{
			text.text = "Fetching, please wait...";
			text.text = "";
			try
			{
				List<Supporter> supporters = Decoder.Decode(await HTTPRequest.get("http://dl.emmvrc.com/supporterList.json")).Make<List<Supporter>>();
				await emmVRC.AwaitUpdate.Yield();
				text.text = "<size=50>Special thanks to these supporters!</size>\n\n";
				foreach (Supporter item in supporters)
				{
					text.text = text.text + "<color=" + item.color + ">" + item.name.Replace("\n", " ") + "</color>\n";
				}
			}
			catch (Exception ex)
			{
				text.text = "Network error...";
				text.text = "<color=#ff0000>" + ex.ToString() + "</color>";
				await emmVRC.AwaitUpdate.Yield();
				throw ex;
			}
		}
	}
}
