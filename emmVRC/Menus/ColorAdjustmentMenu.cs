using System;
using emmVRC.Functions.Core;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;
using UnityEngine.UI;
using VRC.UI.Elements.Tooltips;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class ColorAdjustmentMenu : MelonLoaderEvents
	{
		private static bool _initialized = false;

		private static MenuPage colorAdjustmentPage;

		private static global::emmVRC.Utils.Slider rSlider;

		private static global::emmVRC.Utils.Slider gSlider;

		private static global::emmVRC.Utils.Slider bSlider;

		private static global::emmVRC.Utils.Slider aSlider;

		private static Image colorDemo;

		private static Action<Color> onColorAccepted;

		private static Color currentColor = Color.white;

		public override void OnSceneWasInitialized(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized)
			{
				colorAdjustmentPage = new MenuPage("emmVRC_ColorAdjustment", "Color", root: false, backButton: true, extButton: true, delegate
				{
					onColorAccepted?.Invoke(currentColor);
					colorAdjustmentPage.CloseMenu();
				}, "Save the changes to this color", ButtonAPI.onIconSprite);
				rSlider = new global::emmVRC.Utils.Slider(colorAdjustmentPage, "Red", delegate(float flt)
				{
					currentColor.r = Mathf.Floor(flt) / 255f;
					RenderColor();
				}, "Adjust the red value of the color", 255f, 0f, floor: true, percent: false);
				gSlider = new global::emmVRC.Utils.Slider(colorAdjustmentPage, "Green", delegate(float flt)
				{
					currentColor.g = Mathf.Floor(flt) / 255f;
					RenderColor();
				}, "Adjust the green value of the color", 255f, 0f, floor: true, percent: false);
				bSlider = new global::emmVRC.Utils.Slider(colorAdjustmentPage, "Blue", delegate(float flt)
				{
					currentColor.b = Mathf.Floor(flt) / 255f;
					RenderColor();
				}, "Adjust the green value of the color", 255f, 0f, floor: true, percent: false);
				aSlider = new global::emmVRC.Utils.Slider(colorAdjustmentPage, "Alpha", delegate(float flt)
				{
					currentColor.a = Mathf.Floor(flt) / 100f;
					RenderColor();
				}, "Adjust the transparency of the color", 100f, 0f, floor: true, percent: false);
				SimpleSingleButton simpleSingleButton = new SimpleSingleButton(new ButtonGroup(colorAdjustmentPage, ""), "", null, "");
				UnityEngine.Object.DestroyImmediate(simpleSingleButton.gameObject.GetComponent<VRC.UI.Elements.Tooltips.UiTooltip>());
				UnityEngine.Object.DestroyImmediate(simpleSingleButton.gameObject.GetComponent<Button>());
				simpleSingleButton.gameObject.transform.DestroyChildren((Transform a) => !a.name.Contains("Foreground"));
				colorDemo = simpleSingleButton.gameObject.GetComponentInChildren<Image>(includeInactive: true);
				colorDemo.sprite = global::emmVRC.Functions.Core.Resources.TabIcon;
				simpleSingleButton.gameObject.transform.GetChild(0).gameObject.SetActive(value: true);
				_initialized = true;
			}
		}

		public static void ShowMenu(Color? defaultColor, Action<Color> result)
		{
			currentColor = defaultColor ?? Color.white;
			rSlider.SetValue(Mathf.Floor(currentColor.r * 255f));
			gSlider.SetValue(Mathf.Floor(currentColor.g * 255f));
			bSlider.SetValue(Mathf.Floor(currentColor.b * 255f));
			aSlider.SetValue(Mathf.Floor(currentColor.a * 255f));
			onColorAccepted = result;
			colorAdjustmentPage.OpenMenu();
			RenderColor();
		}

		private static void RenderColor()
		{
			colorDemo.color = currentColor;
		}
	}
}
