using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Functions.WorldHacks;
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class InstanceHistoryMenu : MelonLoaderEvents
	{
		public static MenuPage instanceHistoryPage;

		private static SingleButton instanceHistoryButton;

		private static ButtonGroup optionsGroup;

		private static SimpleSingleButton clearInstanceHistoryButton;

		private static ButtonGroup mainHistoryGroup;

		private static List<SimpleSingleButton> instanceHistoryButtons;

		private static List<ButtonGroup> instanceHistoryGroups;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			instanceHistoryPage = new MenuPage("emmVRC_InstanceHistory", "Instance History", root: false, backButton: true, extButton: true, delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowConfirmDialog("Instance History", "Are you sure you want to clear your instance history?", delegate
				{
					InstanceHistory.ClearInstances();
					instanceHistoryPage.CloseMenu();
					OpenMenu();
					ButtonAPI.GetQuickMenuInstance().ShowAlert("Instance History has been cleared");
				}, delegate
				{
				});
			}, "Clear your instance history", ButtonAPI.xIconSprite);
			instanceHistoryButton = new SingleButton(FunctionsMenu.featuresGroup, "Instance\nHistory", delegate
			{
				OpenMenu();
			}, "View all the instances you've been in, and join them if permitted", global::emmVRC.Functions.Core.Resources.WorldHistoryIcon);
			mainHistoryGroup = new ButtonGroup(instanceHistoryPage, "History");
			instanceHistoryButtons = new List<SimpleSingleButton>();
			instanceHistoryGroups = new List<ButtonGroup>();
			_initialized = true;
		}

		public static void OpenMenu()
		{
			foreach (SimpleSingleButton instanceHistoryButton in instanceHistoryButtons)
			{
				Object.Destroy(instanceHistoryButton.gameObject);
			}
			foreach (ButtonGroup instanceHistoryGroup in instanceHistoryGroups)
			{
				instanceHistoryGroup.Destroy();
			}
			instanceHistoryButtons.Clear();
			instanceHistoryGroups.Clear();
			int num = 0;
			foreach (SerializedWorld world in InstanceHistory.previousInstances)
			{
				if (num == 4)
				{
					instanceHistoryGroups.Add(new ButtonGroup(instanceHistoryPage, ""));
					num = 0;
				}
				else
				{
					num++;
				}
				SimpleSingleButton simpleSingleButton = new SimpleSingleButton(mainHistoryGroup, world.WorldName + "\n" + InstanceIDUtilities.GetInstanceID(world.WorldTags), delegate
				{
					VRCFlowManager.prop_VRCFlowManager_0.EnterWorld(world.WorldID, world.WorldTags);
				}, world.WorldName + (world.WorldTags.Contains("region(jp)") ? " [JP Region]" : (world.WorldTags.Contains("region(eu)") ? " [EU Region]" : "")) + " (" + InstanceHistory.PrettifyInstanceType(world.WorldType) + "), last joined " + UnixTime.ToDateTime(world.loggedDateTime).ToShortDateString() + " " + UnixTime.ToDateTime(world.loggedDateTime).ToShortTimeString() + "\nSelect to join");
				simpleSingleButton.gameObject.transform.SetAsFirstSibling();
				instanceHistoryButtons.Add(simpleSingleButton);
			}
			instanceHistoryPage.OpenMenu();
		}
	}
}
