using emmVRC.Components;
using emmVRC.Functions.Core;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
	[Priority(50)]
	public class FunctionsMenu : MelonLoaderEvents
	{
		public static MenuPage basePage;

		private static Tab mainTab;

		internal static ButtonGroup notificationsGroup;

		internal static ButtonGroup tweaksGroup;

		internal static ButtonGroup featuresGroup;

		internal static ButtonGroup otherGroup;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			basePage = new MenuPage("emmVRC_MainMenu", "emmVRC", root: true, backButton: false, extButton: true, delegate
			{
				ButtonAPI.GetQuickMenuInstance().AskConfirmOpenURL("https://discord.gg/emmVRC");
			}, "<color=#FF69B4>emmVRC</color> version " + Attributes.Version?.ToString() + ".\nClick to join our Discord!", global::emmVRC.Functions.Core.Resources.onlineSprite, preserveColor: true);
			mainTab = new Tab(ButtonAPI.menuTabBase.transform.parent, "emmVRC_MainMenu", "emmVRC", global::emmVRC.Functions.Core.Resources.TabIcon, delegate
			{
				if (Configuration.JSONConfig.AcceptedEULAVersion != "1.0.1")
				{
					ButtonAPI.menuTabBase.transform.parent.Find("Page_Dashboard").GetComponent<Button>().onClick.Invoke();
					ButtonAPI.GetQuickMenuInstance().ShowCustomDialog("Welcome to emmVRC!", (Configuration.JSONConfig.AcceptedEULAVersion == "0.0.0") ? "To use emmVRC, you must first read and agree to our End User License Agreement.\n\nThis will open in your web browser." : "We have updated our End User License Agreement. To continue, you must read and agree to it.\n\nThis will open in your web browser.", "Open EULA", "Agree", "Decline", delegate
					{
						Application.OpenURL("https://ttyf.me/emmvrceula");
					}, delegate
					{
						Configuration.WriteConfigOption("AcceptedEULAVersion", "1.0.1");
					});
				}
			});
			GameObject gameObject = new GameObject("ChangelogText");
			gameObject.transform.SetParent(basePage.menuContents);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
			gameObject.transform.localScale = Vector3.one;
			TextMeshProUGUI textText = gameObject.AddComponent<TextMeshProUGUI>();
			textText.margin = new Vector4(25f, 0f, 50f, 0f);
			textText.text = "Version " + Attributes.Version.ToString(3);
			textText.alignment = TextAlignmentOptions.Left;
			NetworkClient.onLogin += delegate
			{
				textText.text = "<align=\"left\">  Version " + Attributes.Version.ToString(3) + "<line-height=0>\n" + ((!Configuration.JSONConfig.emmVRCNetworkEnabled) ? "" : ((!string.IsNullOrEmpty(NetworkClient.webToken)) ? "<align=\"right\"><color=#00FF00>Connected to the emmVRC Network  </color>" : "<align=\"right\"><color=#FF0000>Not connected to the emmVRC Network  </color>"));
			};
			NetworkClient.onLogout += delegate
			{
				textText.text = "<align=\"left\">  Version " + Attributes.Version.ToString(3) + "<line-height=0>\n" + ((!Configuration.JSONConfig.emmVRCNetworkEnabled) ? "" : ((!string.IsNullOrEmpty(NetworkClient.webToken)) ? "<align=\"right\"><color=#00FF00>Connected to the emmVRC Network  </color>" : "<align=\"right\"><color=#FF0000>Not connected to the emmVRC Network  </color>"));
			};
			gameObject.AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				textText.text = "<align=\"left\">  Version " + Attributes.Version.ToString(3) + "<line-height=0>\n" + ((!Configuration.JSONConfig.emmVRCNetworkEnabled) ? "" : ((!string.IsNullOrEmpty(NetworkClient.webToken)) ? "<align=\"right\"><color=#00FF00>Connected to the emmVRC Network  </color>" : "<align=\"right\"><color=#FF0000>Not connected to the emmVRC Network  </color>"));
			};
			notificationsGroup = new ButtonGroup(basePage, "Notifications");
			tweaksGroup = new ButtonGroup(basePage.menuContents, "Tweaks");
			featuresGroup = new ButtonGroup(basePage.menuContents, "Features");
			new SingleButton(featuresGroup.gameObject.transform, "Alarm\nClocks", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowAlert("Not yet implemented");
			}, "Nothing here", global::emmVRC.Functions.Core.Resources.AlarmClockIcon);
			otherGroup = new ButtonGroup(basePage.menuContents, "Other");
			basePage.menuContents.parent.parent.parent.GetChild(0).Find("RightItemContainer/Button_QM_Expand/Icon").GetComponent<RectTransform>()
				.sizeDelta = new Vector2(72f, 72f);
			basePage.menuContents.parent.parent.parent.GetChild(0).Find("RightItemContainer/Button_QM_Expand/Icon").GetComponent<RectTransform>()
				.localPosition = new Vector3(0f, 8f, 0f);
			_initialized = true;
			emmVRCNotificationsManager.OnNotificationAdded += delegate
			{
				mainTab.SetBadge((emmVRCNotificationsManager.Notifications.Count != 0) ? true : false, emmVRCNotificationsManager.Notifications.Count + " NEW");
			};
			emmVRCNotificationsManager.OnNotificationRemoved += delegate
			{
				mainTab.SetBadge((emmVRCNotificationsManager.Notifications.Count != 0) ? true : false, emmVRCNotificationsManager.Notifications.Count + " NEW");
			};
		}
	}
}
