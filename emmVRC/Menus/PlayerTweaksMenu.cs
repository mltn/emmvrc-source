using System;
using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class PlayerTweaksMenu : MelonLoaderEvents
	{
		public static MenuPage playerTweaksPage;

		private static SingleButton playerTweaksButton;

		private static ButtonGroup avatarsGroup;

		private static SimpleSingleButton removeLoadedDynamicBonesButton;

		private static SimpleSingleButton reloadAllAvatarsButton;

		private static SimpleSingleButton avatarPermissions;

		private static SimpleSingleButton dynamicBoneOptions;

		private static ButtonGroup riskyFunctionsGroup;

		private static SimpleSingleButton jumpingToggleButton;

		private static SimpleSingleButton waypointsButton;

		private static SimpleButtonGroup riskyFunctionsGroup2;

		private static ToggleButton flightToggle;

		private static ToggleButton noclipToggle;

		private static ToggleButton speedToggle;

		private static ToggleButton espToggle;

		private static Slider speedSlider;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (_initialized)
			{
				jumpingToggleButton.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				waypointsButton.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				flightToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				noclipToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				speedToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				speedSlider.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
				espToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			}
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			playerTweaksPage = new MenuPage("emmVRC_PlayerTweaks", "Player Tweaks", root: false);
			playerTweaksButton = new SingleButton(FunctionsMenu.tweaksGroup, "Player", OpenMenu, "Functions that affect the current world you're in", global::emmVRC.Functions.Core.Resources.PlayerIcon);
			avatarsGroup = new ButtonGroup(playerTweaksPage, "Avatars");
			removeLoadedDynamicBonesButton = new SimpleSingleButton(avatarsGroup, "Remove\nLoaded\nDynanic\nBones", delegate
			{
				foreach (DynamicBone item in UnityEngine.Object.FindObjectsOfType<DynamicBone>())
				{
					UnityEngine.Object.Destroy(item);
				}
				foreach (DynamicBoneCollider item2 in UnityEngine.Object.FindObjectsOfType<DynamicBoneCollider>())
				{
					UnityEngine.Object.Destroy(item2);
				}
			}, "Unload all dynamic bones in the current instance");
			reloadAllAvatarsButton = new SimpleSingleButton(avatarsGroup, "Reload All\nAvatars", delegate
			{
				PlayerUtils.ReloadAllAvatars();
			}, "Reloads every avatar in the current instance, including the current one");
			avatarPermissions = new SimpleSingleButton(avatarsGroup, "Avatar\nPermissions", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowAlert("Not yet implemented");
			}, "Turn off components on a per-avatar basis");
			dynamicBoneOptions = new SimpleSingleButton(avatarsGroup, "Global\nDynamic\nBones", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowAlert("Not yet implemented");
			}, "Configure the Global Dynamic Bones system of emmVRC");
			riskyFunctionsGroup = new ButtonGroup(playerTweaksPage, "Risky Functions");
			jumpingToggleButton = new SimpleSingleButton(riskyFunctionsGroup, "Enable\nJumping", delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowAlert("Not yet implemented");
			}, "Enable jumping in the current world, if it isn't available already");
			waypointsButton = new SimpleSingleButton(riskyFunctionsGroup, "Waypoints", delegate
			{
				WaypointsMenu.OpenMenu();
			}, "Configure an unlimited amount of points in the world to teleport to");
			riskyFunctionsGroup2 = new SimpleButtonGroup(playerTweaksPage.menuContents, "");
			riskyFunctionsGroup2.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1160f, 220f);
			flightToggle = new ToggleButton(riskyFunctionsGroup, "Flight", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (!val && Flight.IsNoClipEnabled)
					{
						noclipToggle.SetToggleState(newState: false, invoke: true);
					}
					Flight.SetFlyActive(val);
				}
			}, "Fly around the world using your controllers or mouse + keyboard", "Stop using flight");
			noclipToggle = new ToggleButton(riskyFunctionsGroup, "Noclip", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (val && !Flight.IsFlyEnabled)
					{
						flightToggle.SetToggleState(newState: true, invoke: true);
					}
					Flight.SetNoClipActive(val);
				}
			}, "Clip through walls to access areas quicker, or find secrets. Requires flight", "Stop using noclip");
			speedToggle = new ToggleButton(riskyFunctionsGroup, "Speed", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					Speed.SetActive(val);
				}
			}, "Increase or decrease your movement speed", "Go back to the world's default speed");
			espToggle = new ToggleButton(riskyFunctionsGroup, "ESP", delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					ESP.SetActive(val);
				}
			}, "Emit a glow around players in the current instance", "Remove glow around surrounding players");
			speedSlider = new Slider(playerTweaksPage, "Current speed", delegate(float val)
			{
				Configuration.WriteConfigOption("SpeedModifier", val / 10f);
				if (Speed.IsEnabled)
				{
					Speed.SetActive(active: false);
					Speed.SetActive(active: true);
				}
			}, "Adjust the speed that you move at", Configuration.JSONConfig.MaxSpeedIncrease * 10f, Configuration.JSONConfig.SpeedModifier * 10f, floor: true, percent: false);
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("RiskyFunctionsEnabled", delegate
			{
			}));
			RiskyFunctionsManager.RiskyFuncsProcessed += delegate(bool val)
			{
				if (!val)
				{
					flightToggle.SetToggleState(newState: false, invoke: true);
					noclipToggle.SetToggleState(newState: false, invoke: true);
					espToggle.SetToggleState(newState: false, invoke: true);
				}
				jumpingToggleButton.SetInteractable(val);
				waypointsButton.SetInteractable(val);
				flightToggle.SetInteractable(val);
				noclipToggle.SetInteractable(val);
				speedToggle.SetInteractable(val);
				speedSlider.SetInteractable(val);
				espToggle.SetInteractable(val);
			};
			_initialized = true;
		}

		private static void OpenMenu()
		{
			flightToggle.SetToggleState(Flight.IsFlyEnabled);
			noclipToggle.SetToggleState(Flight.IsNoClipEnabled);
			speedToggle.SetToggleState(Speed.IsEnabled);
			espToggle.SetToggleState(ESP.IsEnabled);
			jumpingToggleButton.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			waypointsButton.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			flightToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			noclipToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			speedToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			speedSlider.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			espToggle.SetInteractable(Configuration.JSONConfig.RiskyFunctionsEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed);
			playerTweaksPage.OpenMenu();
		}
	}
}
