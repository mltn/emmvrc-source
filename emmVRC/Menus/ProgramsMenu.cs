using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Functions.Other;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class ProgramsMenu : MelonLoaderEvents
	{
		public static MenuPage programsPage;

		private static SingleButton programsButton;

		private static ButtonGroup mainProgramsGroup;

		private static List<SimpleSingleButton> programsButtons;

		private static List<ButtonGroup> programsGroups;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized)
			{
				programsPage = new MenuPage("emmVRC_Programs", "Programs", root: false);
				programsButton = new SingleButton(FunctionsMenu.featuresGroup, "Programs", delegate
				{
					OpenMenu();
				}, "Lets you launch external programs from within VRChat", global::emmVRC.Functions.Core.Resources.ProgramsIcon);
				mainProgramsGroup = new ButtonGroup(programsPage, "Programs");
				programsButtons = new List<SimpleSingleButton>();
				programsGroups = new List<ButtonGroup>();
				_initialized = true;
			}
		}

		public static void OpenMenu()
		{
			foreach (SimpleSingleButton programsButton in programsButtons)
			{
				Object.Destroy(programsButton.gameObject);
			}
			foreach (ButtonGroup programsGroup in programsGroups)
			{
				programsGroup.Destroy();
			}
			programsButtons.Clear();
			programsGroups.Clear();
			int num = 0;
			foreach (Program prgm in Programs.GetPrograms())
			{
				if (num == 4)
				{
					programsGroups.Add(new ButtonGroup(programsPage, ""));
					num = 0;
				}
				else
				{
					num++;
				}
				SimpleSingleButton item = new SimpleSingleButton(mainProgramsGroup, prgm.name, delegate
				{
					Programs.OpenProgram(prgm);
				}, prgm.toolTip);
				programsButtons.Add(item);
			}
			programsPage.OpenMenu();
		}
	}
}
