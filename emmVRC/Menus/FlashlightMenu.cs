using emmVRC.Functions.Core;
using emmVRC.Functions.WorldHacks;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;

namespace emmVRC.Menus
{
	[Priority(60)]
	public class FlashlightMenu : MelonLoaderEvents
	{
		public static MenuPage flashlightPage;

		private static SimpleSingleButton flashlightButton;

		private static ButtonGroup lightTogglesGroup;

		private static ToggleButton flashlightToggle;

		private static ToggleButton headlightToggle;

		private static ButtonGroup optionsGroup;

		private static SingleButton setColor;

		private static Slider rangeSlider;

		private static Slider powerSlider;

		private static Slider angleSlider;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			flashlightPage = new MenuPage("emmVRC_FlashlightMenu", "Flashlight", root: false);
			flashlightButton = new SimpleSingleButton(WorldTweaksMenu.objectsGroup, "Flashlight", OpenMenu, "Spawn a flashlight you can carry around, or use the headlight to light dark areas");
			lightTogglesGroup = new ButtonGroup(flashlightPage, "Toggles");
			flashlightToggle = new ToggleButton(lightTogglesGroup, "Flashlight", delegate(bool val)
			{
				Flashlight.SetFlashlightActive(val);
			}, "Spawn a flashlight in the world", "Remove the flashlight in the world");
			headlightToggle = new ToggleButton(lightTogglesGroup, "Headlight", delegate(bool val)
			{
				Flashlight.SetHeadlightActive(val);
			}, "Turn on a flashlight attached to your head", "Turn off the flashlight attacked to your head");
			rangeSlider = new Slider(flashlightPage, "Range", delegate(float val)
			{
				Configuration.WriteConfigOption("FlashlightRange", val);
			}, "", 20f, Configuration.JSONConfig.FlashlightRange);
			powerSlider = new Slider(flashlightPage, "Power", delegate(float val)
			{
				Configuration.WriteConfigOption("FlashlightPower", val);
			}, "", 2f, Configuration.JSONConfig.FlashlightPower);
			angleSlider = new Slider(flashlightPage, "Angle", delegate(float val)
			{
				Configuration.WriteConfigOption("FlashlightAngle", val);
			}, "", 180f, Configuration.JSONConfig.FlashlightAngle);
			optionsGroup = new ButtonGroup(flashlightPage, "");
			setColor = new SingleButton(optionsGroup, "Light\nColor", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("FlashlightColorHex", ColorConversion.ColorToHex(col, hash: true));
					setColor.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex));
				});
			}, "Adjust the color of the light from the flashlight/headlight", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			setColor.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex));
			_initialized = true;
		}

		private static void OpenMenu()
		{
			flashlightPage.OpenMenu();
		}
	}
}
