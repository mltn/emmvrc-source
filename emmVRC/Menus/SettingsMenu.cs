using System;
using System.Collections.Generic;
using System.IO;
using emmVRC.Functions.Core;
using emmVRC.Functions.UI;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Menus
{
	[Priority(50)]
	public class SettingsMenu : MelonLoaderEvents
	{
		private static MenuPage settingsPage;

		private static SingleButton settingsButton;

		private static ButtonGroup featuresGroup;

		private static ToggleButton riskyFunctionsToggle;

		private static ToggleButton globalDynamicBonesToggle;

		private static ToggleButton friendGlobalDynamicBonesToggle;

		private static ToggleButton everyoneGlobalDynamicBonesToggle;

		private static ToggleButton vrFlightControlsToggle;

		private static ToggleButton trackingSavingToggle;

		private static ToggleButton actionMenuIntegrationToggle;

		private static ToggleButton emojiFavoriteMenuToggle;

		private static ToggleButton clockToggle;

		private static ToggleButton masterIconToggle;

		private static ToggleButton hudToggle;

		private static ToggleButton forceRestartToggle;

		private static ToggleButton unlimitedFPSToggle;

		private static ButtonGroup modIntegrationsGroup;

		private static ToggleButton uiExpansionKitToggle;

		private static ButtonGroup networkGroup;

		private static ToggleButton emmVRCNetworkToggle;

		private static ToggleButton avatarFavoriteListToggle;

		private static ToggleButton avatarFavoriteJumpToggle;

		private static ToggleButton submitAvatarPedestalsToggle;

		private static ButtonGroup colorGroup;

		private static ToggleButton uiColorChangingToggle;

		private static SingleButton uiColorButton;

		private static ToggleButton uiActionMenuColorChangingToggle;

		private static ToggleButton uiMicIconColorChangingToggle;

		private static ToggleButton uiMicIconPulseToggle;

		private static ToggleButton uiExpansionKitColorChangingToggle;

		private static ToggleButton nameplateColorChangingToggle;

		private static MenuPage colorChangingPage;

		private static SimpleSingleButton colorChoiceButton;

		private static SingleButton friendNameplateColorButton;

		private static SingleButton visitorNameplateColorButton;

		private static SingleButton newUserNameplateColorButton;

		private static SingleButton userNameplateColorButton;

		private static SingleButton knownUserNameplateColorButton;

		private static SingleButton trustedUserNameplateColorButton;

		private static SingleButton veteranUserNameplateColorButton;

		private static SingleButton legendaryUserNameplateColorButton;

		private static ButtonGroup vrchatUiGroup;

		private static ToggleButton upperInviteButtonsToggle;

		private static ToggleButton oneHandMovementToggle;

		private static ToggleButton micTooltipToggle;

		private static ToggleButton vrcPlusMenuTabsToggle;

		private static ToggleButton vrcPlusUserInfoToggle;

		private static ButtonGroup avatarListsGroup;

		private static ToggleButton avatarHotWorldsToggle;

		private static ToggleButton avatarRandomWorldsToggle;

		private static ToggleButton avatarPersonalListToggle;

		private static ToggleButton avatarLegacyListToggle;

		private static ToggleButton avatarPublicListToggle;

		private static ToggleButton avatarOtherListToggle;

		private static ButtonGroup keybindsGroup;

		private static ToggleButton keybindsToggle;

		private static TextButton flightKeybindButton;

		private static TextButton noclipKeybindButton;

		private static TextButton speedKeybindButton;

		private static TextButton thirdPersonKeybindButton;

		private static TextButton hudEnabledKeybindButton;

		private static TextButton respawnKeybindButton;

		private static TextButton goHomeKeybindButton;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			settingsPage = new MenuPage("emmVRC_Settings", "Settings", root: false, backButton: true, extButton: true, delegate
			{
				ButtonAPI.GetQuickMenuInstance().ShowConfirmDialog("emmVRC", "Would you like to export your emmVRC Favorite Avatars?", delegate
				{
					List<ExportedAvatar> list = new List<ExportedAvatar>();
					Enumerator<ApiAvatar> enumerator = CustomAvatarFavorites.LoadedAvatars.GetEnumerator();
					while (enumerator.MoveNext())
					{
						ApiAvatar current = enumerator.get_Current();
						list.Add(new ExportedAvatar
						{
							avatar_id = current.id,
							avatar_name = current.name
						});
					}
					File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/ExportedList.json"), Encoder.Encode(list, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
					emmVRCNotificationsManager.AddNotification(new Notification("Avatar Export", global::emmVRC.Functions.Core.Resources.alertSprite, "Your emmVRC Favorite list has been exported to UserData/emmVRC/ExportedList.json", canIgnore: true, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
				});
			}, "Export your emmVRC Avatar Favorites to a file");
			settingsButton = new SingleButton(FunctionsMenu.otherGroup, "Settings", OpenMenu, "", global::emmVRC.Functions.Core.Resources.SettingsIcon);
			featuresGroup = new ButtonGroup(settingsPage, "Features");
			riskyFunctionsToggle = new ToggleButton(featuresGroup, "Risky Functions", delegate(bool val)
			{
				if (!Configuration.JSONConfig.RiskyFunctionsWarningShown && !Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					ButtonAPI.GetQuickMenuInstance().ShowCustomDialog("Risky Functions", "By selecting 'yes', you accept that the use of these functions could be reported to VRChat, and that you will not use them for malicious purposes.", "", "Yes", "No", null, delegate
					{
						Configuration.WriteConfigOption("RiskyFunctionsEnabled", val);
						Configuration.WriteConfigOption("RiskyFunctionsWarningShown", true);
					}, delegate
					{
						riskyFunctionsToggle.SetToggleState(newState: false);
					});
				}
				else
				{
					Configuration.WriteConfigOption("RiskyFunctionsEnabled", val);
				}
			}, "Enables Risky Functions, which allows use of modules such as Flight and Speed in approved worlds", "Disables Risky Functions");
			globalDynamicBonesToggle = new ToggleButton(featuresGroup, "Global\nDynamic Bones", delegate(bool val)
			{
				Configuration.WriteConfigOption("GlobalDynamicBonesEnabled", val);
			}, "Enables Global Dynamic Bones, allowing you to interact with other users' dynamic bones", "Disables Global Dynamic Bones");
			friendGlobalDynamicBonesToggle = new ToggleButton(featuresGroup, "Friend Global\nDynamics", delegate(bool val)
			{
				Configuration.WriteConfigOption("FriendGlobalDynamicBonesEnabled", val);
			}, "Turns on Global Dynamic Bones automatically for friends", "Do not turn on Global Dynamic Bones automatically for friends");
			everyoneGlobalDynamicBonesToggle = new ToggleButton(featuresGroup, "Everyone Global\nDynamics", delegate(bool val)
			{
				Configuration.WriteConfigOption("EveryoneGlobalDynamicBonesEnabled", val);
			}, "Turns on Global Dynamic Bones for everyone. This can cause lag!", "Do not enable Global Dynamic Bones for everyone");
			new ButtonGroup(settingsPage, "");
			vrFlightControlsToggle = new ToggleButton(featuresGroup, "VR Flight\nControls", delegate(bool val)
			{
				Configuration.WriteConfigOption("VRFlightControls", val);
			}, "Use VR-friendly flight controls", "Use desktop-mode flight controls");
			trackingSavingToggle = new ToggleButton(featuresGroup, "Calibration\nSaving", delegate(bool val)
			{
				Configuration.WriteConfigOption("TrackingSaving", val);
			}, "Enables saving of FBT calibration", "Disables saving of FBT calibration");
			actionMenuIntegrationToggle = new ToggleButton(featuresGroup, "Action Menu\nIntegration", delegate(bool val)
			{
				Configuration.WriteConfigOption("ActionMenuIntegration", val);
			}, "Enables emmVRC Action Menu Integration", "Disables emmVRC Action Menu Integration");
			clockToggle = new ToggleButton(featuresGroup, "Clock", delegate(bool val)
			{
				Configuration.WriteConfigOption("ClockEnabled", val);
			}, "Enables the Quick Menu clock, which shows both the local time and the current time spent in this instance", "Disables the Quick Menu clock");
			new ButtonGroup(settingsPage, "");
			masterIconToggle = new ToggleButton(featuresGroup, "Master Icon", delegate(bool val)
			{
				Configuration.WriteConfigOption("MasterIconEnabled", val);
			}, "Enables the Master Icon, which shows above the current master of the instance", "Disables the Master Icon");
			hudToggle = new ToggleButton(featuresGroup, "HUD", delegate(bool val)
			{
				Configuration.WriteConfigOption("HUDEnabled", val);
			}, "Enables the desktop HUD, which shows information such as the current position and players in the instance", "Disable the desktop HUD");
			forceRestartToggle = new ToggleButton(featuresGroup, "Force Restart", delegate(bool val)
			{
				Configuration.WriteConfigOption("ForceRestartButtonEnabled", val);
			}, "Enables the Force Restart button in loading screens", "Disables the Force Restart button in loading screens");
			unlimitedFPSToggle = new ToggleButton(featuresGroup, "Unlimited\nFPS", delegate(bool val)
			{
				Configuration.WriteConfigOption("UnlimitedFPSEnabled", val);
			}, "Removes the FPS limiter, which by default limits your FPS to 90 (Desktop only!)", "Enables the FPS limiter (Desktop only!)");
			modIntegrationsGroup = new ButtonGroup(settingsPage, "Mod Integrations");
			uiExpansionKitToggle = new ToggleButton(modIntegrationsGroup, "UIExpansionKit\nIntegration", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIExpansionKitIntegration", val);
			}, "Enables Risky Functions buttons in the UIExpansionKit quick menu", "Disables Risky Functions buttons in the UIExpansionKit quick menu");
			networkGroup = new ButtonGroup(settingsPage, "Network");
			emmVRCNetworkToggle = new ToggleButton(networkGroup, "emmVRC\nNetwork", delegate(bool val)
			{
				Configuration.WriteConfigOption("emmVRCNetworkEnabled", val);
			}, "Enables the emmVRC Network, which provides features like Avatar Favorites", "Disables the emmVRC Network");
			avatarFavoriteListToggle = new ToggleButton(networkGroup, "Avatar\nFavorites", delegate(bool val)
			{
				Configuration.WriteConfigOption("AvatarFavoritesEnabled", val);
			}, "Enables emmVRC Avatar Favorites, which allows to search our database of avatars, and if you have VRChat Plus, favorite an unlimited number of avatars", "Disables emmVRC Avatar Favorites");
			avatarFavoriteJumpToggle = new ToggleButton(networkGroup, "Jump to\nStart", delegate(bool val)
			{
				Configuration.WriteConfigOption("AvatarFavoritesJumpToStart", val);
			}, "In the emmVRC Favorites, automatically jumps back to the start when switching pages or reloading favorites", "In the emmVRC Favorites, automatically jumps back to the start when switching pages or reloading favorites");
			submitAvatarPedestalsToggle = new ToggleButton(networkGroup, "Submit Public\nAvatar Pedestals", delegate(bool val)
			{
				Configuration.WriteConfigOption("SubmitAvatarPedestals", val);
			}, "Automatically submit public pedestals in the world to be included in emmVRC Search", "Do not automatically submit public pedestals in the world to be included in emmVRC Search");
			colorGroup = new ButtonGroup(settingsPage, "Coloring");
			uiColorChangingToggle = new ToggleButton(colorGroup, "UI Coloring", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIColorChangingEnabled", val);
			}, "Enable coloring the VRChat UI", "Disable coloring the VRChat UI");
			uiColorButton = new SingleButton(colorGroup, "UI\nColor", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("UIColorHex", ColorConversion.ColorToHex(col));
					uiColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex));
				});
			}, "Choose the color for UI coloring", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			uiActionMenuColorChangingToggle = new ToggleButton(colorGroup, "Action Menu\nColoring", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIActionMenuColorChangingEnabled", val);
			}, "Enable coloring of the Action Menu", "Disable coloring of the Action Menu");
			uiMicIconColorChangingToggle = new ToggleButton(colorGroup, "Microphone\nIcon Coloring", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIMicIconColorChangingEnabled", val);
			}, "Color the microphone icon", "Do not color the microphone icon");
			new ButtonGroup(settingsPage, "");
			uiMicIconPulseToggle = new ToggleButton(colorGroup, "Microphone\nIcon Pulsing", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIMicIconPulsingEnabled", val);
			}, "Disable the pulsing of the microphone icon", "Enable the pulsing of the microphone icon");
			nameplateColorChangingToggle = new ToggleButton(colorGroup, "Nameplate\nColoring", delegate(bool val)
			{
				Configuration.WriteConfigOption("NameplateColorChangingEnabled", val);
			}, "Color the nameplates of other users", "Do not color nameplates");
			uiExpansionKitColorChangingToggle = new ToggleButton(colorGroup, "UIExpansionKit\nColoring", delegate(bool val)
			{
				Configuration.WriteConfigOption("UIExpansionKitColorChangingEnabled", val);
			}, "Enable coloring of the UI Expansion Kit menus", "Do not color UI Expansion Kit menus");
			colorChangingPage = new MenuPage("emmVRC_Settings_NameplateColors", "Nameplate Colors", root: false);
			colorChoiceButton = new SimpleSingleButton(colorGroup, "Nameplate\nColors", colorChangingPage.OpenMenu, "Configure the colors of the VRChat nameplates");
			ButtonGroup grp = new ButtonGroup(colorChangingPage, "");
			friendNameplateColorButton = new SingleButton(grp, "Friends", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("FriendNamePlateColorHex", ColorConversion.ColorToHex(col));
					friendNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for friends", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			visitorNameplateColorButton = new SingleButton(grp, "Visitor", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.VisitorNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("VisitorNamePlateColorHex", ColorConversion.ColorToHex(col));
					visitorNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for visitors", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			newUserNameplateColorButton = new SingleButton(grp, "New User", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("NewUserNamePlateColorHex", ColorConversion.ColorToHex(col));
					newUserNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for new users", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			userNameplateColorButton = new SingleButton(grp, "User", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("UserNamePlateColorHex", ColorConversion.ColorToHex(col));
					userNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for users", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			knownUserNameplateColorButton = new SingleButton(grp, "Known User", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("KnownUserNamePlateColorHex", ColorConversion.ColorToHex(col));
					knownUserNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for known users", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			trustedUserNameplateColorButton = new SingleButton(grp, "Trusted User", delegate
			{
				ColorAdjustmentMenu.ShowMenu(ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex), delegate(Color col)
				{
					Configuration.WriteConfigOption("TrustedUserNamePlateColorHex", ColorConversion.ColorToHex(col));
					trustedUserNameplateColorButton.SetIconColor(col);
				});
			}, "Configure the nameplate color for trusted users", global::emmVRC.Functions.Core.Resources.TabIcon, preserveColor: true);
			vrchatUiGroup = new ButtonGroup(settingsPage, "VRChat UI");
			upperInviteButtonsToggle = new ToggleButton(vrchatUiGroup, "Upper Invite\nButtons", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableOldInviteButtons", !val);
			}, "Disable the invite buttons along the top of the Quick Menu", "Enable the invite buttons along the top of the Quick Menu");
			oneHandMovementToggle = new ToggleButton(vrchatUiGroup, "One handed\nMovement", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableOneHandMovement", !val);
			}, "Disable the one handed movement guide that appears when the action menu is open", "Enable the one handed movement guide that appears when the action menu is open");
			micTooltipToggle = new ToggleButton(vrchatUiGroup, "Microphone\nTooltip", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableMicTooltip", !val);
			}, "Disable the microphone keybind tooltip above the microphone icon", "Enable the microphone keybind tooltip above the microphone icon");
			vrcPlusMenuTabsToggle = new ToggleButton(vrchatUiGroup, "VRChat+\nMenu Tabs", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableVRCPlusMenuTabs", !val);
			}, "Disable the Gallery and VRC+ tabs in the Main Menu", "Enable the Gallery and VRC+ tabs in the Main Menu");
			new ButtonGroup(settingsPage, "");
			vrcPlusUserInfoToggle = new ToggleButton(vrchatUiGroup, "VRChat+\nUser Info", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableVRCPlusUserInfo", !val);
			}, "Disable the VRChat Plus information in the User Info page of the Main Menu", "Enable the VRChat Plus information in the User Info page of the Main Menu");
			avatarListsGroup = new ButtonGroup(settingsPage, "Avatar Lists");
			avatarHotWorldsToggle = new ToggleButton(avatarListsGroup, "Hot Worlds\nCategory", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarHotWorlds", !val);
			}, "Disable the \"Hot Worlds\" category in the Avatar Menu", "Enable the \"Hot Worlds\" category in the Avatar Menu");
			avatarRandomWorldsToggle = new ToggleButton(avatarListsGroup, "Random\nCategory", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarRandomWorlds", !val);
			}, "Disable the \"Random\" category in the Avatar Menu", "Enable the \"Random Worlds\" category in the Avatar Menu");
			avatarPersonalListToggle = new ToggleButton(avatarListsGroup, "Personal\nList", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarPersonal", !val);
			}, "Disable the \"Personal\" list in the Avatar Menu", "Enable the \"Personal\" list in the Avatar Menu");
			avatarLegacyListToggle = new ToggleButton(avatarListsGroup, "Legacy\nList", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarLegacy", !val);
			}, "Disable the \"Legacy\" list in the Avatar Menu", "Enable the \"Legacy\" list in the Avatar Menu");
			new ButtonGroup(settingsPage, "");
			avatarPublicListToggle = new ToggleButton(avatarListsGroup, "Public\nList", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarPublic", !val);
			}, "Disable the \"Public\" list in the Avatar Menu", "Enable the \"Public\" list in the Avatar Menu");
			avatarOtherListToggle = new ToggleButton(avatarListsGroup, "Other\nList", delegate(bool val)
			{
				Configuration.WriteConfigOption("DisableAvatarOther", !val);
			}, "Disable the \"Other\" list in the Avatar Menu", "Enable the \"Other\" list in the Avatar Menu");
			_initialized = true;
		}

		private static void OpenMenu()
		{
			settingsPage.OpenMenu();
			riskyFunctionsToggle.SetToggleState(Configuration.JSONConfig.RiskyFunctionsEnabled);
			globalDynamicBonesToggle.SetToggleState(Configuration.JSONConfig.GlobalDynamicBonesEnabled);
			friendGlobalDynamicBonesToggle.SetToggleState(Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled);
			everyoneGlobalDynamicBonesToggle.SetToggleState(Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled);
			vrFlightControlsToggle.SetToggleState(Configuration.JSONConfig.VRFlightControls);
			trackingSavingToggle.SetToggleState(Configuration.JSONConfig.TrackingSaving);
			actionMenuIntegrationToggle.SetToggleState(Configuration.JSONConfig.ActionMenuIntegration);
			clockToggle.SetToggleState(Configuration.JSONConfig.ClockEnabled);
			masterIconToggle.SetToggleState(Configuration.JSONConfig.MasterIconEnabled);
			hudToggle.SetToggleState(Configuration.JSONConfig.HUDEnabled);
			forceRestartToggle.SetToggleState(Configuration.JSONConfig.ForceRestartButtonEnabled);
			unlimitedFPSToggle.SetToggleState(Configuration.JSONConfig.UnlimitedFPSEnabled);
			modIntegrationsGroup.SetActive(ModCompatibility.UIExpansionKit);
			if (ModCompatibility.UIExpansionKit)
			{
				uiExpansionKitToggle.SetToggleState(Configuration.JSONConfig.UIExpansionKitIntegration);
			}
			uiExpansionKitToggle.SetActive(ModCompatibility.UIExpansionKit);
			emmVRCNetworkToggle.SetToggleState(Configuration.JSONConfig.emmVRCNetworkEnabled);
			avatarFavoriteListToggle.SetToggleState(Configuration.JSONConfig.AvatarFavoritesEnabled);
			avatarFavoriteJumpToggle.SetToggleState(Configuration.JSONConfig.AvatarFavoritesJumpToStart);
			submitAvatarPedestalsToggle.SetToggleState(Configuration.JSONConfig.SubmitAvatarPedestals);
			uiColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex));
			uiColorChangingToggle.SetToggleState(Configuration.JSONConfig.UIColorChangingEnabled);
			uiActionMenuColorChangingToggle.SetToggleState(Configuration.JSONConfig.UIActionMenuColorChangingEnabled);
			uiMicIconColorChangingToggle.SetToggleState(Configuration.JSONConfig.UIMicIconColorChangingEnabled);
			uiMicIconPulseToggle.SetToggleState(Configuration.JSONConfig.UIMicIconPulsingEnabled);
			if (ModCompatibility.UIExpansionKit)
			{
				uiExpansionKitColorChangingToggle.SetToggleState(Configuration.JSONConfig.UIExpansionKitColorChangingEnabled);
				uiExpansionKitColorChangingToggle.gameObject.SetActive(value: true);
			}
			else
			{
				uiExpansionKitColorChangingToggle.gameObject.SetActive(value: false);
			}
			nameplateColorChangingToggle.SetToggleState(Configuration.JSONConfig.NameplateColorChangingEnabled);
			friendNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex));
			visitorNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.VisitorNamePlateColorHex));
			newUserNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex));
			userNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex));
			knownUserNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex));
			trustedUserNameplateColorButton.SetIconColor(ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex));
			upperInviteButtonsToggle.SetToggleState(!Configuration.JSONConfig.DisableOldInviteButtons);
			oneHandMovementToggle.SetToggleState(!Configuration.JSONConfig.DisableOneHandMovement);
			micTooltipToggle.SetToggleState(!Configuration.JSONConfig.DisableMicTooltip);
			vrcPlusMenuTabsToggle.SetToggleState(!Configuration.JSONConfig.DisableVRCPlusMenuTabs);
			vrcPlusUserInfoToggle.SetToggleState(!Configuration.JSONConfig.DisableVRCPlusUserInfo);
			avatarHotWorldsToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarHotWorlds);
			avatarRandomWorldsToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarRandomWorlds);
			avatarPersonalListToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarPersonal);
			avatarLegacyListToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarLegacy);
			avatarPublicListToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarPublic);
			avatarOtherListToggle.SetToggleState(!Configuration.JSONConfig.DisableAvatarOther);
		}
	}
}
