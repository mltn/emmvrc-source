using System;
using System.Collections;
using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem;
using MelonLoader;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Menus
{
	[Priority(55)]
	public class PlayerHistoryMenu : MelonLoaderEvents
	{
		public static MenuPage playerHistoryPage;

		private static SingleButton playerHistoryButton;

		private static ButtonGroup optionsGroup;

		private static ToggleButton historyToggle;

		private static ToggleButton logPlayerToggle;

		private static ButtonGroup mainHistoryGroup;

		private static int Timeout;

		private static List<SimpleSingleButton> playerHistoryButtons;

		private static List<ButtonGroup> playerHistoryGroups;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			playerHistoryPage = new MenuPage("emmVRC_PlayerHistory", "Player History", root: false);
			playerHistoryButton = new SingleButton(FunctionsMenu.featuresGroup, "Player\nHistory", delegate
			{
				OpenMenu();
			}, "View all the players that have been in your instance since you joined", global::emmVRC.Functions.Core.Resources.PlayerHistoryIcon);
			optionsGroup = new ButtonGroup(playerHistoryPage, "Options");
			historyToggle = new ToggleButton(optionsGroup, "Player\nHistory", delegate(bool val)
			{
				Configuration.WriteConfigOption("PlayerHistoryEnable", val);
				if (!val)
				{
					foreach (SimpleSingleButton playerHistoryButton in playerHistoryButtons)
					{
						UnityEngine.Object.Destroy(playerHistoryButton.gameObject);
					}
					foreach (ButtonGroup playerHistoryGroup in playerHistoryGroups)
					{
						playerHistoryGroup.Destroy();
					}
				}
			}, "Enable player history for this menu", "Disable player history for this menu");
			logPlayerToggle = new ToggleButton(optionsGroup, "Log Join\nand Leaves", delegate(bool val)
			{
				Configuration.WriteConfigOption("LogPlayerJoin", val);
			}, "Enable logging of joined players to the console", "Disable logging of joined players to the console");
			mainHistoryGroup = new ButtonGroup(playerHistoryPage, "History");
			playerHistoryButtons = new List<SimpleSingleButton>();
			playerHistoryGroups = new List<ButtonGroup>();
			_initialized = true;
		}

		private static void OpenMenu()
		{
			foreach (SimpleSingleButton playerHistoryButton in playerHistoryButtons)
			{
				UnityEngine.Object.Destroy(playerHistoryButton.gameObject);
			}
			foreach (ButtonGroup playerHistoryGroup in playerHistoryGroups)
			{
				playerHistoryGroup.Destroy();
			}
			int num = 0;
			foreach (InstancePlayer plr in PlayerHistory.currentPlayers)
			{
				if (num == 4)
				{
					playerHistoryGroups.Add(new ButtonGroup(playerHistoryPage, ""));
					num = 0;
				}
				else
				{
					num++;
				}
				SimpleSingleButton simpleSingleButton = new SimpleSingleButton(mainHistoryGroup, plr.Name, delegate
				{
					if (Timeout == 0 && NetworkConfig.Instance.APICallsAllowed)
					{
						APIUser.FetchUser(plr.UserID, Action<APIUser>.op_Implicit((Action<APIUser>)delegate(APIUser usr)
						{
							ButtonAPI.GetQuickMenuInstance().OpenUser(usr);
						}), Action<string>.op_Implicit((Action<string>)delegate(string str)
						{
							emmVRCLoader.Logger.LogError("API returned an error: " + str);
						}));
						Timeout = 5;
						MelonCoroutines.Start(WaitForTimeout());
					}
				}, "Joined " + plr.TimeJoinedStamp);
				simpleSingleButton.gameObject.transform.SetAsFirstSibling();
				playerHistoryButtons.Add(simpleSingleButton);
			}
			playerHistoryPage.OpenMenu();
		}

		private static IEnumerator WaitForTimeout()
		{
			while (Timeout != 0)
			{
				yield return new WaitForSeconds(1f);
				Timeout--;
			}
		}
	}
}
