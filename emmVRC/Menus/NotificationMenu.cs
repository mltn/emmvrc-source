using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VRC.UI.Elements.Tooltips;

namespace emmVRC.Menus
{
	[Priority(51)]
	public class NotificationMenu : MelonLoaderEvents
	{
		private static MenuPage notificationViewPage;

		private static SimpleSingleButton textView;

		private static TextMeshProUGUI textMeshPro;

		private static ButtonGroup notificationActionGroup;

		private static SimpleSingleButton acceptButton;

		private static SimpleSingleButton ignoreButton;

		private static SingleButton[] notificationButtons;

		private static Notification currentNotification;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1 || _initialized)
			{
				return;
			}
			notificationButtons = new SingleButton[4]
			{
				new SingleButton(FunctionsMenu.notificationsGroup, "", null, ""),
				new SingleButton(FunctionsMenu.notificationsGroup, "", null, ""),
				new SingleButton(FunctionsMenu.notificationsGroup, "", null, ""),
				new SingleButton(FunctionsMenu.notificationsGroup, "", null, "")
			};
			notificationViewPage = new MenuPage("emmVRC_NotificationView", "emmVRC Notification", root: false);
			textView = new SimpleSingleButton(notificationViewPage, "This is a long string of text that can easily go on to the next line and beyond!", null, "");
			Object.DestroyImmediate(textView.gameObject.GetComponent<Button>());
			Object.DestroyImmediate(textView.gameObject.GetComponent<VRC.UI.Elements.Tooltips.UiTooltip>());
			textView.gameObject.transform.DestroyChildren((Transform a) => !a.name.Contains("Text"));
			textMeshPro = textView.gameObject.transform.GetComponentInChildren<TextMeshProUGUI>();
			textMeshPro.GetComponent<RectTransform>().offsetMin = new Vector2(-500f, -150f);
			textMeshPro.GetComponent<RectTransform>().offsetMax = new Vector2(500f, 150f);
			textMeshPro.color = Configuration.menuColor();
			new ButtonGroup(notificationViewPage, "");
			notificationActionGroup = new ButtonGroup(notificationViewPage, "");
			acceptButton = new SimpleSingleButton(notificationActionGroup, "", null, "");
			ignoreButton = new SimpleSingleButton(notificationActionGroup, "", null, "");
			emmVRCNotificationsManager.OnNotificationAdded += delegate
			{
				UpdateNotificationGroup();
			};
			emmVRCNotificationsManager.OnNotificationRemoved += delegate
			{
				UpdateNotificationGroup();
				if (notificationViewPage.menuContents.gameObject.activeInHierarchy)
				{
					notificationViewPage.CloseMenu();
				}
			};
			_initialized = true;
			UpdateNotificationGroup();
		}

		private static void UpdateNotificationGroup()
		{
			SingleButton[] array = notificationButtons;
			for (int i = 0; i < array.Length; i++)
			{
				array[i].gameObject.SetActive(value: false);
			}
			FunctionsMenu.notificationsGroup.SetActive(state: false);
			if (emmVRCNotificationsManager.Notifications.Count >= 4 || emmVRCNotificationsManager.Notifications.Count <= 0)
			{
				return;
			}
			FunctionsMenu.notificationsGroup.SetActive(state: true);
			for (int j = 0; j < emmVRCNotificationsManager.Notifications.Count; j++)
			{
				Notification notif = emmVRCNotificationsManager.Notifications[j];
				notificationButtons[j].gameObject.SetActive(value: true);
				notificationButtons[j].SetText(emmVRCNotificationsManager.Notifications[j].name);
				notificationButtons[j].SetIconColor(Color.white);
				notificationButtons[j].SetIcon(emmVRCNotificationsManager.Notifications[j].icon);
				notificationButtons[j].SetAction(delegate
				{
					currentNotification = notif;
					notificationViewPage.OpenMenu();
					textView.SetText(notif.content);
					acceptButton.gameObject.SetActive(notif.showAcceptButton);
					ignoreButton.gameObject.SetActive(notif.showIgnoreButton);
					acceptButton.SetText(notif.acceptButtonText);
					ignoreButton.SetText(notif.ignoreButtonText);
					acceptButton.SetAction(delegate
					{
						notificationViewPage.CloseMenu();
						if (notif.acceptButton != null)
						{
							notif.acceptButton();
						}
						emmVRCNotificationsManager.RemoveNotification(notif);
					});
					ignoreButton.SetAction(delegate
					{
						notificationViewPage.CloseMenu();
						if (notif.ignoreButton != null)
						{
							notif.ignoreButton();
						}
						emmVRCNotificationsManager.RemoveNotification(notif);
					});
					acceptButton.SetTooltip(notif.acceptButtonTooltip);
					ignoreButton.SetTooltip(notif.ignoreButtonTooltip);
				});
			}
		}
	}
}
