using System;

namespace emmVRC.Objects
{
	public struct CachedValue<T>
	{
		public T value;

		public DateTime cachedTime;

		public int maxAge;

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (obj is T)
			{
				return obj.Equals(value);
			}
			if (obj is CachedValue<T>)
			{
				return ((CachedValue<T>)obj).value.Equals(value);
			}
			return false;
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}

		public CachedValue(T value, DateTime? cachedTime = null, int maxAge = 3600)
		{
			if (value == null)
			{
				throw new ArgumentNullException("Value cannot be null");
			}
			if (!cachedTime.HasValue)
			{
				cachedTime = DateTime.Now;
			}
			this.value = value;
			this.cachedTime = cachedTime.Value;
			this.maxAge = maxAge;
		}

		public bool Validate(DateTime? currentTime = null)
		{
			if (!currentTime.HasValue)
			{
				currentTime = DateTime.Now;
			}
			return (currentTime.Value - cachedTime).TotalSeconds < (double)maxAge;
		}

		public static implicit operator T(CachedValue<T> value)
		{
			return value.value;
		}

		public static implicit operator CachedValue<T>(T value)
		{
			return new CachedValue<T>(value);
		}
	}
}
