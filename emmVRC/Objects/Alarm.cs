namespace emmVRC.Objects
{
	public class Alarm
	{
		public string Name = "New Alarm";

		public bool IsEnabled;

		public long Time;

		public bool Repeats;

		public bool IsSystemTime = true;

		public float Volume = 0.5f;

		public readonly int Id;

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Alarm alarm = obj as Alarm;
			if (alarm != null)
			{
				return alarm.Id == Id;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public Alarm()
		{
		}

		public Alarm(int id)
		{
			Id = id;
		}
	}
}
