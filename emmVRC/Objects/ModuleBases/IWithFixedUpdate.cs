namespace emmVRC.Objects.ModuleBases
{
	internal interface IWithFixedUpdate
	{
		void OnFixedUpdate();
	}
}
