namespace emmVRC.Objects.ModuleBases
{
	internal interface IWithLateUpdate
	{
		void LateUpdate();
	}
}
