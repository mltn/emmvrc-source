using System;

namespace emmVRC.Objects.ModuleBases
{
	public class PriorityAttribute : Attribute
	{
		public readonly int priority;

		public PriorityAttribute(int priority)
		{
			this.priority = priority;
		}
	}
}
