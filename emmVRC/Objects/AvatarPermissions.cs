namespace emmVRC.Objects
{
	public class AvatarPermissions
	{
		public string AvatarId;

		public bool HandColliders = true;

		public bool FeetColliders;

		public bool MiscColliders;

		public bool HipBones;

		public bool LegBones;

		public bool ChestBones = true;

		public bool ArmBones;

		public bool HeadBones = true;

		public bool MiscBones;

		public bool DynamicBonesEnabled = true;

		public bool ParticleSystemsEnabled = true;

		public bool ClothEnabled = true;

		public bool ShadersEnabled = true;

		public bool AudioSourcesEnabled = true;
	}
}
