using UnityEngine;

namespace emmVRC.Objects
{
	public class UnityObjectWrapper<T> where T : Object
	{
		public readonly T value;

		public UnityObjectWrapper(T value)
		{
			this.value = value;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			T val = obj as T;
			if (val != null)
			{
				return val.GetInstanceID() == GetHashCode();
			}
			UnityObjectWrapper<T> unityObjectWrapper = obj as UnityObjectWrapper<T>;
			if (unityObjectWrapper != null)
			{
				return unityObjectWrapper.GetHashCode() == GetHashCode();
			}
			return false;
		}

		public override int GetHashCode()
		{
			return value.GetInstanceID();
		}

		public static implicit operator UnityObjectWrapper<T>(T value)
		{
			return new UnityObjectWrapper<T>(value);
		}

		public static implicit operator T(UnityObjectWrapper<T> value)
		{
			return value.value;
		}
	}
}
