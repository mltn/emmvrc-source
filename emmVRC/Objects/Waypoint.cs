using UnityEngine;

namespace emmVRC.Objects
{
	public class Waypoint
	{
		public string Name = "";

		public float x;

		public float y;

		public float z;

		public float rx;

		public float ry;

		public float rz;

		public float rw;

		public void Goto()
		{
			if (!(VRCPlayer.field_Internal_Static_VRCPlayer_0 == null))
			{
				VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position = new Vector3(x, y, z);
				VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation = new Quaternion(rx, ry, rz, rw);
			}
		}

		public bool IsEmpty()
		{
			if (string.IsNullOrEmpty(Name) && x == 0f && y == 0f && z == 0f && rx == 0f && ry == 0f && rz == 0f)
			{
				return rw == 0f;
			}
			return false;
		}
	}
}
