using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using MelonLoader;

namespace emmVRC
{
	public static class emmVRC
	{
		public static bool Initialized = false;

		public static readonly AwaitProvider AwaitUpdate = new AwaitProvider();

		private static readonly IEnumerable<CompatibilityCheck> compatCheckers = from type in typeof(emmVRC).Assembly.GetTypes()
			where type.IsSubclassOf(typeof(CompatibilityCheck))
			select (CompatibilityCheck)Activator.CreateInstance(type);

		private static readonly IEnumerable<MelonLoaderEvents> eventListeners = from type in typeof(emmVRC).Assembly.GetTypes()
			where type.IsSubclassOf(typeof(MelonLoaderEvents))
			orderby ((PriorityAttribute)Attribute.GetCustomAttribute(type, typeof(PriorityAttribute)))?.priority ?? 0
			select (MelonLoaderEvents)Activator.CreateInstance(type);

		private static readonly IEnumerable<IWithUpdate> eventListenersWithUpdate = from type in Assembly.GetExecutingAssembly().GetTypes()
			where typeof(IWithUpdate).IsAssignableFrom(type) && type != typeof(IWithUpdate)
			orderby ((PriorityAttribute)Attribute.GetCustomAttribute(type, typeof(PriorityAttribute)))?.priority ?? 0
			select (IWithUpdate)Activator.CreateInstance(type);

		private static readonly IEnumerable<IWithFixedUpdate> eventListenersWithFixedUpdate = from type in Assembly.GetExecutingAssembly().GetTypes()
			where typeof(IWithFixedUpdate).IsAssignableFrom(type) && type != typeof(IWithFixedUpdate)
			orderby ((PriorityAttribute)Attribute.GetCustomAttribute(type, typeof(PriorityAttribute)))?.priority ?? 0
			select (IWithFixedUpdate)Activator.CreateInstance(type);

		private static readonly IEnumerable<IWithLateUpdate> eventListenersWithLateUpdate = from type in Assembly.GetExecutingAssembly().GetTypes()
			where typeof(IWithLateUpdate).IsAssignableFrom(type) && type != typeof(IWithLateUpdate)
			orderby ((PriorityAttribute)Attribute.GetCustomAttribute(type, typeof(PriorityAttribute)))?.priority ?? 0
			select (IWithLateUpdate)Activator.CreateInstance(type);

		private static readonly IEnumerable<IWithGUI> eventListenersWithGUI = from type in Assembly.GetExecutingAssembly().GetTypes()
			where typeof(IWithGUI).IsAssignableFrom(type) && type != typeof(IWithGUI)
			orderby ((PriorityAttribute)Attribute.GetCustomAttribute(type, typeof(PriorityAttribute)))?.priority ?? 0
			select (IWithGUI)Activator.CreateInstance(type);

		private static void OnApplicationStart()
		{
			Configuration.Initialize();
			foreach (MelonLoaderEvents eventListener in eventListeners)
			{
				try
				{
					eventListener.OnApplicationStart();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnApplicationStart of \"" + eventListener.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
			MelonCoroutines.Start(WaitForUiManagerInit());
		}

		private static IEnumerator WaitForUiManagerInit()
		{
			while (VRCUiManager.field_Private_Static_VRCUiManager_0 == null)
			{
				yield return null;
			}
			OnUIManagerInit();
		}

		public static void OnUIManagerInit()
		{
			Logger.LogDebug("UI manager initialized");
			if (compatCheckers.Any((CompatibilityCheck a) => !a.RunCheck()))
			{
				Logger.LogError("One or more compatibility issues were detected. See above for details. emmVRC cannot start.");
				MessageBox.Show("One or more compatibility issues were detected. See the console for more details. emmVRC cannot start.", "emmVRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			foreach (MelonLoaderEvents eventListener in eventListeners)
			{
				try
				{
					eventListener.OnUiManagerInit();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running UiManagerInit of \"" + eventListener.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
			stopwatch.Stop();
			Logger.Log("Initialization is successful in " + stopwatch.Elapsed.ToString("ss\\.f", null) + "s. Welcome to emmVRC!");
			Logger.Log("You are running version " + Attributes.Version.ToString(3));
			Initialized = true;
		}

		public static void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			foreach (MelonLoaderEvents eventListener in eventListeners)
			{
				try
				{
					eventListener.OnSceneWasLoaded(buildIndex, sceneName);
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnSceneLoad of \"" + eventListener.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnSceneWasInitialized(int buildIndex, string sceneName)
		{
			_ = DateTime.Now;
			foreach (MelonLoaderEvents eventListener in eventListeners)
			{
				try
				{
					eventListener.OnSceneWasInitialized(buildIndex, sceneName);
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnSceneInit of \"" + eventListener.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnSceneWasUnloaded(int buildIndex, string sceneName)
		{
			foreach (MelonLoaderEvents eventListener in eventListeners)
			{
				try
				{
					eventListener.OnSceneWasUnloaded(buildIndex, sceneName);
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnSceneUnload of \"" + eventListener.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnUpdate()
		{
			AwaitUpdate.Flush();
			foreach (IWithUpdate item in eventListenersWithUpdate)
			{
				try
				{
					item.OnUpdate();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnUpdate of \"" + item.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
			if (Initialized && Resources.onlineSprite != null && !Configuration.JSONConfig.WelcomeMessageShown)
			{
				emmVRCNotificationsManager.AddNotification(new Notification("Welcome to emmVRC!", null, "Welcome to emmVRC! For updates regarding the client, teasers for new features, and bug reports and support, join the Discord!", canIgnore: true, showAcceptButton: true, delegate
				{
					ButtonAPI.GetQuickMenuInstance().AskConfirmOpenURL("https://discord.gg/emmVRC");
				}, "Open Discord", "Open the invite to our Discord", showIgnoreButton: true, null, "Dismiss", "Dismisses the notification"));
				Configuration.WriteConfigOption("WelcomeMessageShown", true);
			}
		}

		public static void OnFixedUpdate()
		{
			foreach (IWithFixedUpdate item in eventListenersWithFixedUpdate)
			{
				try
				{
					item.OnFixedUpdate();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnFixedUpdate of \"" + item.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnLateUpdate()
		{
			foreach (IWithLateUpdate item in eventListenersWithLateUpdate)
			{
				try
				{
					item.LateUpdate();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnLateUpdate of \"" + item.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnGUI()
		{
			foreach (IWithGUI item in eventListenersWithGUI)
			{
				try
				{
					item.OnGUI();
				}
				catch (Exception ex)
				{
					Logger.LogError("emmVRC encountered an exception while running OnGUI of \"" + item.GetType().FullName + "\":\n" + ex.ToString());
				}
			}
		}

		public static void OnApplicationQuit()
		{
			NetworkClient.Logout();
		}
	}
}
