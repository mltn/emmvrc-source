using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VRC.UI.Elements.Controls;
using VRC.UI.Elements.Tooltips;

namespace emmVRC.Utils
{
	public class ToggleButton
	{
		private readonly TextMeshProUGUI buttonText;

		private readonly Image buttonImage;

		private readonly Toggle buttonToggle;

		private readonly UiToggleTooltip toggleTooltip;

		public readonly GameObject gameObject;

		private bool _stateInvoke = true;

		public ToggleButton(Transform parent, string text, Action<bool> stateChanged, string offTooltip, string onTooltip, Sprite icon = null)
		{
			ToggleButton toggleButton = this;
			gameObject = UnityEngine.Object.Instantiate(ButtonAPI.toggleButtonBase, parent);
			buttonText = gameObject.GetComponentInChildren<TextMeshProUGUI>(includeInactive: true);
			buttonText.text = text;
			buttonToggle = gameObject.GetComponentInChildren<Toggle>(includeInactive: true);
			buttonToggle.onValueChanged = new Toggle.ToggleEvent();
			buttonToggle.isOn = false;
			buttonToggle.onValueChanged.AddListener((Action<bool>)delegate(bool val)
			{
				if (toggleButton._stateInvoke)
				{
					stateChanged(val);
				}
			});
			toggleTooltip = gameObject.GetComponentInChildren<UiToggleTooltip>(includeInactive: true);
			toggleTooltip.field_Public_String_0 = onTooltip;
			toggleTooltip.field_Public_String_1 = offTooltip;
			toggleTooltip.prop_Boolean_0 = true;
			buttonImage = gameObject.transform.Find("Icon_On").GetComponentInChildren<Image>(includeInactive: true);
			if (icon != null)
			{
				buttonImage.sprite = icon;
				buttonImage.overrideSprite = icon;
			}
			else
			{
				buttonImage.sprite = ButtonAPI.onIconSprite;
				buttonImage.overrideSprite = ButtonAPI.onIconSprite;
			}
		}

		public ToggleButton(MenuPage pge, string text, Action<bool> stateChanged, string offTooltip, string onTooltip, Sprite icon = null)
			: this(pge.menuContents, text, stateChanged, offTooltip, onTooltip, icon)
		{
		}

		public ToggleButton(ButtonGroup grp, string text, Action<bool> stateChanged, string offTooltip, string onTooltip, Sprite icon = null)
			: this(grp.gameObject.transform, text, stateChanged, offTooltip, onTooltip, icon)
		{
		}

		public void SetAction(Action<bool> newAction)
		{
			buttonToggle.onValueChanged = new Toggle.ToggleEvent();
			buttonToggle.onValueChanged.AddListener((Action<bool>)delegate(bool val)
			{
				newAction(val);
			});
		}

		public void SetText(string newText)
		{
			buttonText.text = newText;
		}

		public void SetTooltip(string newOffTooltip, string newOnTooltip)
		{
			toggleTooltip.field_Public_String_0 = newOnTooltip;
			toggleTooltip.field_Public_String_1 = newOffTooltip;
		}

		public void SetIcon(Sprite newIcon)
		{
			if (newIcon == null)
			{
				buttonImage.gameObject.SetActive(value: false);
				return;
			}
			buttonImage.sprite = newIcon;
			buttonImage.overrideSprite = newIcon;
			buttonImage.gameObject.SetActive(value: true);
		}

		public void SetToggleState(bool newState, bool invoke = false)
		{
			_stateInvoke = false;
			buttonToggle.isOn = newState;
			buttonToggle.onValueChanged.Invoke(newState);
			_stateInvoke = true;
			if (invoke)
			{
				buttonToggle.onValueChanged.Invoke(newState);
			}
		}

		private void SetToggleIcon(bool newState)
		{
			buttonToggle.GetComponent<ToggleIcon>().Method_Private_Void_Boolean_PDM_0(newState);
			buttonToggle.GetComponent<ToggleIcon>().Method_Private_Void_Boolean_PDM_1(newState);
			buttonToggle.GetComponent<ToggleIcon>().Method_Private_Void_Boolean_PDM_2(newState);
			buttonToggle.GetComponent<ToggleIcon>().Method_Private_Void_Boolean_PDM_3(newState);
		}

		public void SetInteractable(bool val)
		{
			buttonToggle.interactable = val;
			buttonToggle.m_Interactable = val;
		}

		public void SetActive(bool state)
		{
			gameObject.SetActive(state);
		}
	}
}
