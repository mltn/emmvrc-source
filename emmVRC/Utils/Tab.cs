using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VRC.UI.Core.Styles;
using VRC.UI.Elements.Controls;
using VRC.UI.Elements.Tooltips;

namespace emmVRC.Utils
{
	public class Tab
	{
		private readonly GameObject gameObject;

		private readonly MenuTab menuTab;

		private readonly Image tabIcon;

		private readonly VRC.UI.Elements.Tooltips.UiTooltip tabTooltip;

		private readonly GameObject badgeGameObject;

		private readonly TextMeshProUGUI badgeText;

		public Tab(Transform parent, string menuName, string tooltip, Sprite icon = null, Action tabAction = null)
		{
			Tab tab = this;
			gameObject = UnityEngine.Object.Instantiate(ButtonAPI.menuTabBase, parent);
			gameObject.name = menuName + "Tab";
			menuTab = gameObject.GetComponent<MenuTab>();
			menuTab.field_Private_MenuStateController_0 = ButtonAPI.GetMenuStateControllerInstance();
			menuTab.field_Public_String_0 = menuName;
			tabIcon = gameObject.transform.Find("Icon").GetComponent<Image>();
			tabIcon.sprite = icon;
			tabIcon.overrideSprite = icon;
			tabTooltip = gameObject.GetComponent<VRC.UI.Elements.Tooltips.UiTooltip>();
			tabTooltip.field_Public_String_0 = tooltip;
			badgeGameObject = gameObject.transform.GetChild(0).gameObject;
			badgeText = badgeGameObject.GetComponentInChildren<TextMeshProUGUI>();
			menuTab.GetComponent<StyleElement>().field_Private_Selectable_0 = menuTab.GetComponent<Button>();
			menuTab.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				tabAction?.DelegateSafeInvoke();
				tab.menuTab.GetComponent<StyleElement>().field_Private_Selectable_0 = tab.menuTab.GetComponent<Button>();
			});
		}

		public void SetBadge(bool showing = true, string text = "")
		{
			if (!(badgeGameObject == null) && !(badgeText == null))
			{
				badgeGameObject.SetActive(showing);
				badgeText.text = text;
			}
		}

		public void SetTooltip(string newText)
		{
			tabTooltip.field_Public_String_0 = newText;
		}
	}
}
