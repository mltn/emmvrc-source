using System;
using System.Linq;
using System.Reflection;
using UnhollowerBaseLib.Attributes;
using UnhollowerRuntimeLib.XrefScans;

namespace emmVRC.Utils
{
	public static class XrefUtils
	{
		public static bool CheckMethod(MethodInfo method, string match)
		{
			try
			{
				return XrefScanner.XrefScan(method).Any((XrefInstance instance) => instance.Type == XrefType.Global && instance.ReadAsObject().ToString().Contains(match));
			}
			catch
			{
			}
			return false;
		}

		public static bool CheckUsedBy(MethodInfo method, string methodName, Type type = null)
		{
			foreach (XrefInstance item in XrefScanner.UsedBy(method))
			{
				if (item.Type != XrefType.Method)
				{
					continue;
				}
				try
				{
					if ((type == null || item.TryResolve().DeclaringType == type) && item.TryResolve().Name.Contains(methodName))
					{
						return true;
					}
				}
				catch
				{
				}
			}
			return false;
		}

		public static bool CheckUsing(MethodInfo method, string methodName, Type type = null)
		{
			foreach (XrefInstance item in XrefScanner.XrefScan(method))
			{
				if (item.Type != XrefType.Method)
				{
					continue;
				}
				try
				{
					if ((type == null || item.TryResolve().DeclaringType == type) && item.TryResolve().Name.Contains(methodName))
					{
						return true;
					}
				}
				catch
				{
				}
			}
			return false;
		}

		public static Type GetTypeFromObfuscatedName(string obfuscatedName)
		{
			return AppDomain.CurrentDomain.GetAssemblies().SelectMany((Assembly assembly) => assembly.GetTypes()).First((Type type) => type.GetCustomAttribute<ObfuscatedNameAttribute>() != null && type.GetCustomAttribute<ObfuscatedNameAttribute>().ObfuscatedName.Equals(obfuscatedName, StringComparison.InvariantCulture));
		}
	}
}
