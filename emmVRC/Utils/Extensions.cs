using System;
using System.Linq;
using System.Reflection;
using emmVRCLoader;
using Il2CppSystem;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;
using VRC.DataModel;
using VRC.DataModel.Core;
using VRC.UI.Elements;
using VRC.UI.Elements.Controls;

namespace emmVRC.Utils
{
	public static class Extensions
	{
		private static MethodInfo ourShowAlertMethod;

		private static MethodInfo ourShowOKDialogMethod;

		private static MethodInfo ourShowConfirmDialogMethod;

		private static MethodInfo ourAskConfirmOpenURLMethod;

		private static MethodInfo _apiUserToIUser;

		public static MethodInfo ShowOKDialogMethod
		{
			get
			{
				if (ourShowOKDialogMethod != null)
				{
					return ourShowOKDialogMethod;
				}
				ourShowOKDialogMethod = typeof(VRC.UI.Elements.QuickMenu).GetMethods().First((MethodInfo it) => it != null && it.GetParameters().Length == 3 && it.Name.Contains("_PDM") && it.GetParameters().First().ParameterType == typeof(string) && it.GetParameters().Last().ParameterType == typeof(Action) && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
				{
					if (jt.Type == XrefType.Global)
					{
						Object obj = jt.ReadAsObject();
						return ((obj != null) ? obj.ToString() : null) == "ConfirmDialog";
					}
					return false;
				}));
				return ourShowOKDialogMethod;
			}
		}

		public static MethodInfo ShowConfirmDialogMethod
		{
			get
			{
				if (ourShowConfirmDialogMethod != null)
				{
					return ourShowConfirmDialogMethod;
				}
				ourShowConfirmDialogMethod = typeof(VRC.UI.Elements.QuickMenu).GetMethods().First((MethodInfo it) => it != null && it.GetParameters().Length == 4 && it.Name.Contains("_PDM") && it.GetParameters()[0].ParameterType == typeof(string) && it.GetParameters()[1].ParameterType == typeof(string) && it.GetParameters()[2].ParameterType == typeof(Action) && it.GetParameters()[3].ParameterType == typeof(Action) && XrefScanner.UsedBy(it).ToList().Count > 30);
				return ourShowConfirmDialogMethod;
			}
		}

		public static MethodInfo AskConfirmOpenURLMethod
		{
			get
			{
				if (ourAskConfirmOpenURLMethod != null)
				{
					return ourAskConfirmOpenURLMethod;
				}
				ourAskConfirmOpenURLMethod = typeof(VRC.UI.Elements.QuickMenu).GetMethods().First((MethodInfo it) => it != null && it.GetParameters().Length == 1 && it.Name.Contains("_Virtual_Final_New") && it.GetParameters().First().ParameterType == typeof(string) && XrefScanner.XrefScan(it).Any((XrefInstance jt) => jt.Type == XrefType.Global && jt.ReadAsObject() != null && jt.ReadAsObject().ToString().Contains("This link will open in your web browser.")));
				return ourAskConfirmOpenURLMethod;
			}
		}

		public static MethodInfo ToIUserMethod
		{
			get
			{
				if (_apiUserToIUser == null)
				{
					Type type2 = typeof(VRCPlayer).Assembly.GetTypes().First((Type type) => type.GetMethods().FirstOrDefault((MethodInfo method) => method.Name.StartsWith("Method_Private_Void_Action_1_ApiWorldInstance_Action_1_String_")) != null && type.GetMethods().FirstOrDefault((MethodInfo method) => method.Name.StartsWith("Method_Public_Virtual_Final_New_Observable_1_List_1_String_")) == null);
					_apiUserToIUser = typeof(DataModelCache).GetMethod("Method_Public_TYPE_String_TYPE2_Boolean_0");
					_apiUserToIUser = _apiUserToIUser.MakeGenericMethod(type2, typeof(APIUser));
				}
				return _apiUserToIUser;
			}
		}

		public static GameObject FindObject(this GameObject parent, string name)
		{
			Transform[] array = parent.GetComponentsInChildren<Transform>(includeInactive: true);
			foreach (Transform transform in array)
			{
				if (transform.name == name)
				{
					return transform.gameObject;
				}
			}
			return null;
		}

		public static string GetPath(this GameObject gameObject)
		{
			string text = "/" + gameObject.name;
			while (gameObject.transform.parent != null)
			{
				gameObject = gameObject.transform.parent.gameObject;
				text = "/" + gameObject.name + text;
			}
			return text;
		}

		public static void DestroyChildren(this Transform transform, Func<Transform, bool> exclude)
		{
			for (int num = transform.childCount - 1; num >= 0; num--)
			{
				if (exclude == null || exclude(transform.GetChild(num)))
				{
					UnityEngine.Object.DestroyImmediate(transform.GetChild(num).gameObject);
				}
			}
		}

		public static void DestroyChildren(this Transform transform)
		{
			transform.DestroyChildren(null);
		}

		public static Vector3 SetX(this Vector3 vector, float x)
		{
			return new Vector3(x, vector.y, vector.z);
		}

		public static Vector3 SetY(this Vector3 vector, float y)
		{
			return new Vector3(vector.x, y, vector.z);
		}

		public static Vector3 SetZ(this Vector3 vector, float z)
		{
			return new Vector3(vector.x, vector.y, z);
		}

		public static float RoundAmount(this float i, float nearestFactor)
		{
			return (float)Math.Round(i / nearestFactor) * nearestFactor;
		}

		public static Vector3 RoundAmount(this Vector3 i, float nearestFactor)
		{
			return new Vector3(i.x.RoundAmount(nearestFactor), i.y.RoundAmount(nearestFactor), i.z.RoundAmount(nearestFactor));
		}

		public static Vector2 RoundAmount(this Vector2 i, float nearestFactor)
		{
			return new Vector2(i.x.RoundAmount(nearestFactor), i.y.RoundAmount(nearestFactor));
		}

		public static Sprite ToSprite(this Texture2D tex)
		{
			Rect rect = new Rect(0f, 0f, tex.width, tex.height);
			Vector2 pivot = new Vector2(0.5f, 0.5f);
			Vector4 border = Vector4.zero;
			Sprite sprite = Sprite.CreateSprite_Injected(tex, ref rect, ref pivot, 50f, 0u, SpriteMeshType.FullRect, ref border, generateFallbackPhysicsShape: false);
			sprite.hideFlags |= HideFlags.DontUnloadUnusedAsset;
			return sprite;
		}

		public static string ReplaceFirst(this string text, string search, string replace)
		{
			int num = text.IndexOf(search);
			if (num < 0)
			{
				return text;
			}
			return text.Substring(0, num) + replace + text.Substring(num + search.Length);
		}

		public static ColorBlock SetColor(this ColorBlock block, Color color)
		{
			ColorBlock result = default(ColorBlock);
			result.colorMultiplier = block.colorMultiplier;
			result.disabledColor = Color.grey;
			result.highlightedColor = color;
			result.normalColor = color / 1.5f;
			result.pressedColor = Color.white;
			result.selectedColor = color / 1.5f;
			return result;
		}

		public static void DelegateSafeInvoke(this Delegate @delegate, params object[] args)
		{
			Delegate[] invocationList = @delegate.GetInvocationList();
			for (int i = 0; i < invocationList.Length; i++)
			{
				try
				{
					invocationList[i].DynamicInvoke(args);
				}
				catch (Exception ex)
				{
					emmVRCLoader.Logger.LogError("Error while executing delegate:\n" + ex.ToString());
				}
			}
		}

		public static string ToEasyString(this TimeSpan timeSpan)
		{
			if (Mathf.FloorToInt(timeSpan.Ticks / 864000000000L) > 0)
			{
				return $"{timeSpan:%d} days";
			}
			if (Mathf.FloorToInt(timeSpan.Ticks / 36000000000L) > 0)
			{
				return $"{timeSpan:%h} hours";
			}
			if (Mathf.FloorToInt(timeSpan.Ticks / 600000000) > 0)
			{
				return $"{timeSpan:%m} minutes";
			}
			return $"{timeSpan:%s} seconds";
		}

		public static void ShowAlert(this VRC.UI.Elements.QuickMenu qm, string message)
		{
			if (ourShowAlertMethod == null)
			{
				MethodInfo[] methods = typeof(ModalAlert).GetMethods();
				foreach (MethodInfo methodInfo in methods)
				{
					emmVRCLoader.Logger.LogDebug(methodInfo.Name);
					if (methodInfo.Name.Contains("Method_Private_Void_") && !methodInfo.Name.Contains("PDM") && methodInfo.GetParameters().Length == 0)
					{
						qm.field_Private_ModalAlert_0.field_Private_String_0 = message;
						methodInfo.Invoke(qm.field_Private_ModalAlert_0, null);
						if (qm.transform.Find("Container/Window/QMParent/Modal_Alert/Background_Alert").gameObject.activeSelf)
						{
							ourShowAlertMethod = methodInfo;
							break;
						}
					}
				}
			}
			else
			{
				qm.field_Private_ModalAlert_0.field_Private_String_0 = message;
				ourShowAlertMethod.Invoke(qm.field_Private_ModalAlert_0, null);
			}
		}

		public static void ShowOKDialog(this VRC.UI.Elements.QuickMenu qm, string title, string message, Action okButton = null)
		{
			ShowOKDialogMethod.Invoke(qm, new object[3]
			{
				title,
				message,
				DelegateSupport.ConvertDelegate<Action>((Delegate)okButton)
			});
		}

		public static void ShowConfirmDialog(this VRC.UI.Elements.QuickMenu qm, string title, string message, Action yesButton = null, Action noButton = null)
		{
			ShowConfirmDialogMethod.Invoke(qm, new object[4]
			{
				title,
				message,
				DelegateSupport.ConvertDelegate<Action>((Delegate)yesButton),
				DelegateSupport.ConvertDelegate<Action>((Delegate)noButton)
			});
		}

		public static void ShowCustomDialog(this VRC.UI.Elements.QuickMenu qm, string title, string message, string button1Text, string button2Text, string button3Text, Action button1Action = null, Action button2Action = null, Action button3Action = null)
		{
			qm.Method_Public_Void_String_String_String_String_String_Action_Action_Action_PDM_0(title, message, button1Text, button2Text, button3Text, Action.op_Implicit(button1Action), Action.op_Implicit(button2Action), Action.op_Implicit(button3Action));
		}

		public static void AskConfirmOpenURL(this VRC.UI.Elements.QuickMenu qm, string url)
		{
			AskConfirmOpenURLMethod.Invoke(qm, new object[1] { url });
		}

		public static IUser ToIUser(this APIUser value)
		{
			//IL_0030: Unknown result type (might be due to invalid IL or missing references)
			//IL_003a: Expected O, but got Unknown
			return ((Il2CppObjectBase)(Object)_apiUserToIUser.Invoke(DataModelManager.get_field_Private_Static_DataModelManager_0().get_field_Private_DataModelCache_0(), new object[3] { value.id, value, false })).Cast<IUser>();
		}
	}
}
