using System.Linq;
using System.Reflection;
using emmVRC.Objects.ModuleBases;
using VRC.Core;
using VRC.UI;

namespace emmVRC.Utils
{
	public class WorldUtils : MelonLoaderEvents
	{
		private static MethodInfo joinWorldFromWorldPage;

		public override void OnApplicationStart()
		{
			joinWorldFromWorldPage = (from mb in typeof(PageWorldInfo).GetMethods()
				where mb.Name.StartsWith("Method_Private_Void_")
				select mb).First((MethodInfo mb) => XrefUtils.CheckMethod(mb, "WorldInfo_Go"));
		}

		public static void JoinWorld(ApiWorld world, ApiWorldInstance instance)
		{
			Singletons.pageWorldInfo.field_Private_ApiWorld_0 = world;
			Singletons.pageWorldInfo.field_Public_ApiWorldInstance_0 = instance;
			joinWorldFromWorldPage.Invoke(Singletons.pageWorldInfo, null);
		}
	}
}
