using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Utils
{
	public class SimpleButtonGroup
	{
		public readonly GameObject gameObject;

		public readonly RectMask2D parentMenuMask;

		public SimpleButtonGroup(Transform parent, string text)
		{
			gameObject = Object.Instantiate(ButtonAPI.buttonGroupBase, parent);
			gameObject.transform.DestroyChildren();
			parentMenuMask = parent.parent.GetComponent<RectMask2D>();
		}

		public SimpleButtonGroup(MenuPage pge, string text)
			: this(pge.menuContents, text)
		{
		}

		public void Destroy()
		{
			Object.Destroy(gameObject);
		}

		public void SetActive(bool state)
		{
			gameObject.SetActive(state);
		}
	}
}
