using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using emmVRCLoader;
using VRC.Core;

namespace emmVRC.Functions.PlayerHacks
{
	public class Waypoints : MelonLoaderEvents
	{
		private static string currentWorld;

		public static List<Waypoint> CurrentWaypoints { get; private set; }

		public override void OnApplicationStart()
		{
			CurrentWaypoints = new List<Waypoint>();
			NetworkEvents.OnInstanceChanged += OnInstanceChanged;
		}

		private static void OnInstanceChanged(ApiWorld world, ApiWorldInstance instance)
		{
			currentWorld = world.id;
			CurrentWaypoints.Clear();
			string text = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/Waypoints");
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			text = Path.Combine(text, currentWorld + ".json");
			if (File.Exists(text))
			{
				string text2 = File.ReadAllText(text);
				if (text2.Contains("emmVRC.Menus"))
				{
					text2 = text2.Replace("emmVRC.Menus", "emmVRC.Objects");
					File.WriteAllText(text, text2);
				}
				CurrentWaypoints = Decoder.Decode(File.ReadAllText(text)).Make<List<Waypoint>>();
			}
			for (int i = 0; i < CurrentWaypoints.Count; i++)
			{
				if (CurrentWaypoints[i] != null && CurrentWaypoints[i].IsEmpty())
				{
					CurrentWaypoints.RemoveAt(i);
				}
			}
		}

		public static void SaveWaypoints()
		{
			emmVRCLoader.Logger.LogDebug("Saving waypoints...");
			if (CurrentWaypoints.All((Waypoint waypoint) => waypoint == null))
			{
				return;
			}
			string text = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/Waypoints");
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			string text2 = Encoder.Encode(CurrentWaypoints, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints);
			text = Path.Combine(text, currentWorld + ".json");
			if (!File.Exists(text))
			{
				using StreamWriter streamWriter = File.CreateText(text);
				streamWriter.Write(text2);
			}
			else
			{
				File.WriteAllText(text, text2);
			}
		}
	}
}
