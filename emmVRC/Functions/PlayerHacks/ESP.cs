using System;
using System.Collections.Generic;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;
using VRC;
using VRC.Core;

namespace emmVRC.Functions.PlayerHacks
{
	public class ESP : MelonLoaderEvents
	{
		private static HighlightsFXStandalone highlightFX;

		private static HighlightsFXStandalone friendHighlightFX;

		public static bool IsEnabled { get; private set; }

		public override void OnApplicationStart()
		{
			NetworkEvents.OnPlayerJoined += delegate(Player player)
			{
				if (APIUser.IsFriendsWith(player.field_Private_APIUser_0.id))
				{
					friendHighlightFX.field_Protected_HashSet_1_Renderer_0.Add(player.transform.Find("SelectRegion").GetComponent<Renderer>());
				}
				else
				{
					highlightFX.field_Protected_HashSet_1_Renderer_0.Add(player.transform.Find("SelectRegion").GetComponent<Renderer>());
				}
			};
			NetworkEvents.OnPlayerLeft += delegate(Player player)
			{
				if (APIUser.IsFriendsWith(player.field_Private_APIUser_0.id))
				{
					friendHighlightFX.field_Protected_HashSet_1_Renderer_0.Remove(player.transform.Find("SelectRegion").GetComponent<Renderer>());
				}
				else
				{
					highlightFX.field_Protected_HashSet_1_Renderer_0.Remove(player.transform.Find("SelectRegion").GetComponent<Renderer>());
				}
			};
		}

		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("RiskyFunctionsEnabled", delegate
			{
				if (!Configuration.JSONConfig.RiskyFunctionsEnabled && IsEnabled)
				{
					SetActive(active: false);
				}
			}));
		}

		public override void OnSceneWasInitialized(int buildIndex, string sceneName)
		{
			if (buildIndex == -1)
			{
				if (highlightFX == null)
				{
					highlightFX = HighlightsFX.field_Private_Static_HighlightsFX_0.gameObject.AddComponent<HighlightsFXStandalone>();
					friendHighlightFX = HighlightsFX.field_Private_Static_HighlightsFX_0.gameObject.AddComponent<HighlightsFXStandalone>();
				}
				highlightFX.enabled = false;
				highlightFX.field_Protected_HashSet_1_Renderer_0?.Clear();
				friendHighlightFX.highlightColor = ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex);
				friendHighlightFX.enabled = false;
				friendHighlightFX.field_Protected_HashSet_1_Renderer_0?.Clear();
			}
		}

		public static void SetActive(bool active)
		{
			if (highlightFX != null)
			{
				highlightFX.enabled = active;
				friendHighlightFX.enabled = active;
			}
			IsEnabled = active;
		}
	}
}
