using System;
using System.Collections.Generic;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using VRC;
using VRC.Core;

namespace emmVRC.Functions.PlayerHacks
{
	public class PlayerHistory : MelonLoaderEvents
	{
		public static List<InstancePlayer> currentPlayers;

		public override void OnUiManagerInit()
		{
			currentPlayers = new List<InstancePlayer>();
			NetworkEvents.OnPlayerJoined += delegate(Player plr)
			{
				if (Configuration.JSONConfig.PlayerHistoryEnable && plr.prop_APIUser_0 != null && plr.prop_APIUser_0.id != APIUser.CurrentUser.id)
				{
					currentPlayers.Add(new InstancePlayer
					{
						Name = plr.prop_APIUser_0.GetName(),
						UserID = plr.prop_APIUser_0.id,
						TimeJoinedStamp = DateTime.Now.ToShortTimeString()
					});
				}
			};
			NetworkEvents.OnLocalPlayerJoined += delegate
			{
				currentPlayers.Clear();
			};
		}
	}
}
