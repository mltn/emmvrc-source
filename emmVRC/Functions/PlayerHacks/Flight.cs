using System;
using System.Collections;
using System.Collections.Generic;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using MelonLoader;
using UnityEngine;
using UnityEngine.XR;
using VRC;
using VRC.Animation;

namespace emmVRC.Functions.PlayerHacks
{
	public class Flight : MelonLoaderEvents
	{
		private static Vector3 gravity;

		private static object coroutine;

		public static bool IsFlyEnabled { get; private set; }

		public static bool IsNoClipEnabled { get; private set; }

		public static VRCInput VerticalInput { get; private set; }

		public static VRCInput HorizontalInput { get; private set; }

		public static VRCInput VerticalLookInput { get; private set; }

		public static VRCInput RunInput { get; private set; }

		public static CharacterController Controller { get; private set; }

		public static VRCMotionState State { get; private set; }

		public override void OnApplicationStart()
		{
			NetworkEvents.OnLocalPlayerJoined += delegate(Player player)
			{
				Controller = player.GetComponent<CharacterController>();
				State = player.GetComponent<VRCMotionState>();
			};
			NetworkEvents.OnLocalPlayerLeft += delegate
			{
				SetFlyActive(active: false);
				SetNoClipActive(active: false);
			};
		}

		public override void OnUiManagerInit()
		{
			VerticalInput = VRCInputManager.field_Private_Static_Dictionary_2_String_VRCInput_0.get_Item("Vertical");
			HorizontalInput = VRCInputManager.field_Private_Static_Dictionary_2_String_VRCInput_0.get_Item("Horizontal");
			VerticalLookInput = VRCInputManager.field_Private_Static_Dictionary_2_String_VRCInput_0.get_Item("LookVertical");
			RunInput = VRCInputManager.field_Private_Static_Dictionary_2_String_VRCInput_0.get_Item("Run");
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("RiskyFunctionsEnabled", delegate
			{
				if (!Configuration.JSONConfig.RiskyFunctionsEnabled && (IsFlyEnabled || IsNoClipEnabled))
				{
					if (IsNoClipEnabled)
					{
						SetNoClipActive(active: false);
					}
					SetFlyActive(active: false);
				}
			}));
		}

		public static void SetFlyActive(bool active)
		{
			if (active)
			{
				if (!IsFlyEnabled && RiskyFunctionsManager.AreRiskyFunctionsAllowed)
				{
					gravity = Physics.gravity;
					Physics.gravity = Vector3.zero;
					if (XRDevice.isPresent && Configuration.JSONConfig.VRFlightControls)
					{
						coroutine = MelonCoroutines.Start(FlyCoroutineVR());
					}
					else
					{
						coroutine = MelonCoroutines.Start(FlyCoroutineDesktop());
					}
					IsFlyEnabled = true;
				}
			}
			else
			{
				if (coroutine != null)
				{
					MelonCoroutines.Stop(coroutine);
				}
				coroutine = null;
				IsFlyEnabled = false;
				IsNoClipEnabled = false;
				Physics.gravity = gravity;
			}
		}

		public static void SetNoClipActive(bool active)
		{
			if (Controller != null)
			{
				Controller.enabled = !active;
			}
			IsNoClipEnabled = active;
		}

		private static IEnumerator FlyCoroutineVR()
		{
			while (true)
			{
				VRCPlayer field_Internal_Static_VRCPlayer_ = VRCPlayer.field_Internal_Static_VRCPlayer_0;
				Vector3 vector = (Camera.main.transform.forward * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetRunSpeed() * VerticalInput.field_Public_Single_0 + Vector3.up * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetStrafeSpeed() * VerticalLookInput.field_Public_Single_0 + Camera.main.transform.right * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetStrafeSpeed() * HorizontalInput.field_Public_Single_0) * Time.deltaTime;
				field_Internal_Static_VRCPlayer_.transform.position += vector;
				State.Reset();
				yield return null;
			}
		}

		private static IEnumerator FlyCoroutineDesktop()
		{
			while (true)
			{
				VRCPlayer field_Internal_Static_VRCPlayer_ = VRCPlayer.field_Internal_Static_VRCPlayer_0;
				float num = 0f;
				num += (float)(Input.GetKey(KeyCode.Q) ? (-1) : 0);
				num += (float)(Input.GetKey(KeyCode.E) ? 1 : 0);
				Vector3 vector = ((!RunInput.field_Private_Boolean_0) ? ((Camera.main.transform.right * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetStrafeSpeed() * Input.GetAxis("Horizontal") + Vector3.up * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetWalkSpeed() * num + Camera.main.transform.forward * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetWalkSpeed() * Input.GetAxis("Vertical")) * Time.deltaTime) : ((Camera.main.transform.right * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetStrafeSpeed() * Input.GetAxis("Horizontal") + Vector3.up * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetRunSpeed() * num + Camera.main.transform.forward * field_Internal_Static_VRCPlayer_.field_Private_VRCPlayerApi_0.GetRunSpeed() * Input.GetAxis("Vertical")) * Time.deltaTime));
				field_Internal_Static_VRCPlayer_.transform.position += vector;
				yield return null;
			}
		}
	}
}
