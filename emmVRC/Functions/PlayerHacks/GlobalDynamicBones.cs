using System;
using System.Collections.Generic;
using System.Linq;
using emmVRC.Functions.Core;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Functions.PlayerHacks
{
	public class GlobalDynamicBones : MelonLoaderEvents
	{
		private static List<DynamicBone> currentWorldDynamicBones = new List<DynamicBone>();

		private static List<DynamicBoneCollider> currentWorldDynamicBoneColliders = new List<DynamicBoneCollider>();

		public override void OnUiManagerInit()
		{
			if (!global::emmVRC.Functions.Core.ModCompatibility.MultiplayerDynamicBones)
			{
				NetworkEvents.OnAvatarInstantiated += delegate(VRCPlayer plr, ApiAvatar avtr, GameObject gobj)
				{
					ProcessDynamicBones(gobj, avtr, plr);
				};
				Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("GlobalDynamicBonesEnabled", PlayerUtils.ReloadAllAvatars));
				Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FriendGlobalDynamicBonesEnabled", PlayerUtils.ReloadAllAvatars));
				Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("EveryoneGlobalDynamicBonesEnabled", PlayerUtils.ReloadAllAvatars));
			}
		}

		public static void ProcessDynamicBones(GameObject avatarObject, ApiAvatar avtr, VRCPlayer player)
		{
			if (Configuration.JSONConfig.GlobalDynamicBonesEnabled)
			{
				if (avatarObject == null)
				{
					emmVRCLoader.Logger.LogDebug("Avatar object is null");
				}
				if (avtr == null)
				{
					emmVRCLoader.Logger.LogDebug("APIAvatar is invalid");
				}
				if (player == null)
				{
					emmVRCLoader.Logger.LogDebug("Player is invalid");
				}
				if (avatarObject == null || avtr == null || player == null)
				{
					return;
				}
				if (!AvatarPermissionsManager.TryGetAvatarPermissions(avtr.id, out var permissions))
				{
					permissions = new AvatarPermissions();
				}
				if (UserPermissions.GetUserPermissions(player._player.prop_APIUser_0.id).GlobalDynamicBonesEnabled || player == VRCPlayer.field_Internal_Static_VRCPlayer_0 || Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled || (Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled && APIUser.IsFriendsWith(player._player.prop_APIUser_0.id)))
				{
					emmVRCLoader.Logger.LogDebug("Global Dynamic Bones is allowed for this user, processing...");
					if (!permissions.HandColliders && !permissions.FeetColliders)
					{
						emmVRCLoader.Logger.LogDebug("Hand and Feet colliders are disabled, so we're fetching all colliders.");
						foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentsInChildren<DynamicBoneCollider>())
						{
							currentWorldDynamicBoneColliders.Add(componentsInChild);
						}
						emmVRCLoader.Logger.LogDebug("There are " + currentWorldDynamicBoneColliders.Count + " processed colliders in this instance.");
					}
					else
					{
						emmVRCLoader.Logger.LogDebug("Hand colliders are " + (permissions.HandColliders ? "enabled" : "disabled") + ", foot colliders are " + (permissions.FeetColliders ? "enabled" : "disabled"));
						if (permissions.HandColliders && avatarObject.GetComponentInChildren<Animator>() != null && avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftHand) != null && avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand) != null)
						{
							emmVRCLoader.Logger.LogDebug("This avatar has valid hands, fetching colliders...");
							foreach (DynamicBoneCollider componentsInChild2 in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).GetComponentsInChildren<DynamicBoneCollider>())
							{
								if (componentsInChild2.m_Bound != DynamicBoneCollider.DynamicBoneColliderBound.Inside)
								{
									currentWorldDynamicBoneColliders.Add(componentsInChild2);
								}
							}
							foreach (DynamicBoneCollider componentsInChild3 in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).GetComponentsInChildren<DynamicBoneCollider>())
							{
								if (componentsInChild3.m_Bound != DynamicBoneCollider.DynamicBoneColliderBound.Inside)
								{
									currentWorldDynamicBoneColliders.Add(componentsInChild3);
								}
							}
							emmVRCLoader.Logger.LogDebug("There are " + currentWorldDynamicBoneColliders.Count + " processed colliders in this instance.");
						}
						if (permissions.FeetColliders && avatarObject.GetComponentInChildren<Animator>() != null && avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot) != null && avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightFoot) != null)
						{
							emmVRCLoader.Logger.LogDebug("This avatar has valid feet, fetching colliders...");
							foreach (DynamicBoneCollider componentsInChild4 in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).GetComponentsInChildren<DynamicBoneCollider>())
							{
								if (componentsInChild4.m_Bound != DynamicBoneCollider.DynamicBoneColliderBound.Inside)
								{
									currentWorldDynamicBoneColliders.Add(componentsInChild4);
								}
							}
							foreach (DynamicBoneCollider componentsInChild5 in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightFoot).GetComponentsInChildren<DynamicBoneCollider>())
							{
								if (componentsInChild5.m_Bound != DynamicBoneCollider.DynamicBoneColliderBound.Inside)
								{
									currentWorldDynamicBoneColliders.Add(componentsInChild5);
								}
							}
							emmVRCLoader.Logger.LogDebug("There are " + currentWorldDynamicBoneColliders.Count + " processed colliders in this instance.");
						}
					}
					emmVRCLoader.Logger.LogDebug("Global Dynamic Bones are allowed for all parts of the avatar. Processing bones...");
					foreach (DynamicBone componentsInChild6 in avatarObject.GetComponentsInChildren<DynamicBone>())
					{
						currentWorldDynamicBones.Add(componentsInChild6);
					}
					emmVRCLoader.Logger.LogDebug("There are " + currentWorldDynamicBones.Count + " processed bones in this instance.");
					emmVRCLoader.Logger.LogDebug("Now processing available dynamic bones, and adding colliders...");
					foreach (DynamicBone item in currentWorldDynamicBones.ToList())
					{
						if (item == null)
						{
							currentWorldDynamicBones.Remove(item);
							continue;
						}
						foreach (DynamicBoneCollider item2 in currentWorldDynamicBoneColliders.ToList())
						{
							if (item2 == null)
							{
								currentWorldDynamicBoneColliders.Remove(item2);
							}
							else if (item.m_Colliders.IndexOf(item2) == -1)
							{
								item.m_Colliders.Add(item2);
							}
						}
					}
				}
			}
			emmVRCLoader.Logger.LogDebug("Done processing dynamic bones for avatar.");
		}
	}
}
