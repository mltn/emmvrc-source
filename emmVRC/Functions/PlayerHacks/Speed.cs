using System;
using System.Collections.Generic;
using emmVRC.Managers;
using emmVRC.Objects.ModuleBases;
using VRC.SDKBase;

namespace emmVRC.Functions.PlayerHacks
{
	public class Speed : MelonLoaderEvents
	{
		public static bool IsEnabled { get; private set; }

		public static float OriginalRunSpeed { get; private set; }

		public static float OriginalStrafeSpeed { get; private set; }

		public static float OriginalWalkSpeed { get; private set; }

		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("RiskyFunctionsEnabled", delegate
			{
				if (!Configuration.JSONConfig.RiskyFunctionsEnabled && IsEnabled)
				{
					SetActive(active: false);
				}
			}));
		}

		public static void SetActive(bool active)
		{
			if (active)
			{
				VRCPlayerApi vRCPlayerApi = VRCPlayer.field_Internal_Static_VRCPlayer_0?.field_Private_VRCPlayerApi_0;
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && vRCPlayerApi != null)
				{
					if (!IsEnabled)
					{
						OriginalRunSpeed = vRCPlayerApi.GetRunSpeed();
						OriginalStrafeSpeed = vRCPlayerApi.GetStrafeSpeed();
						OriginalWalkSpeed = vRCPlayerApi.GetWalkSpeed();
					}
					vRCPlayerApi.SetRunSpeed(OriginalRunSpeed * Configuration.JSONConfig.SpeedModifier);
					vRCPlayerApi.SetStrafeSpeed(OriginalStrafeSpeed * Configuration.JSONConfig.SpeedModifier);
					vRCPlayerApi.SetWalkSpeed(OriginalWalkSpeed * Configuration.JSONConfig.SpeedModifier);
					IsEnabled = true;
				}
			}
			else
			{
				VRCPlayerApi vRCPlayerApi2 = VRCPlayer.field_Internal_Static_VRCPlayer_0?.field_Private_VRCPlayerApi_0;
				if (vRCPlayerApi2 != null)
				{
					vRCPlayerApi2.SetRunSpeed(OriginalRunSpeed);
					vRCPlayerApi2.SetStrafeSpeed(OriginalStrafeSpeed);
					vRCPlayerApi2.SetWalkSpeed(OriginalWalkSpeed);
					IsEnabled = false;
				}
			}
		}
	}
}
