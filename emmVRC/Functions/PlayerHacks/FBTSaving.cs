using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace emmVRC.Functions.PlayerHacks
{
	[Priority(55)]
	public class FBTSaving : MelonLoaderEvents
	{
		public static List<FBTAvatarCalibrationInfo> calibratedAvatars;

		public static Button.ButtonClickedEvent originalCalibrateButton;

		public static PropertyInfo leftFootInf;

		public static PropertyInfo rightFootInf;

		public static PropertyInfo hipInf;

		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized && !Environment.CurrentDirectory.Contains("vrchat-vrchat"))
			{
				SetupFBTSaving();
				_initialized = true;
			}
		}

		private static void SetupFBTSaving()
		{
			calibratedAvatars = new List<FBTAvatarCalibrationInfo>();
			originalCalibrateButton = ButtonAPI.menuPageBase.transform.Find("ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions").Find("SitStandCalibrateButton/Button_CalibrateFBT").GetComponentInChildren<Button>()
				.onClick;
			VRCTrackingSteam steam = Resources.FindObjectsOfTypeAll<VRCTrackingSteam>().First();
			leftFootInf = typeof(VRCTrackingSteam).GetProperties().First((PropertyInfo a) => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue(steam)).parent.name == "Puck1");
			rightFootInf = typeof(VRCTrackingSteam).GetProperties().First((PropertyInfo a) => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue(steam)).parent.name == "Puck2");
			hipInf = typeof(VRCTrackingSteam).GetProperties().First((PropertyInfo a) => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue(steam)).parent.name == "Puck3");
			ButtonAPI.menuPageBase.transform.Find("ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions").Find("SitStandCalibrateButton/Button_CalibrateFBT").GetComponentInChildren<Button>()
				.onClick = new Button.ButtonClickedEvent();
			ApiAvatar targetAvtr;
			ButtonAPI.menuPageBase.transform.Find("ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions").Find("SitStandCalibrateButton/Button_CalibrateFBT").GetComponentInChildren<Button>()
				.onClick.AddListener((Action)delegate
			{
				if (Configuration.JSONConfig.TrackingSaving && VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0 != null)
				{
					try
					{
						if (VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1 != null)
						{
							targetAvtr = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1;
						}
						else
						{
							targetAvtr = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0;
						}
						if (calibratedAvatars.FindIndex((FBTAvatarCalibrationInfo a) => a.AvatarID == targetAvtr.id) != -1)
						{
							calibratedAvatars.RemoveAll((FBTAvatarCalibrationInfo a) => a.AvatarID == targetAvtr.id);
						}
					}
					catch (Exception ex)
					{
						originalCalibrateButton.Invoke();
						emmVRCLoader.Logger.LogError("An error occured with FBT Saving. Invoking original calibration button. Error: " + ex.ToString());
					}
				}
				originalCalibrateButton.Invoke();
			});
		}

		public static IEnumerator SaveCalibrationInfo(VRCTrackingSteam trackingSteam, string avatarID)
		{
			yield return new WaitForSeconds(1f);
			if (calibratedAvatars.FindIndex((FBTAvatarCalibrationInfo a) => a.AvatarID == avatarID) != -1)
			{
				calibratedAvatars.RemoveAll((FBTAvatarCalibrationInfo a) => a.AvatarID == avatarID);
			}
			FBTAvatarCalibrationInfo item = new FBTAvatarCalibrationInfo
			{
				AvatarID = avatarID,
				LeftFootTrackerPosition = ((Transform)leftFootInf.GetValue(trackingSteam)).localPosition,
				LeftFootTrackerRotation = ((Transform)leftFootInf.GetValue(trackingSteam)).localRotation,
				RightFootTrackerPosition = ((Transform)rightFootInf.GetValue(trackingSteam)).localPosition,
				RightFootTrackerRotation = ((Transform)rightFootInf.GetValue(trackingSteam)).localRotation,
				HipTrackerPosition = ((Transform)hipInf.GetValue(trackingSteam)).localPosition,
				HipTrackerRotation = ((Transform)hipInf.GetValue(trackingSteam)).localRotation,
				PlayerHeight = VRCTrackingManager.field_Private_Static_VRCTrackingManager_0.GetPlayerHeight()
			};
			calibratedAvatars.Add(item);
			emmVRCLoader.Logger.LogDebug("Saved calibration info");
		}

		public static void LoadCalibrationInfo(VRCTrackingSteam trackingSteam, string avatarID)
		{
			if (calibratedAvatars.FindIndex((FBTAvatarCalibrationInfo a) => a.AvatarID == avatarID) != -1)
			{
				FBTAvatarCalibrationInfo fBTAvatarCalibrationInfo = calibratedAvatars.Find((FBTAvatarCalibrationInfo a) => a.AvatarID == avatarID);
				((Transform)leftFootInf.GetValue(trackingSteam)).localPosition = fBTAvatarCalibrationInfo.LeftFootTrackerPosition;
				((Transform)leftFootInf.GetValue(trackingSteam)).localRotation = fBTAvatarCalibrationInfo.LeftFootTrackerRotation;
				((Transform)rightFootInf.GetValue(trackingSteam)).localPosition = fBTAvatarCalibrationInfo.RightFootTrackerPosition;
				((Transform)rightFootInf.GetValue(trackingSteam)).localRotation = fBTAvatarCalibrationInfo.RightFootTrackerRotation;
				((Transform)hipInf.GetValue(trackingSteam)).localPosition = fBTAvatarCalibrationInfo.HipTrackerPosition;
				((Transform)hipInf.GetValue(trackingSteam)).localRotation = fBTAvatarCalibrationInfo.HipTrackerRotation;
				VRCTrackingManager.field_Private_Static_VRCTrackingManager_0.SetPlayerHeight(fBTAvatarCalibrationInfo.PlayerHeight);
			}
			emmVRCLoader.Logger.LogDebug("Loaded calibration info");
		}

		public static bool IsPreviouslyCalibrated(string avatarID)
		{
			return calibratedAvatars.FindIndex((FBTAvatarCalibrationInfo a) => a.AvatarID == avatarID) != -1;
		}
	}
}
