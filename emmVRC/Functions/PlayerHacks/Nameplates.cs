using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using MelonLoader;

namespace emmVRC.Functions.PlayerHacks
{
	public class Nameplates : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("NameplateColorChangingEnabled", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FriendNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("VisitorNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("NewUserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("KnownUserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("TrustedUserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("VeteranUserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("LegendaryUserNamePlateColorHex", global::emmVRC.Utils.PlayerUtils.ReloadAllAvatars));
			NetworkEvents.OnPlayerJoined += delegate
			{
				if (global::emmVRC.Functions.Core.ModCompatibility.OGTrustRank)
				{
					if (Configuration.JSONConfig.NameplateColorChangingEnabled)
					{
						VRCPlayer.field_Internal_Static_Color_1 = ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex);
						VRCPlayer.field_Internal_Static_Color_2 = ColorConversion.HexToColor(Configuration.JSONConfig.VisitorNamePlateColorHex);
						VRCPlayer.field_Internal_Static_Color_3 = ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex);
						VRCPlayer.field_Internal_Static_Color_4 = ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex);
						VRCPlayer.field_Internal_Static_Color_5 = ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex);
						VRCPlayer.field_Internal_Static_Color_6 = ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex);
						try
						{
							MelonHandler.Mods.First((MelonMod i) => i.Info.Name == "OGTrustRanks").Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("TrustedUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex));
							MelonHandler.Mods.First((MelonMod i) => i.Info.Name == "OGTrustRanks").Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("VeteranUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, ColorConversion.HexToColor(Configuration.JSONConfig.VeteranUserNamePlateColorHex));
							MelonHandler.Mods.First((MelonMod i) => i.Info.Name == "OGTrustRanks").Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("LegendaryUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, ColorConversion.HexToColor(Configuration.JSONConfig.LegendaryUserNamePlateColorHex));
						}
						catch (Exception ex)
						{
							Logger.LogError(ex.ToString());
						}
					}
				}
				else if (!Configuration.JSONConfig.NameplateColorChangingEnabled)
				{
					VRCPlayer.field_Internal_Static_Color_1 = ColorConversion.HexToColor("#FFFF00");
					VRCPlayer.field_Internal_Static_Color_2 = ColorConversion.HexToColor("#CCCCCC");
					VRCPlayer.field_Internal_Static_Color_3 = ColorConversion.HexToColor("#1778FF");
					VRCPlayer.field_Internal_Static_Color_4 = ColorConversion.HexToColor("#2BCE5C");
					VRCPlayer.field_Internal_Static_Color_5 = ColorConversion.HexToColor("#FF7B42");
					VRCPlayer.field_Internal_Static_Color_6 = ColorConversion.HexToColor("#8143E6");
				}
			};
		}
	}
}
