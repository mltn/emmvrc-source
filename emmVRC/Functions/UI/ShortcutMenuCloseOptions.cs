using System;
using emmVRC.Functions.Other;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	[Priority(50)]
	public class ShortcutMenuCloseOptions : MelonLoaderEvents
	{
		private static bool _initialized;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && !_initialized)
			{
				ButtonAPI.GetQuickMenuInstance().transform.Find("Container/Window/QMParent/Menu_Settings/QMHeader_H1/RightItemContainer/Button_QM_Exit/").GetComponent<Button>().onClick = new Button.ButtonClickedEvent();
				ButtonAPI.GetQuickMenuInstance().transform.Find("Container/Window/QMParent/Menu_Settings/QMHeader_H1/RightItemContainer/Button_QM_Exit/").GetComponent<Button>().onClick.AddListener((Action)delegate
				{
					ButtonAPI.GetQuickMenuInstance().ShowCustomDialog("Exit", "Really exit VRChat?", "Quit", "Restart", "Cancel", DestructiveActions.ForceQuit, DestructiveActions.ForceRestart);
				});
				_initialized = true;
			}
		}
	}
}
