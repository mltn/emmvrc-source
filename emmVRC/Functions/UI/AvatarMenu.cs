using System.Linq;
using emmVRC.Components;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using UnityEngine;

namespace emmVRC.Functions.UI
{
	public class AvatarMenu : MelonLoaderEvents
	{
		private static GameObject avatarScreen;

		private static GameObject hotWorldsViewPort;

		private static GameObject hotWorldsButton;

		private static GameObject randomWorldsViewPort;

		private static GameObject randomWorldsButton;

		private static GameObject personalList;

		private static GameObject legacyList;

		private static GameObject publicList;

		private static GameObject otherList;

		private static UiAvatarList otherListList;

		public override void OnUiManagerInit()
		{
			avatarScreen = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar").gameObject;
			hotWorldsViewPort = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (What's Hot)/ViewPort").gameObject;
			hotWorldsButton = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (What's Hot)/Button").gameObject;
			randomWorldsViewPort = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (Random)/ViewPort").gameObject;
			randomWorldsButton = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (Random)/Button").gameObject;
			personalList = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Personal Avatar List").gameObject;
			legacyList = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Legacy Avatar List").gameObject;
			publicList = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Public Avatar List").gameObject;
			otherList = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Licensed Avatar List (1)").gameObject;
			if (otherList == null)
			{
				otherList = avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Licensed Avatar List").gameObject;
			}
			if (otherList != null)
			{
				otherListList = otherList.GetComponent<UiAvatarList>();
			}
			avatarScreen.AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				if (Configuration.JSONConfig.DisableAvatarHotWorlds)
				{
					hotWorldsViewPort.SetActive(value: false);
					hotWorldsButton.SetActive(value: false);
				}
				else
				{
					hotWorldsViewPort.SetActive(value: true);
					hotWorldsButton.SetActive(value: true);
				}
				if (Configuration.JSONConfig.DisableAvatarRandomWorlds)
				{
					randomWorldsViewPort.SetActive(value: false);
					randomWorldsButton.SetActive(value: false);
				}
				else
				{
					randomWorldsViewPort.SetActive(value: true);
					randomWorldsButton.SetActive(value: true);
				}
				if (Configuration.JSONConfig.DisableAvatarPersonal)
				{
					personalList.SetActive(value: false);
				}
				else
				{
					personalList.SetActive(value: true);
				}
				if (Configuration.JSONConfig.DisableAvatarLegacy)
				{
					legacyList.SetActive(value: false);
				}
				else
				{
					legacyList.SetActive(value: true);
				}
				if (Configuration.JSONConfig.DisableAvatarPublic)
				{
					publicList.SetActive(value: false);
				}
				else
				{
					publicList.SetActive(value: true);
				}
				if (Configuration.JSONConfig.DisableAvatarOther && otherListList != null)
				{
					otherList.SetActive(value: false);
				}
				else if (!Configuration.JSONConfig.DisableAvatarOther && otherListList != null && otherListList.pickers.ToArray().Any((VRCUiContentButton a) => a.isActiveAndEnabled))
				{
					otherList.SetActive(value: true);
				}
			};
		}
	}
}
