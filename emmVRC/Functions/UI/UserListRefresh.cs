using System;
using System.Collections;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class UserListRefresh : MelonLoaderEvents
	{
		public static int cooldown;

		public static GameObject refreshButton;

		public override void OnUiManagerInit()
		{
			refreshButton = UnityEngine.Object.Instantiate(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social/UserProfileAndStatusSection/Status/EditStatusButton").gameObject, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social"));
			refreshButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(1260f, 370f);
			refreshButton.GetComponentInChildren<Text>().text = "<color=#FFFFFF>Refresh</color>";
			refreshButton.GetComponent<Button>().onClick = new Button.ButtonClickedEvent();
			refreshButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				RefreshMenu();
			});
		}

		public static void RefreshMenu()
		{
			if (cooldown <= 0)
			{
				foreach (UiUserList item in Resources.FindObjectsOfTypeAll<UiUserList>())
				{
					try
					{
						item.Method_Public_Void_0();
						item.Method_Public_Void_1();
					}
					catch (Exception ex)
					{
						emmVRCLoader.Logger.LogError(ex.ToString());
					}
					cooldown = 10;
					MelonCoroutines.Start(CooldownTimer());
				}
			}
			else
			{
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowAlert("emmVRC", "You must wait " + cooldown + " seconds before refreshing again.", 10f);
			}
		}

		public static IEnumerator CooldownTimer()
		{
			yield return new WaitForSecondsRealtime(cooldown);
			cooldown = 0;
		}
	}
}
