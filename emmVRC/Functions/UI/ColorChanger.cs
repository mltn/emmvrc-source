using System;
using System.Collections.Generic;
using System.Linq;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	[Priority(255)]
	internal class ColorChanger : MelonLoaderEvents
	{
		private static List<Image> normalColorImage;

		private static List<Image> dimmerColorImage;

		private static List<Image> darkerColorImage;

		private static List<Text> normalColorText;

		private static bool setupSkybox;

		private static GameObject loadingBackground;

		private static GameObject initialLoadingBackground;

		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIColorChangingEnabled", ApplyIfApplicable));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIColorHex", ApplyIfApplicable));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIActionMenuColorChangingEnabled", ApplyIfApplicable));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIMicIconColorChangingEnabled", ApplyIfApplicable));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIMicIconPulsingEnabled", ApplyIfApplicable));
			ApplyIfApplicable();
		}

		public static void ApplyIfApplicable()
		{
			Color color = (Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : Configuration.defaultMenuColor());
			Color color2 = new Color(color.r, color.g, color.b, 0.7f);
			new Color(color.r / 0.75f, color.g / 0.75f, color.b / 0.75f);
			Color color3 = new Color(color.r / 0.75f, color.g / 0.75f, color.b / 0.75f, 0.9f);
			Color color4 = new Color(color.r / 2.5f, color.g / 2.5f, color.b / 2.5f);
			Color color5 = new Color(color.r / 2.5f, color.g / 2.5f, color.b / 2.5f, 0.9f);
			if (normalColorImage == null || normalColorImage.Count == 0)
			{
				emmVRCLoader.Logger.LogDebug("Gathering elements to color normally...");
				normalColorImage = new List<Image>();
				GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent();
				normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Description_SafetyLevel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Custom/ON").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_None/ON").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Normal/ON").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Maxiumum/ON").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/InputKeypadPopup/Rectangle/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/InputKeypadPopup/InputField").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopupV2/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/InnerDashRing").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/RingGlow").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/UpdateStatusPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/InputPopup/InputField").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/UpdateStatusPopup/Popup/InputFieldStatus").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/AdvancedSettingsPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/AddToPlaylistPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/BookmarkFriendPopup/Popup/Panel (2)").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/EditPlaylistPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/PerformanceSettingsPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/AlertPopup/Lighter").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/RoomInstancePopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/ReportWorldPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/ReportUserPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/AddToAvatarFavoritesPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/SearchOptionsPopup/Popup/Panel (1)").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/SendInvitePopup/SendInviteMenu/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/RequestInvitePopup/RequestInviteMenu/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/ControllerBindingsPopup/Popup/Panel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/ChangeProfilePicPopup/Popup/PanelBackground").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Popups/ChangeProfilePicPopup/Popup/TitlePanel").GetComponent<Image>());
				normalColorImage.Add(gameObject.transform.Find("Screens/UserInfo/User Panel/PanelHeaderBackground").GetComponent<Image>());
				foreach (Transform item in from x in gameObject.GetComponentsInChildren<Transform>(includeInactive: true)
					where x.name.Contains("Panel_Header")
					select x)
				{
					foreach (Image componentsInChild in item.GetComponentsInChildren<Image>())
					{
						if (componentsInChild.gameObject.name != "Checkmark")
						{
							normalColorImage.Add(componentsInChild);
						}
					}
				}
				foreach (Transform item2 in from x in gameObject.GetComponentsInChildren<Transform>(includeInactive: true)
					where x.name.Contains("Handle")
					select x)
				{
					foreach (Image componentsInChild2 in item2.GetComponentsInChildren<Image>())
					{
						if (componentsInChild2.gameObject.name != "Checkmark")
						{
							normalColorImage.Add(componentsInChild2);
						}
					}
				}
				try
				{
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Panel_Backdrop").GetComponent<Image>());
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Decoration_Left").GetComponent<Image>());
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Decoration_Right").GetComponent<Image>());
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Panel_Backdrop").GetComponent<Image>());
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Decoration_Left").GetComponent<Image>());
					normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Decoration_Right").GetComponent<Image>());
				}
				catch (Exception)
				{
					new Exception();
				}
			}
			if (dimmerColorImage == null || dimmerColorImage.Count == 0)
			{
				emmVRCLoader.Logger.LogDebug("Gathering elements to color lighter...");
				dimmerColorImage = new List<Image>();
				GameObject gameObject2 = QuickMenuUtils.GetVRCUiMInstance().menuContent();
				dimmerColorImage.Add(gameObject2.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Custom/ON/TopPanel_SafetyLevel").GetComponent<Image>());
				dimmerColorImage.Add(gameObject2.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_None/ON/TopPanel_SafetyLevel").GetComponent<Image>());
				dimmerColorImage.Add(gameObject2.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Normal/ON/TopPanel_SafetyLevel").GetComponent<Image>());
				dimmerColorImage.Add(gameObject2.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Maxiumum/ON/TopPanel_SafetyLevel").GetComponent<Image>());
				dimmerColorImage.Add(gameObject2.transform.Find("Popups/ChangeProfilePicPopup/Popup/BorderImage").GetComponent<Image>());
				foreach (Transform item3 in from x in gameObject2.GetComponentsInChildren<Transform>(includeInactive: true)
					where x.name.Contains("Fill")
					select x)
				{
					foreach (Image componentsInChild3 in item3.GetComponentsInChildren<Image>())
					{
						if (componentsInChild3.gameObject.name != "Checkmark")
						{
							dimmerColorImage.Add(componentsInChild3);
						}
					}
				}
			}
			if (darkerColorImage == null || darkerColorImage.Count == 0)
			{
				emmVRCLoader.Logger.LogDebug("Gathering elements to color darker...");
				darkerColorImage = new List<Image>();
				GameObject gameObject3 = QuickMenuUtils.GetVRCUiMInstance().menuContent();
				darkerColorImage.Add(gameObject3.transform.Find("Popups/InputKeypadPopup/Rectangle").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/StandardPopupV2/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/StandardPopup/Rectangle").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/StandardPopup/MidRing").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/UpdateStatusPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/AdvancedSettingsPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/AddToPlaylistPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/BookmarkFriendPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/EditPlaylistPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/PerformanceSettingsPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/RoomInstancePopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/RoomInstancePopup/Popup/BorderImage (1)").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/ReportWorldPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/ReportUserPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/AddToAvatarFavoritesPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/SearchOptionsPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/SendInvitePopup/SendInviteMenu/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/RequestInvitePopup/RequestInviteMenu/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Popups/ControllerBindingsPopup/Popup/BorderImage").GetComponent<Image>());
				darkerColorImage.Add(gameObject3.transform.Find("Screens/UserInfo/ModerateDialog/Panel/BorderImage").GetComponent<Image>());
				foreach (Transform item4 in from x in gameObject3.GetComponentsInChildren<Transform>(includeInactive: true)
					where x.name.Contains("Background") && x.name != "PanelHeaderBackground" && !x.transform.parent.name.Contains("UserIcon")
					select x)
				{
					foreach (Image componentsInChild4 in item4.GetComponentsInChildren<Image>())
					{
						if (componentsInChild4.gameObject.name != "Checkmark")
						{
							darkerColorImage.Add(componentsInChild4);
						}
					}
				}
			}
			if (normalColorText == null || normalColorText.Count == 0)
			{
				emmVRCLoader.Logger.LogDebug("Gathering text elements to color...");
				normalColorText = new List<Text>();
				GameObject gameObject4 = QuickMenuUtils.GetVRCUiMInstance().menuContent();
				foreach (Text componentsInChild5 in gameObject4.transform.Find("Popups/InputPopup/Keyboard/Keys").GetComponentsInChildren<Text>(includeInactive: true))
				{
					normalColorText.Add(componentsInChild5);
				}
				foreach (Text componentsInChild6 in gameObject4.transform.Find("Popups/InputKeypadPopup/Keyboard/Keys").GetComponentsInChildren<Text>(includeInactive: true))
				{
					normalColorText.Add(componentsInChild6);
				}
				normalColorText.Add(gameObject4.transform.Find("Screens/Settings/VolumePanel/VolumeGameWorld/Label").GetComponentInChildren<Text>(includeInactive: true));
				normalColorText.Add(gameObject4.transform.Find("Screens/Settings/VolumePanel/VolumeGameVoice/Label").GetComponentInChildren<Text>(includeInactive: true));
				normalColorText.Add(gameObject4.transform.Find("Screens/Settings/VolumePanel/VolumeGameAvatars/Label").GetComponentInChildren<Text>(includeInactive: true));
				normalColorText.AddRange(gameObject4.transform.Find("Screens/Social/UserProfileAndStatusSection").GetComponentsInChildren<Text>(includeInactive: true));
				normalColorText.Add(gameObject4.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Loading Elements/txt_LOADING_Size").GetComponentInChildren<Text>(includeInactive: true));
				normalColorText.Add(gameObject4.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Loading Elements/txt_LOADING_Size").GetComponentInChildren<Text>(includeInactive: true));
			}
			emmVRCLoader.Logger.LogDebug("Coloring normal elements...");
			foreach (Image item5 in normalColorImage)
			{
				item5.color = color2;
			}
			emmVRCLoader.Logger.LogDebug("Coloring lighter elements...");
			foreach (Image item6 in dimmerColorImage)
			{
				item6.color = color3;
			}
			emmVRCLoader.Logger.LogDebug("Coloring darker elements...");
			foreach (Image item7 in darkerColorImage)
			{
				item7.color = color5;
			}
			emmVRCLoader.Logger.LogDebug("Coloring text elements...");
			foreach (Text item8 in normalColorText)
			{
				item8.color = color;
			}
			if (!setupSkybox && !global::emmVRC.Functions.Core.ModCompatibility.BetterLoadingScreen)
			{
				try
				{
					emmVRCLoader.Logger.LogDebug("Setting up skybox coloring...");
					loadingBackground = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/3DElements/LoadingBackground_TealGradient/SkyCube_Baked").gameObject;
					loadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", global::emmVRC.Functions.Core.Resources.blankGradient);
					loadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color.r / 2f, color.g / 2f, color.b / 2f, color.a));
					loadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", global::emmVRC.Functions.Core.Resources.blankGradient);
					setupSkybox = true;
					initialLoadingBackground = GameObject.Find("LoadingBackground_TealGradient_Music/SkyCube_Baked");
					initialLoadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", global::emmVRC.Functions.Core.Resources.blankGradient);
					initialLoadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color.r / 2f, color.g / 2f, color.b / 2f, color.a));
					initialLoadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", global::emmVRC.Functions.Core.Resources.blankGradient);
				}
				catch (Exception ex2)
				{
					emmVRCLoader.Logger.LogError(ex2.ToString());
				}
			}
			if (setupSkybox && loadingBackground != null && !global::emmVRC.Functions.Core.ModCompatibility.BetterLoadingScreen)
			{
				emmVRCLoader.Logger.LogDebug("Coloring skybox...");
				loadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color.r / 2f, color.g / 2f, color.b / 2f, color.a));
			}
			ColorBlock colorBlock = default(ColorBlock);
			colorBlock.colorMultiplier = 1f;
			colorBlock.disabledColor = Color.grey;
			colorBlock.highlightedColor = color * 1.5f;
			colorBlock.normalColor = color / 1.5f;
			colorBlock.pressedColor = new Color(1f, 1f, 1f, 1f);
			colorBlock.fadeDuration = 0.1f;
			colorBlock.selectedColor = color / 1.5f;
			ColorBlock colors = colorBlock;
			color.a = 0.9f;
			if (UnityEngine.Resources.FindObjectsOfTypeAll<HighlightsFXStandalone>().Count != 0)
			{
				UnityEngine.Resources.FindObjectsOfTypeAll<HighlightsFXStandalone>().FirstOrDefault().highlightColor = color;
			}
			try
			{
				GameObject gameObject5 = UnityEngine.Resources.FindObjectsOfTypeAll<FadeCycleEffect>().First().transform.parent.gameObject;
				if (Configuration.JSONConfig.UIMicIconColorChangingEnabled && Configuration.JSONConfig.UIColorChangingEnabled)
				{
					emmVRCLoader.Logger.LogDebug("Coloring Push To Talk icons...");
					foreach (Image componentsInChild7 in gameObject5.transform.GetComponentsInChildren<Image>())
					{
						if (componentsInChild7.gameObject.name != "PushToTalkKeybd" && componentsInChild7.gameObject.name != "PushToTalkXbox")
						{
							componentsInChild7.color = color;
						}
					}
				}
				else
				{
					emmVRCLoader.Logger.LogDebug("Decoloring Push To Talk icons...");
					foreach (Image componentsInChild8 in gameObject5.transform.GetComponentsInChildren<Image>())
					{
						if (componentsInChild8.gameObject.name != "PushToTalkKeybd" && componentsInChild8.gameObject.name != "PushToTalkXbox")
						{
							componentsInChild8.color = Color.red;
						}
					}
				}
				gameObject5.transform.Find("VoiceDotDisabled").GetComponent<FadeCycleEffect>().enabled = Configuration.JSONConfig.UIMicIconPulsingEnabled;
			}
			catch (Exception)
			{
				new Exception();
			}
			if (!(QuickMenuUtils.GetVRCUiMInstance().menuContent() != null))
			{
				return;
			}
			GameObject gameObject6 = QuickMenuUtils.GetVRCUiMInstance().menuContent();
			emmVRCLoader.Logger.LogDebug("Coloring input popup...");
			try
			{
				Transform transform = gameObject6.transform.Find("Popups/InputPopup");
				color4.a = 0.8f;
				transform.Find("Rectangle").GetComponent<Image>().color = color4;
				color4.a = 0.5f;
				color.a = 0.8f;
				transform.Find("Rectangle/Panel").GetComponent<Image>().color = color;
				color.a = 0.5f;
				Transform transform2 = gameObject6.transform.Find("Backdrop/Header/Tabs/ViewPort/Content/Search");
				transform2.Find("SearchTitle").GetComponent<Text>().color = color;
				transform2.Find("InputField").GetComponent<Image>().color = color;
			}
			catch (Exception ex4)
			{
				emmVRCLoader.Logger.LogError(ex4.ToString());
			}
			emmVRCLoader.Logger.LogDebug("Coloring QM buttons...");
			try
			{
				colorBlock = default(ColorBlock);
				colorBlock.colorMultiplier = 1f;
				colorBlock.disabledColor = Color.grey;
				colorBlock.highlightedColor = color4;
				colorBlock.normalColor = color;
				colorBlock.pressedColor = Color.gray;
				colorBlock.fadeDuration = 0.1f;
				ColorBlock colors2 = colorBlock;
				gameObject6.GetComponentsInChildren<Transform>(includeInactive: true).FirstOrDefault((Transform x) => x.name == "Row:4 Column:0").GetComponent<Button>()
					.colors = colors;
				color.a = 0.5f;
				color4.a = 1f;
				colors2.normalColor = color4;
				foreach (Slider componentsInChild9 in gameObject6.GetComponentsInChildren<Slider>(includeInactive: true))
				{
					componentsInChild9.colors = colors2;
				}
				color4.a = 0.5f;
				colors2.normalColor = color;
				foreach (Button componentsInChild10 in gameObject6.GetComponentsInChildren<Button>(includeInactive: true))
				{
					if (componentsInChild10.gameObject.GetComponentsInChildren<Transform>(includeInactive: true).Any((Transform a) => a.name == "emmVRCDoNotColor") || (componentsInChild10.name != "SubscribeToAddPhotosButton" && componentsInChild10.name != "SupporterButton" && componentsInChild10.name != "ModerateButton" && componentsInChild10.transform.parent.name != "VRC+PageTab" && (componentsInChild10.name != "ReportButton" || componentsInChild10.transform.parent.name.Contains("WorldInfo"))))
					{
						componentsInChild10.colors = colors;
					}
				}
				gameObject6 = GameObject.Find("QuickMenu");
				foreach (Button componentsInChild11 in gameObject6.GetComponentsInChildren<Button>(includeInactive: true))
				{
					if (componentsInChild11.name != "rColorButton" && componentsInChild11.name != "gColorButton" && componentsInChild11.name != "bColorButton" && componentsInChild11.name != "ColorPickPreviewButton")
					{
						componentsInChild11.colors = colors;
					}
				}
				foreach (UiToggleButton componentsInChild12 in gameObject6.GetComponentsInChildren<UiToggleButton>(includeInactive: true))
				{
					foreach (Image componentsInChild13 in componentsInChild12.GetComponentsInChildren<Image>(includeInactive: true))
					{
						componentsInChild13.color = color;
					}
				}
				foreach (Slider componentsInChild14 in gameObject6.GetComponentsInChildren<Slider>(includeInactive: true))
				{
					componentsInChild14.colors = colors2;
					foreach (Image componentsInChild15 in componentsInChild14.GetComponentsInChildren<Image>(includeInactive: true))
					{
						componentsInChild15.color = color;
					}
				}
				foreach (Toggle componentsInChild16 in gameObject6.GetComponentsInChildren<Toggle>(includeInactive: true))
				{
					componentsInChild16.colors = colors2;
					foreach (Image componentsInChild17 in componentsInChild16.GetComponentsInChildren<Image>(includeInactive: true))
					{
						if (componentsInChild17.gameObject.name != "Checkmark")
						{
							componentsInChild17.color = color;
						}
					}
				}
				foreach (Image componentsInChild18 in GameObject.Find("UserInterface/QuickMenu/QuickModeMenus/QuickModeNotificationsMenu/ScrollRect").GetComponentsInChildren<Image>(includeInactive: true))
				{
					if (componentsInChild18.transform.name == "Background")
					{
						componentsInChild18.color = color;
					}
				}
				foreach (MonoBehaviourPublicObCoGaCoObCoObCoUnique componentsInChild19 in GameObject.Find("UserInterface/QuickMenu/QuickModeTabs").GetComponentsInChildren<MonoBehaviourPublicObCoGaCoObCoObCoUnique>())
				{
					Color color6 = new Color(color.r / 2.25f, color.g / 2.25f, color.b / 2.25f);
					componentsInChild19.field_Public_Color32_0 = color6;
				}
			}
			catch (Exception)
			{
				new Exception();
			}
			if (!Configuration.JSONConfig.UIActionMenuColorChangingEnabled)
			{
				return;
			}
			try
			{
				emmVRCLoader.Logger.LogDebug("Coloring Action Menu...");
				Color color7 = (Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : new Color(Configuration.defaultMenuColor().r * 1.5f, Configuration.defaultMenuColor().g * 1.5f, Configuration.defaultMenuColor().b * 1.5f));
				new Color(color7.r, color7.g, color7.b, color7.a / 1.25f);
				foreach (PedalGraphic item9 in UnityEngine.Resources.FindObjectsOfTypeAll<PedalGraphic>())
				{
					item9.color = color7;
				}
				foreach (ActionMenu item10 in UnityEngine.Resources.FindObjectsOfTypeAll<ActionMenu>())
				{
					_ = item10;
				}
			}
			catch (Exception ex6)
			{
				emmVRCLoader.Logger.LogError(ex6.ToString());
			}
		}
	}
}
