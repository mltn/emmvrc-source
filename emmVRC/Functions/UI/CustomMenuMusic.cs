using System;
using System.Collections;
using System.IO;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;
using UnityEngine;
using UnityEngine.Networking;

namespace emmVRC.Functions.UI
{
	public class CustomMenuMusic : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			emmVRCLoader.Logger.LogDebug("Initializing custom menu music...");
			MelonCoroutines.Start(Initialize());
		}

		public static IEnumerator Initialize()
		{
			if (!global::emmVRC.Functions.Core.ModCompatibility.BetterLoadingScreen)
			{
				if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic")))
				{
					Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic"));
				}
				if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/custommenu.ogg")))
				{
					try
					{
						File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/custommenu.ogg"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic/custommenu.ogg"));
					}
					catch (Exception)
					{
						new Exception();
						emmVRCLoader.Logger.LogError("A custommenu.ogg was detected, but you already have one in the CustomMenuMusic folder. Please put custom menu music in the Ogg Vorbis format into UserData/emmVRC/CustomMenuMusic instead.");
					}
				}
				string[] files = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic"));
				if (files.Length >= 1)
				{
					emmVRCLoader.Logger.Log("Processing custom menu music...");
					int num = new System.Random().Next(files.Length);
					emmVRCLoader.Logger.LogDebug("Picked track: " + files[num]);
					GameObject loadingMusic1 = GameObject.Find("LoadingBackground_TealGradient_Music/LoadingSound");
					GameObject loadingMusic2 = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/LoadingSound").gameObject;
					if (loadingMusic1 != null)
					{
						loadingMusic1.GetComponent<AudioSource>().Stop();
					}
					if (loadingMusic2 != null)
					{
						loadingMusic2.GetComponent<AudioSource>().Stop();
					}
					UnityWebRequest CustomLoadingMusicRequest = UnityWebRequest.Get($"file://{files[num]}".Replace("\\", "/"));
					CustomLoadingMusicRequest.SendWebRequest();
					while (!CustomLoadingMusicRequest.isDone)
					{
						yield return null;
					}
					emmVRCLoader.Logger.LogDebug("Request sent and returned");
					AudioClip audioClip = null;
					if (CustomLoadingMusicRequest.isHttpError)
					{
						emmVRCLoader.Logger.LogError("Could not load music file: " + CustomLoadingMusicRequest.error);
					}
					else
					{
						audioClip = WebRequestWWW.InternalCreateAudioClipUsingDH(CustomLoadingMusicRequest.downloadHandler, CustomLoadingMusicRequest.url, stream: false, compressed: false, AudioType.UNKNOWN);
					}
					if (audioClip != null)
					{
						if (loadingMusic1 != null)
						{
							loadingMusic1.GetComponent<AudioSource>().clip = audioClip;
							loadingMusic1.GetComponent<AudioSource>().Play();
						}
						if (loadingMusic2 != null)
						{
							loadingMusic2.GetComponent<AudioSource>().clip = audioClip;
							loadingMusic2.GetComponent<AudioSource>().Play();
						}
					}
				}
			}
			yield return null;
		}
	}
}
