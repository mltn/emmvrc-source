using System;
using System.IO;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class PlayerNotes : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes")))
			{
				Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes"));
			}
		}

		public static void LoadNote(string userID, string displayName)
		{
			try
			{
				PlayerNote loadedNote;
				if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json")))
				{
					string text = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"));
					if (text.Contains("emmVRC.Hacks"))
					{
						text = text.Replace("emmVRC.Hacks", "emmVRC.Functions.UI");
						File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"), text);
					}
					loadedNote = Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"))).Make<PlayerNote>();
				}
				else
				{
					loadedNote = new PlayerNote
					{
						UserID = userID,
						NoteText = ""
					};
				}
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("Note for " + displayName, string.IsNullOrWhiteSpace(loadedNote.NoteText) ? "There is currently no note for this user." : loadedNote.NoteText, "Change Note", delegate
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter a note for " + displayName + ":", loadedNote.NoteText, InputField.InputType.Standard, keypad: false, "Accept", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate(string newNoteText, List<KeyCode> keyk, Text tx)
					{
						SaveNote(new PlayerNote
						{
							UserID = userID,
							NoteText = newNoteText
						});
					}), null, "Enter note....");
				});
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError("Failed to load note: " + ex.ToString());
			}
		}

		public static void LoadNoteQM(string userID, string displayName)
		{
			try
			{
				PlayerNote loadedNote;
				if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json")))
				{
					string text = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"));
					if (text.Contains("emmVRC.Hacks"))
					{
						text = text.Replace("emmVRC.Hacks", "emmVRC.Functions.UI");
						File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"), text);
					}
					loadedNote = Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"))).Make<PlayerNote>();
				}
				else
				{
					loadedNote = new PlayerNote
					{
						UserID = userID,
						NoteText = ""
					};
				}
				ButtonAPI.GetQuickMenuInstance().ShowCustomDialog("Note for " + displayName, string.IsNullOrWhiteSpace(loadedNote.NoteText) ? "There is currently no note for this user." : loadedNote.NoteText, "", "Change Note", "Close", null, delegate
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter a note for " + displayName + ":", loadedNote.NoteText, InputField.InputType.Standard, keypad: false, "Accept", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate(string newNoteText, List<KeyCode> keyk, Text tx)
					{
						SaveNote(new PlayerNote
						{
							UserID = userID,
							NoteText = newNoteText
						});
					}), null, "Enter note....");
				});
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError("Failed to load note: " + ex.ToString());
			}
		}

		public static void SaveNote(PlayerNote note)
		{
			File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + note.UserID + ".json"), Encoder.Encode(note, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
		}
	}
}
