using System;
using System.Collections.Generic;
using emmVRC.Objects.ModuleBases;
using UnityEngine;

namespace emmVRC.Functions.UI
{
	public class UnscaledUI : MelonLoaderEvents
	{
		private static GameObject micTooltipDesktop;

		private static GameObject micTooltipXbox;

		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("DisableMicTooltip", Process));
			Process();
		}

		public static void Process()
		{
			if (micTooltipDesktop == null)
			{
				micTooltipDesktop = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/VoiceDotParent/PushToTalkKeybd");
			}
			if (micTooltipXbox == null)
			{
				micTooltipXbox = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/VoiceDotParent/PushToTalkXbox");
			}
			micTooltipDesktop.transform.localScale = (Configuration.JSONConfig.DisableMicTooltip ? Vector3.zero : Vector3.one);
			micTooltipXbox.transform.localScale = (Configuration.JSONConfig.DisableMicTooltip ? Vector3.zero : Vector3.one);
		}
	}
}
