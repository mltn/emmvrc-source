using emmVRC.Components;
using emmVRC.Functions.Core;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class MainMenu : MelonLoaderEvents
	{
		public static bool menuJustOpened;

		public static bool menuOpen;

		public static GameObject vrcPlusButton;

		public static GameObject userIconsButton;

		public static GameObject worldButton;

		public static GameObject avatarButton;

		public static GameObject socialButton;

		public static GameObject settingsButton;

		public static GameObject safetyButton;

		public static GameObject vrcPlusGetMoreFavorites;

		public override void OnUiManagerInit()
		{
			if (global::emmVRC.Functions.Core.ModCompatibility.VRCMinus)
			{
				return;
			}
			GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs").AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				if (vrcPlusButton == null)
				{
					vrcPlusButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/VRC+PageTab/");
					userIconsButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/GalleryTab/");
					worldButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/WorldsPageTab/");
					avatarButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/AvatarPageTab/");
					socialButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SocialPageTab/");
					settingsButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SettingsPageTab/");
					safetyButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SafetyPageTab/");
					vrcPlusGetMoreFavorites = GameObject.Find("/UserInterface/MenuContent/Screens/Avatar/Vertical Scroll View/Viewport/Content/Favorite Avatar List/GetMoreFavorites/");
					worldButton.GetComponent<LayoutElement>().preferredWidth = 250f;
					avatarButton.GetComponent<LayoutElement>().preferredWidth = 250f;
					socialButton.GetComponent<LayoutElement>().preferredWidth = 250f;
					settingsButton.GetComponent<LayoutElement>().preferredWidth = 250f;
					safetyButton.GetComponent<LayoutElement>().preferredWidth = 250f;
				}
				if (Configuration.JSONConfig.DisableVRCPlusMenuTabs)
				{
					vrcPlusButton.SetActive(value: false);
					userIconsButton.GetComponent<LayoutElement>().enabled = false;
					userIconsButton.GetComponent<Image>().enabled = false;
					userIconsButton.transform.Find("Image_NEW").gameObject.SetActive(value: false);
					userIconsButton.SetActive(value: false);
					vrcPlusGetMoreFavorites.transform.localScale = Vector3.zero;
				}
				else
				{
					vrcPlusButton.SetActive(value: true);
					userIconsButton.GetComponent<LayoutElement>().enabled = true;
					userIconsButton.GetComponent<Image>().enabled = true;
					userIconsButton.transform.Find("Image_NEW").gameObject.SetActive(value: true);
					userIconsButton.SetActive(value: true);
					vrcPlusGetMoreFavorites.transform.localScale = Vector3.one;
				}
			};
		}
	}
}
