using System;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class Volume : MelonLoaderEvents
	{
		private static Slider UIVolumeSlider;

		private static Slider WorldVolumeSlider;

		private static Slider VoicesVolumeSlider;

		private static Slider AvatarVolumeSlider;

		private static GameObject UIVolumeMuteButton;

		private static GameObject WorldVolumeMuteButton;

		private static GameObject VoiceVolumeMuteButton;

		private static GameObject AvatarVolumeMuteButton;

		public override void OnUiManagerInit()
		{
			UIVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeUi").GetComponent<Slider>();
			UIVolumeSlider.onValueChanged.AddListener((Action<float>)delegate(float flt)
			{
				if (Configuration.JSONConfig.UIVolumeMute && flt != 0f)
				{
					Configuration.WriteConfigOption("UIVolumeMute", false);
					UIVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.UIVolumeMute ? "U" : "M");
				}
			});
			WorldVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameWorld").GetComponent<Slider>();
			WorldVolumeSlider.onValueChanged.AddListener((Action<float>)delegate(float flt)
			{
				if (Configuration.JSONConfig.WorldVolumeMute && flt != 0f)
				{
					Configuration.WriteConfigOption("WorldVolumeMute", false);
					WorldVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.WorldVolumeMute ? "U" : "M");
				}
			});
			VoicesVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameVoice").GetComponent<Slider>();
			VoicesVolumeSlider.onValueChanged.AddListener((Action<float>)delegate(float flt)
			{
				if (Configuration.JSONConfig.VoiceVolumeMute && flt != 0f)
				{
					Configuration.WriteConfigOption("VoiceVolumeMute", false);
					VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.VoiceVolumeMute ? "U" : "M");
				}
			});
			AvatarVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameAvatars").GetComponent<Slider>();
			AvatarVolumeSlider.onValueChanged.AddListener((Action<float>)delegate(float flt)
			{
				if (Configuration.JSONConfig.AvatarVolumeMute && flt != 0f)
				{
					Configuration.WriteConfigOption("AvatarVolumeMute", false);
					AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.AvatarVolumeMute ? "U" : "M");
				}
			});
			UIVolumeMuteButton = UnityEngine.Object.Instantiate(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/Footer/Exit").gameObject, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel").transform);
			UIVolumeMuteButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(5.25f, 1.75f);
			UIVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(180f, 468.5f);
			UIVolumeMuteButton.GetComponentInChildren<Text>().fontSize = (int)((double)UIVolumeMuteButton.GetComponentInChildren<Text>().fontSize / 1.75);
			UIVolumeMuteButton.GetComponent<Button>().onClick = new Button.ButtonClickedEvent();
			WorldVolumeMuteButton = UnityEngine.Object.Instantiate(UIVolumeMuteButton, UIVolumeMuteButton.transform.parent);
			WorldVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0f, 47f);
			VoiceVolumeMuteButton = UnityEngine.Object.Instantiate(WorldVolumeMuteButton, UIVolumeMuteButton.transform.parent);
			VoiceVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0f, 47f);
			AvatarVolumeMuteButton = UnityEngine.Object.Instantiate(VoiceVolumeMuteButton, UIVolumeMuteButton.transform.parent);
			AvatarVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0f, 47f);
			UIVolumeMuteButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				if (!Configuration.JSONConfig.UIVolumeMute)
				{
					Configuration.WriteConfigOption("UIVolumeMute", true);
					Configuration.WriteConfigOption("UIVolume", UIVolumeSlider.value);
					UIVolumeSlider.value = 0f;
					UIVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
				}
				else
				{
					Configuration.WriteConfigOption("UIVolumeMute", false);
					UIVolumeSlider.value = Configuration.JSONConfig.UIVolume;
					UIVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
				}
			});
			WorldVolumeMuteButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				if (!Configuration.JSONConfig.WorldVolumeMute)
				{
					Configuration.WriteConfigOption("WorldVolumeMute", true);
					Configuration.WriteConfigOption("WorldVolume", UIVolumeSlider.value);
					WorldVolumeSlider.value = 0f;
					WorldVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
				}
				else
				{
					Configuration.WriteConfigOption("WorldVolumeSlider", false);
					WorldVolumeSlider.value = Configuration.JSONConfig.WorldVolume;
					WorldVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
				}
			});
			VoiceVolumeMuteButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				if (!Configuration.JSONConfig.VoiceVolumeMute)
				{
					Configuration.WriteConfigOption("VoiceVolumeMute", true);
					Configuration.WriteConfigOption("VoiceVolume", UIVolumeSlider.value);
					VoicesVolumeSlider.value = 0f;
					VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
				}
				else
				{
					Configuration.WriteConfigOption("VoiceVolumeMute", false);
					VoicesVolumeSlider.value = Configuration.JSONConfig.VoiceVolume;
					VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
				}
			});
			AvatarVolumeMuteButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				if (!Configuration.JSONConfig.AvatarVolumeMute)
				{
					Configuration.WriteConfigOption("AvatarVolumeMute", true);
					Configuration.WriteConfigOption("AvatarVolume", UIVolumeSlider.value);
					AvatarVolumeSlider.value = 0f;
					AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
				}
				else
				{
					Configuration.WriteConfigOption("AvatarVolumeMute", false);
					AvatarVolumeSlider.value = Configuration.JSONConfig.AvatarVolume;
					AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
				}
			});
			UIVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.UIVolumeMute ? "U" : "M");
			WorldVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.WorldVolumeMute ? "U" : "M");
			VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.VoiceVolumeMute ? "U" : "M");
			AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = (Configuration.JSONConfig.AvatarVolumeMute ? "U" : "M");
		}
	}
}
