using System;
using System.Collections;
using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Functions.Debug;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using MelonLoader;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using VRC;

namespace emmVRC.Functions.UI
{
	public class DesktopHUD : MelonLoaderEvents, IWithUpdate
	{
		private static GameObject CanvasObject;

		private static GameObject BackgroundObject;

		private static GameObject TextObject;

		private static GameObject LogoIconContainer;

		private static Image BackgroundImage;

		private static Image emmLogo;

		private static Text TextText;

		private static bool keyFlag;

		public static bool UIExpanded = false;

		public static bool Initialized = false;

		public static bool enabled = true;

		private static CanvasScaler scaler;

		public override void OnUiManagerInit()
		{
			if (!XRDevice.isPresent)
			{
				MelonCoroutines.Start(Initialize());
				Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("VRHUDInDesktop", delegate
				{
					enabled = !Configuration.JSONConfig.VRHUDInDesktop;
				}));
			}
		}

		private static IEnumerator Initialize()
		{
			while (global::emmVRC.Functions.Core.Resources.HUD_Minimized == null)
			{
				yield return null;
			}
			CanvasObject = new GameObject("emmVRCDesktopHUDCanvas");
			UnityEngine.Object.DontDestroyOnLoad(CanvasObject);
			CanvasObject.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
			CanvasObject.transform.position = Vector3.zero;
			scaler = CanvasObject.AddComponent<CanvasScaler>();
			scaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
			BackgroundObject = new GameObject("Background");
			BackgroundObject.AddComponent<CanvasRenderer>();
			BackgroundImage = BackgroundObject.AddComponent<Image>();
			BackgroundObject.GetComponent<RectTransform>().anchorMin = new Vector2(0f, 1f);
			BackgroundObject.GetComponent<RectTransform>().anchorMax = new Vector2(0f, 1f);
			BackgroundObject.GetComponent<RectTransform>().pivot = new Vector2(0f, 1f);
			BackgroundObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -5f);
			BackgroundObject.GetComponent<RectTransform>().sizeDelta = new Vector2(325f, 768f);
			BackgroundObject.transform.SetParent(CanvasObject.transform, worldPositionStays: false);
			BackgroundImage.sprite = global::emmVRC.Functions.Core.Resources.HUD_Minimized;
			TextObject = new GameObject("Text");
			TextObject.AddComponent<CanvasRenderer>();
			TextObject.transform.SetParent(BackgroundObject.transform, worldPositionStays: false);
			TextText = TextObject.AddComponent<Text>();
			TextText.font = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
			TextText.fontSize = 15;
			TextText.text = "";
			TextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(310f, 768f);
			TextObject.GetComponent<RectTransform>().localPosition += new Vector3(0f, 3f, 0f);
			LogoIconContainer = new GameObject("emmVRCHUDLogo");
			LogoIconContainer.AddComponent<CanvasRenderer>();
			LogoIconContainer.transform.SetParent(BackgroundObject.transform, worldPositionStays: false);
			emmLogo = LogoIconContainer.AddComponent<Image>();
			emmLogo.sprite = global::emmVRC.Functions.Core.Resources.emmHUDLogo;
			emmLogo.GetComponent<RectTransform>().anchorMin = new Vector2(0f, 1f);
			emmLogo.GetComponent<RectTransform>().anchorMax = new Vector2(0f, 1f);
			emmLogo.GetComponent<RectTransform>().pivot = new Vector2(0f, 1f);
			emmLogo.GetComponent<RectTransform>().anchoredPosition = new Vector2(10f, -2.5f);
			emmLogo.GetComponent<RectTransform>().localScale = new Vector3(0.58f, 0.58f, 0.58f);
			CanvasObject.SetActive(value: false);
			Initialized = true;
		}

		public void OnUpdate()
		{
			if (CanvasObject == null || BackgroundObject == null)
			{
				return;
			}
			CanvasObject.SetActive(Configuration.JSONConfig.HUDEnabled && enabled);
			if (!CanvasObject.activeSelf)
			{
				return;
			}
			if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKey(KeyCode.E) && !keyFlag)
			{
				UIExpanded = !UIExpanded;
				if (UIExpanded)
				{
					BackgroundImage.sprite = global::emmVRC.Functions.Core.Resources.HUD_Base;
				}
				else
				{
					BackgroundImage.sprite = global::emmVRC.Functions.Core.Resources.HUD_Minimized;
				}
				keyFlag = true;
			}
			if ((Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[1]) || Configuration.JSONConfig.ToggleHUDEnabledKeybind[1] == 0) && Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[0]) && !keyFlag && Configuration.JSONConfig.EnableKeybinds)
			{
				Configuration.WriteConfigOption("HUDEnabled", !Configuration.JSONConfig.HUDEnabled);
				keyFlag = true;
			}
			if (!Input.GetKey(KeyCode.E) && !Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[0]) && keyFlag)
			{
				keyFlag = false;
			}
			if ((bool)TextText)
			{
				string text = "";
				text = CommonHUD.RenderPlayerList();
				TextText.text = "\n                  <color=#FF69B4>emmVRC</color>                        fps: " + Mathf.Floor(1f / Time.deltaTime) + "\n                  " + (UIExpanded ? "press 'CTRL+E' to close" : "press 'CTRL+E' to open") + (UIExpanded ? ("\n\n\nUsers in room" + ((PlayerManager.prop_PlayerManager_0 != null && RoomManager.field_Internal_Static_ApiWorldInstance_0 != null) ? (" (" + PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.get_Count() + ")") : "") + ":\n" + text + "\n\nPosition in world:\n" + CommonHUD.RenderWorldInfo() + "\n\n" + ((!Configuration.JSONConfig.emmVRCNetworkEnabled) ? "" : ((NetworkClient.webToken != null) ? "<color=lime>Connected to the\nemmVRC Network</color>" : "<color=red>Not connected to the\nemmVRC Network</color>")) + "\n\n" + (Attributes.Debug ? ("Current frame time: " + FrameTimeCalculator.frameTimes[(FrameTimeCalculator.iterator == 0) ? (FrameTimeCalculator.frameTimes.Length - 1) : (FrameTimeCalculator.iterator - 1)] + "ms\nAverage frame time: " + FrameTimeCalculator.frameTimeAvg + "ms\n") : "")) : "");
			}
		}
	}
}
