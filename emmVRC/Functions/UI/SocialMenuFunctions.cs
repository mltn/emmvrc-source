using System;
using System.Collections;
using emmVRC.Components;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using MelonLoader;
using UnityEngine;
using UnityEngine.UI;
using VRC;
using VRC.SDKBase;
using VRC.UI;

namespace emmVRC.Functions.UI
{
	internal class SocialMenuFunctions : MelonLoaderEvents
	{
		private static GameObject SocialFunctionsButton;

		public static GameObject UserSendMessage;

		private static GameObject UserNotes;

		private static GameObject TeleportButton;

		private static GameObject AvatarSearchButton;

		private static GameObject VRCPlusSupporterButton;

		private static GameObject VRCPlusEarlyAdopterIcon;

		private static GameObject VRCPlusSubscriberIcon;

		private static GameObject ToggleBlockButton;

		private static GameObject PortalToUserButton;

		private static int PortalCooldownTimer;

		public override void OnUiManagerInit()
		{
			SocialFunctionsButton = UnityEngine.Object.Instantiate(GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn/FavoriteButton"), GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn").transform);
			SocialFunctionsButton.GetComponentInChildren<Text>().text = "<color=#FF69B4>emmVRC</color> Functions";
			SocialFunctionsButton.GetComponentInChildren<Button>().onClick = new Button.ButtonClickedEvent();
			UserSendMessage = UnityEngine.Object.Instantiate(SocialFunctionsButton, SocialFunctionsButton.transform.parent);
			UserSendMessage.GetComponentInChildren<Text>().text = "Send Message";
			UserSendMessage.SetActive(value: false);
			UserNotes = UnityEngine.Object.Instantiate(UserSendMessage, SocialFunctionsButton.transform.parent);
			UserNotes.GetComponentInChildren<Text>().text = "Notes";
			UserNotes.SetActive(value: false);
			TeleportButton = UnityEngine.Object.Instantiate(UserNotes, SocialFunctionsButton.transform.parent);
			TeleportButton.GetComponentInChildren<Text>().text = "Teleport";
			TeleportButton.SetActive(value: false);
			AvatarSearchButton = UnityEngine.Object.Instantiate(TeleportButton, SocialFunctionsButton.transform.parent);
			AvatarSearchButton.GetComponentInChildren<Text>().text = "Search Avatars";
			AvatarSearchButton.SetActive(value: false);
			ToggleBlockButton = UnityEngine.Object.Instantiate(AvatarSearchButton, SocialFunctionsButton.transform.parent);
			ToggleBlockButton.GetComponentInChildren<Text>().text = "<color=#FF69B4>emmVRC</color> Block";
			ToggleBlockButton.SetActive(value: false);
			PortalToUserButton = UnityEngine.Object.Instantiate(ToggleBlockButton, SocialFunctionsButton.transform.parent);
			PortalToUserButton.GetComponentInChildren<Text>().text = "Drop Portal";
			PortalToUserButton.SetActive(value: false);
			SocialFunctionsButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
			{
				UserNotes.SetActive(!UserNotes.activeSelf);
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed)
				{
					TeleportButton.SetActive(!TeleportButton.activeSelf);
				}
				else
				{
					TeleportButton.SetActive(value: false);
				}
				if (NetworkClient.webToken != null && Configuration.JSONConfig.AvatarFavoritesEnabled)
				{
					AvatarSearchButton.SetActive(!AvatarSearchButton.activeSelf);
				}
				try
				{
					if (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>() != null && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0() != null && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location != "private" && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location != "" && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.statusIsSetToOffline && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location.Contains("friends"))
					{
						PortalToUserButton.SetActive(!PortalToUserButton.activeSelf);
					}
					else
					{
						PortalToUserButton.SetActive(value: false);
					}
				}
				catch
				{
				}
				try
				{
					GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale = ((GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale == Vector3.zero) ? Vector3.one : Vector3.zero);
				}
				catch
				{
				}
			});
			UserSendMessage.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
			{
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Send a message to " + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
					.get_field_Private_APIUser_0()
					.GetName() + ":", "", InputField.InputType.Standard, keypad: false, "Send", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate
				{
				}), null, "Enter message...");
			});
			UserNotes.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
			{
				PlayerNotes.LoadNote(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
					.get_field_Private_APIUser_0()
					.id, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.GetName());
				});
				TeleportButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
				{
					if (RiskyFunctionsManager.AreRiskyFunctionsAllowed)
					{
						Player plrToTP = null;
						PlayerUtils.GetEachPlayer(delegate(Player plr)
						{
							if (plr.prop_APIUser_0.id == QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
								.get_field_Private_APIUser_0()
								.id)
							{
								plrToTP = plr;
							}
						});
						if (plrToTP != null)
						{
							VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position = plrToTP._vrcplayer.transform.position;
						}
						try
						{
							CustomAvatarFavorites.baseChooseEvent.Invoke();
						}
						catch
						{
						}
					}
				});
				AvatarSearchButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
				{
					if (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0() != null)
					{
						VRCUiManager.prop_VRCUiManager_0.ShowScreen(VRCUiManager.prop_VRCUiManager_0.menuContent().transform.Find("Screens/Avatar").GetComponent<VRCUiPage>());
						MelonCoroutines.Start(CustomAvatarFavorites.SearchAvatarsAfterDelay(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
							.get_field_Private_APIUser_0()
							.GetName()));
					}
				});
				PortalToUserButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
				{
					//IL_0139: Unknown result type (might be due to invalid IL or missing references)
					if (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location != "private" && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.statusIsSetToOffline && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location != "" && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
						.get_field_Private_APIUser_0()
						.location.Contains("friends"))
					{
						try
						{
							if (PortalCooldownTimer == 0)
							{
								string[] array = QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
									.get_field_Private_APIUser_0()
									.location.Split(':');
								GameObject targetObject = Networking.Instantiate(VRC_EventHandler.VrcBroadcastType.Always, "Portals/PortalInternalDynamic", VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position + VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.forward * 2f, VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation);
								Object[] obj = new Object[3]
								{
									(Object)String.op_Implicit(array[0]),
									(Object)String.op_Implicit(array[1]),
									default(Object)
								};
								Int32 val = new Int32
								{
									m_value = 0
								};
								obj[2] = ((Int32)(ref val)).BoxIl2CppObject();
								Networking.RPC(RPC.Destination.AllBufferOne, targetObject, "ConfigurePortal", (Object[])(object)obj);
								PortalCooldownTimer = 5;
								MelonCoroutines.Start(PortalCooldown());
							}
							else
							{
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You must wait " + PortalCooldownTimer + " seconds before dropping another portal.", "Dismiss", delegate
								{
									VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
								});
							}
						}
						catch (Exception ex)
						{
							emmVRCLoader.Logger.LogError(ex.ToString());
						}
					}
					else
					{
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You cannot drop a portal to this user.", "Dismiss", delegate
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
						});
					}
				});
				ToggleBlockButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
				{
					if (NetworkClient.webToken != null)
					{
						HTTPRequest.post(NetworkClient.baseURL + "/api/blocked/" + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>()
							.get_field_Private_APIUser_0()
							.id, null).NoAwait("emmVRC block");
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowAlert("emmVRC", "The block state for this user has been toggled.", 5f);
					}
				});
				EnableDisableListener enableDisableListener = SocialFunctionsButton.transform.parent.gameObject.AddComponent<EnableDisableListener>();
				enableDisableListener.OnEnabled += delegate
				{
					if (Configuration.JSONConfig.DisableVRCPlusUserInfo)
					{
						VRCPlusSupporterButton.transform.localScale = Vector3.zero;
						VRCPlusEarlyAdopterIcon.transform.localScale = Vector3.zero;
						VRCPlusSubscriberIcon.transform.localScale = Vector3.zero;
					}
					else
					{
						VRCPlusSupporterButton.transform.localScale = Vector3.one;
						VRCPlusEarlyAdopterIcon.transform.localScale = Vector3.one;
						VRCPlusSubscriberIcon.transform.localScale = Vector3.one;
					}
				};
				enableDisableListener.OnDisabled += delegate
				{
					GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale = Vector3.one;
					UserNotes.SetActive(value: false);
					TeleportButton.SetActive(value: false);
					AvatarSearchButton.SetActive(value: false);
					PortalToUserButton.SetActive(value: false);
				};
			}

			public static IEnumerator PortalCooldown()
			{
				while (PortalCooldownTimer > 0)
				{
					yield return new WaitForSeconds(1f);
					PortalCooldownTimer--;
				}
			}

			public override void OnSceneWasLoaded(int buildIndex, string sceneName)
			{
				if (buildIndex == -1)
				{
					MelonCoroutines.Start(RoomEnter());
				}
			}

			private static IEnumerator RoomEnter()
			{
				while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
				{
					yield return new WaitForEndOfFrame();
				}
				if (VRCPlusSupporterButton == null)
				{
					VRCPlusSupporterButton = GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn/Supporter/SupporterButton");
					VRCPlusEarlyAdopterIcon = GameObject.Find("MenuContent/Screens/UserInfo/User Panel/VRCIcons/VRCPlusEarlyAdopterIcon");
					VRCPlusSubscriberIcon = GameObject.Find("MenuContent/Screens/UserInfo/User Panel/VRCIcons/VRCPlusSubscriberIcon");
				}
			}
		}
	}
