using System;
using emmVRC.Functions.Other;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class LoadingScreen : MelonLoaderEvents
	{
		public static GameObject functionsButton;

		public override void OnUiManagerInit()
		{
			functionsButton = UnityEngine.Object.Instantiate(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/ButtonMiddle"), QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup")).gameObject;
			functionsButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(0f, -128f);
			functionsButton.SetActive(Configuration.JSONConfig.ForceRestartButtonEnabled);
			functionsButton.name = "LoadingFunctionsButton";
			functionsButton.GetComponentInChildren<Text>().text = "Force Restart";
			functionsButton.GetComponent<Button>().onClick = new Button.ButtonClickedEvent();
			functionsButton.GetComponent<Button>().onClick.AddListener(DelegateSupport.ConvertDelegate<UnityAction>((Delegate)(Action)delegate
			{
				DestructiveActions.ForceRestart();
			}));
		}
	}
}
