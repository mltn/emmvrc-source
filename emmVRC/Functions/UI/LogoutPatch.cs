using System;
using emmVRC.Libraries;
using emmVRC.Network;
using emmVRC.Objects.ModuleBases;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class LogoutPatch : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/Footer/Logout").GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				NetworkClient.Logout();
			});
		}
	}
}
