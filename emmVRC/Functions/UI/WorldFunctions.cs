using System;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using UnityEngine.UI;
using VRC.UI;

namespace emmVRC.Functions.UI
{
	public class WorldFunctions : MelonLoaderEvents
	{
		private static GameObject WorldNotesButton;

		private static GameObject ViewFullDescButton;

		public override void OnUiManagerInit()
		{
			WorldNotesButton = UnityEngine.Object.Instantiate(GameObject.Find("MenuContent/Screens/WorldInfo/ReportButton"), GameObject.Find("MenuContent/Screens/WorldInfo").transform);
			WorldNotesButton.GetComponentInChildren<Text>().text = "Notes";
			WorldNotesButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0f, 130f);
			WorldNotesButton.SetActive(value: true);
			WorldNotesButton.GetComponentInChildren<Button>().onClick = new Button.ButtonClickedEvent();
			WorldNotesButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
			{
				WorldNotes.LoadNote(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>()
					.field_Private_ApiWorld_0.id, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>()
					.field_Private_ApiWorld_0.name);
			});
			ViewFullDescButton = UnityEngine.Object.Instantiate(GameObject.Find("MenuContent/Screens/WorldInfo/ReportButton"), GameObject.Find("MenuContent/Screens/WorldInfo").transform);
			ViewFullDescButton.transform.Find("Text").GetComponent<RectTransform>().offsetMax = new Vector2(-70.45f, 10f);
			ViewFullDescButton.transform.Find("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
			ViewFullDescButton.GetComponentInChildren<Text>().text = "View Full";
			ViewFullDescButton.GetComponentInChildren<Text>().resizeTextForBestFit = true;
			ViewFullDescButton.GetComponent<RectTransform>().offsetMax = new Vector2(-400f, 100f);
			ViewFullDescButton.GetComponent<RectTransform>().offsetMin = new Vector2(-650f, 35f);
			ViewFullDescButton.SetActive(value: true);
			ViewFullDescButton.GetComponentInChildren<Button>().onClick = new Button.ButtonClickedEvent();
			ViewFullDescButton.GetComponentInChildren<Button>().onClick.AddListener((Action)delegate
			{
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("Description for " + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>()
					.field_Private_ApiWorld_0.name, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>()
					.field_Private_ApiWorld_0.description, "Close", delegate
				{
					VRCUiPopupManager.prop_VRCUiPopupManager_0.HideCurrentPopup();
				});
			});
		}
	}
}
