using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using emmVRC.Components;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Network.Objects;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using MelonLoader;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using VRC.Core;
using VRC.UI;

namespace emmVRC.Functions.UI
{
	public class CustomAvatarFavorites : MelonLoaderEvents, IWithUpdate
	{
		public enum SortingMode
		{
			DateAdded,
			Alphabetical,
			Creator
		}

		internal static GameObject PublicAvatarList;

		internal static UiAvatarList NewAvatarList;

		private static GameObject avText;

		private static Text avTextText;

		private static GameObject ChangeButton;

		public static Button.ButtonClickedEvent baseChooseEvent;

		private static GameObject FavoriteButton;

		private static GameObject FavoriteButtonNew;

		private static Button FavoriteButtonNewButton;

		private static Text FavoriteButtonNewText;

		public static GameObject pageAvatar;

		private static PageAvatar currPageAvatar;

		private static bool error;

		private static bool errorWarned;

		private static bool Searching;

		public static List<ApiAvatar> LoadedAvatars;

		private static List<ApiAvatar> SearchedAvatars;

		private static UiInputField searchBox;

		private static UnityAction<string> searchBoxAction;

		private static GameObject refreshButton;

		private static GameObject backButton;

		private static GameObject forwardButton;

		private static GameObject pageTicker;

		private static GameObject sortButton;

		private static bool waitingForSearch;

		public static int currentPage;

		private static SortingMode currentSortingMode;

		private static bool sortingInverse;

		public override void OnUiManagerInit()
		{
			if (Configuration.JSONConfig.SortingMode <= 2)
			{
				currentSortingMode = (SortingMode)Configuration.JSONConfig.SortingMode;
			}
			else
			{
				currentSortingMode = SortingMode.DateAdded;
			}
			sortingInverse = Configuration.JSONConfig.SortingInverse;
			pageAvatar = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar").gameObject;
			FavoriteButton = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Favorite Button").gameObject;
			FavoriteButtonNew = UnityEngine.Object.Instantiate(FavoriteButton, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/"));
			FavoriteButtonNewButton = FavoriteButtonNew.GetComponent<Button>();
			FavoriteButtonNewButton.onClick.RemoveAllListeners();
			FavoriteButtonNewButton.onClick.AddListener((Action)delegate
			{
				ApiAvatar apiAvatar = pageAvatar.GetComponent<PageAvatar>().field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0;
				bool flag = false;
				Enumerator<ApiAvatar> enumerator2 = LoadedAvatars.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					if (enumerator2.get_Current().id == apiAvatar.id)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					if (!global::emmVRC.Utils.PlayerUtils.DoesUserHaveVRCPlus())
					{
						VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowAlert("VRChat Plus Required", "VRChat, like emmVRC, relies on the support of their users to keep the platform free. Please support VRChat to unlock these features.", 0f);
					}
					else if ((apiAvatar.releaseStatus == "public" || apiAvatar.authorId == APIUser.CurrentUser.id) && apiAvatar.releaseStatus != null)
					{
						FavoriteAvatar(apiAvatar).NoAwait("FavoriteAvatar");
					}
					else
					{
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot favorite this avatar (it is private!)", "Dismiss", delegate
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
						});
					}
				}
				else
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Are you sure you want to unfavorite the avatar \"" + apiAvatar.name + "\"?", "Yes", delegate
					{
						UnfavoriteAvatar(apiAvatar).NoAwait("UnfavoriteAvatar");
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
					}, "No", delegate
					{
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
					});
				}
			});
			FavoriteButtonNew.GetComponentInChildren<RectTransform>().localPosition += new Vector3(0f, 165f);
			FavoriteButtonNewText = FavoriteButtonNew.GetComponentInChildren<Text>();
			FavoriteButtonNewText.supportRichText = true;
			FavoriteButtonNew.transform.Find("Horizontal").GetComponentsInChildren<Transform>(includeInactive: true).ToList()
				.ForEach(delegate(Transform a)
				{
					if (a.name != "FavoriteActionText" && a.name != "Horizontal")
					{
						a.gameObject.SetActive(value: false);
					}
				});
			GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Vertical Scroll View/Viewport/Content/Legacy Avatar List").gameObject;
			PublicAvatarList = UnityEngine.Object.Instantiate(gameObject, gameObject.transform.parent);
			PublicAvatarList.transform.SetAsFirstSibling();
			ChangeButton = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Change Button").gameObject;
			baseChooseEvent = ChangeButton.GetComponent<Button>().onClick;
			ChangeButton.GetComponent<Button>().onClick = new Button.ButtonClickedEvent();
			ChangeButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				ApiAvatar selectedAvatar = pageAvatar.GetComponent<PageAvatar>().field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0;
				if (LoadedAvatars.ToArray().Any((ApiAvatar a) => a.id == selectedAvatar.id) || SearchedAvatars.ToArray().Any((ApiAvatar a) => a.id == selectedAvatar.id))
				{
					if (selectedAvatar.releaseStatus == "private" && selectedAvatar.authorId != APIUser.CurrentUser.id)
					{
						if (LoadedAvatars.ToArray().Any((ApiAvatar a) => a.id == selectedAvatar.id))
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).\nDo you want to unfavorite it?", "Yes", delegate
							{
								UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							}, "No", delegate
							{
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							});
						}
						else
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).", "Dismiss", delegate
							{
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							});
						}
					}
					else if (selectedAvatar.releaseStatus == "unavailable")
					{
						if (LoadedAvatars.ToArray().Any((ApiAvatar a) => a.id == selectedAvatar.id))
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).\nDo you want to unfavorite it?", "Yes", delegate
							{
								UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							}, "No", delegate
							{
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							});
						}
						else
						{
							VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).", "Dismiss", delegate
							{
								VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
							});
						}
					}
					else
					{
						baseChooseEvent.Invoke();
					}
				}
				else
				{
					baseChooseEvent.Invoke();
				}
			});
			avText = PublicAvatarList.transform.Find("Button").gameObject;
			avTextText = avText.GetComponentInChildren<Text>();
			avTextText.text = "(0) emmVRC Favorites";
			currPageAvatar = pageAvatar.GetComponent<PageAvatar>();
			NewAvatarList = PublicAvatarList.GetComponent<UiAvatarList>();
			NewAvatarList.clearUnseenListOnCollapse = false;
			NewAvatarList.field_Public_Category_0 = UiAvatarList.Category.SpecificList;
			currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Public_Single_0 *= 0.85f;
			refreshButton = UnityEngine.Object.Instantiate(ChangeButton, avText.transform.parent);
			refreshButton.GetComponentInChildren<Text>().text = "↻";
			refreshButton.GetComponent<Button>().onClick.RemoveAllListeners();
			refreshButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				Searching = false;
				MelonCoroutines.Start(JumpToStart());
				MelonCoroutines.Start(RefreshMenu(0.5f));
			});
			refreshButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
			refreshButton.transform.SetParent(avText.transform, worldPositionStays: true);
			refreshButton.GetComponent<RectTransform>().anchoredPosition = avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(980f, 0f);
			backButton = UnityEngine.Object.Instantiate(ChangeButton, avText.transform.parent);
			backButton.GetComponentInChildren<Text>().text = "←";
			backButton.GetComponent<Button>().onClick.RemoveAllListeners();
			backButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				currentPage--;
				MelonCoroutines.Start(JumpToStart());
				MelonCoroutines.Start(RefreshMenu(0.5f));
			});
			backButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
			backButton.transform.SetParent(avText.transform, worldPositionStays: true);
			backButton.GetComponent<RectTransform>().anchoredPosition = avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(750f, 0f);
			forwardButton = UnityEngine.Object.Instantiate(ChangeButton, avText.transform.parent);
			forwardButton.GetComponentInChildren<Text>().text = "→";
			forwardButton.GetComponent<Button>().onClick.RemoveAllListeners();
			forwardButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				currentPage++;
				MelonCoroutines.Start(JumpToStart());
				MelonCoroutines.Start(RefreshMenu(0.5f));
			});
			forwardButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
			forwardButton.transform.SetParent(avText.transform, worldPositionStays: true);
			forwardButton.GetComponent<RectTransform>().anchoredPosition = avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(900f, 0f);
			pageTicker = UnityEngine.Object.Instantiate(ChangeButton, avText.transform.parent);
			pageTicker.GetComponentInChildren<Text>().text = "0 / 0";
			UnityEngine.Object.Destroy(pageTicker.GetComponent<Button>());
			UnityEngine.Object.Destroy(pageTicker.GetComponent<Image>());
			pageTicker.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
			pageTicker.transform.SetParent(avText.transform, worldPositionStays: true);
			pageTicker.GetComponent<RectTransform>().anchoredPosition = avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(825f, 0f);
			sortButton = UnityEngine.Object.Instantiate(ChangeButton, avText.transform.parent);
			switch (currentSortingMode)
			{
			case SortingMode.DateAdded:
				sortButton.GetComponentInChildren<Text>().text = "Date " + (sortingInverse ? "↑" : "↓");
				break;
			case SortingMode.Alphabetical:
				sortButton.GetComponentInChildren<Text>().text = "ABC " + (sortingInverse ? "↑" : "↓");
				break;
			case SortingMode.Creator:
				sortButton.GetComponentInChildren<Text>().text = "Creator " + (sortingInverse ? "↑" : "↓");
				break;
			}
			sortButton.GetComponent<Button>().onClick.RemoveAllListeners();
			sortButton.GetComponent<Button>().onClick.AddListener((Action)delegate
			{
				if (!sortingInverse)
				{
					sortingInverse = true;
				}
				else
				{
					if (currentSortingMode != SortingMode.Creator)
					{
						currentSortingMode++;
					}
					else
					{
						currentSortingMode = SortingMode.DateAdded;
					}
					sortingInverse = false;
				}
				switch (currentSortingMode)
				{
				case SortingMode.DateAdded:
					sortButton.GetComponentInChildren<Text>().text = "Date " + (sortingInverse ? "↑" : "↓");
					break;
				case SortingMode.Alphabetical:
					sortButton.GetComponentInChildren<Text>().text = "ABC " + (sortingInverse ? "↑" : "↓");
					break;
				case SortingMode.Creator:
					sortButton.GetComponentInChildren<Text>().text = "Creator " + (sortingInverse ? "↑" : "↓");
					break;
				}
				currentPage = 0;
				Configuration.WriteConfigOption("SortingMode", (int)currentSortingMode);
				Configuration.WriteConfigOption("SortingInverse", sortingInverse);
				MelonCoroutines.Start(JumpToStart());
				MelonCoroutines.Start(RefreshMenu(0.5f));
			});
			sortButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(2f, 1f);
			sortButton.transform.SetParent(avText.transform, worldPositionStays: true);
			sortButton.GetComponent<RectTransform>().anchoredPosition = avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(635f, 0f);
			pageAvatar.transform.Find("AvatarPreviewBase").transform.localPosition += new Vector3(0f, 60f, 0f);
			pageAvatar.transform.Find("AvatarPreviewBase").transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
			foreach (PropertyInfo item in from a in typeof(PageAvatar).GetProperties()
				where a.PropertyType == typeof(Vector3) && ((Vector3)a.GetValue(currPageAvatar)).x <= -461f && (int)((Vector3)a.GetValue(currPageAvatar)).y == -200
				select a)
			{
				Vector3 vector = (Vector3)item.GetValue(currPageAvatar);
				item.SetValue(currPageAvatar, new Vector3(vector.x, vector.y + 80f, vector.z));
			}
			foreach (PropertyInfo item2 in from a in typeof(PageAvatar).GetProperties()
				where a.PropertyType == typeof(Vector3) && (int)((Vector3)a.GetValue(currPageAvatar)).x == -91
				select a)
			{
				Vector3 vector2 = (Vector3)item2.GetValue(currPageAvatar);
				item2.SetValue(currPageAvatar, new Vector3(vector2.x, vector2.y + 80f, vector2.z));
			}
			LoadedAvatars = new List<ApiAvatar>();
			SearchedAvatars = new List<ApiAvatar>();
			pageAvatar.AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				if ((!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || NetworkClient.webToken == null) && (PublicAvatarList.activeSelf || FavoriteButtonNew.activeSelf))
				{
					PublicAvatarList.SetActive(value: false);
					FavoriteButtonNew.SetActive(value: false);
				}
				else if ((!PublicAvatarList.activeSelf || !FavoriteButtonNew.activeSelf) && Configuration.JSONConfig.AvatarFavoritesEnabled && Configuration.JSONConfig.emmVRCNetworkEnabled && NetworkClient.webToken != null)
				{
					PublicAvatarList.SetActive(value: true);
					FavoriteButtonNew.SetActive(value: true);
					MelonCoroutines.Start(RefreshMenu(1f));
				}
				if (error && !errorWarned)
				{
					emmVRCNotificationsManager.AddNotification(new Notification("emmVRC Network", global::emmVRC.Functions.Core.Resources.errorSprite, "Your emmVRC avatars could not be loaded. Please ask in the Discord to resolve this.", canIgnore: true, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
					errorWarned = true;
				}
				MelonCoroutines.Start(WaitToEnableSearch());
			};
			VRCUiPageHeader componentInChildren = QuickMenuUtils.GetVRCUiMInstance().GetComponentInChildren<VRCUiPageHeader>(includeInactive: true);
			if (componentInChildren != null)
			{
				searchBox = componentInChildren.field_Public_UiInputField_0;
			}
			searchBoxAction = DelegateSupport.ConvertDelegate<UnityAction<string>>((Delegate)(Action<string>)delegate(string searchTerm)
			{
				if (!string.IsNullOrWhiteSpace(searchTerm) && searchTerm.Length >= 2)
				{
					SearchAvatars(searchTerm).NoAwait("SearchAvatars");
				}
			});
			NetworkClient.onLogin += delegate
			{
				PopulateList().NoAwait("PopulateList");
			};
			NetworkClient.onLogout += delegate
			{
				LoadedAvatars = new List<ApiAvatar>();
				SearchedAvatars = new List<ApiAvatar>();
			};
		}

		public static IEnumerator WaitToEnableSearch()
		{
			yield return new WaitForSeconds(0.1f);
			emmVRCLoader.Logger.LogDebug("Searchbox is " + ((searchBox == null) ? "null" : "not null"));
			emmVRCLoader.Logger.LogDebug("Searchbox button is " + ((searchBox.field_Public_Button_0 == null) ? "null" : "not null"));
			if (searchBox != null && searchBox.field_Public_Button_0 != null && !searchBox.field_Public_Button_0.interactable && !string.IsNullOrEmpty(NetworkClient.webToken) && Configuration.JSONConfig.AvatarFavoritesEnabled)
			{
				searchBox.field_Public_Button_0.interactable = true;
				searchBox.field_Public_UnityAction_1_String_0 = searchBoxAction;
			}
		}

		public static async Task FavoriteAvatar(ApiAvatar avtr)
		{
			if (LoadedAvatars.ToArray().ToList().FindIndex((ApiAvatar a) => a.id == avtr.id) == -1)
			{
				LoadedAvatars.Insert(0, avtr);
				global::emmVRC.Network.Objects.Avatar obj = new global::emmVRC.Network.Objects.Avatar(avtr);
				try
				{
					await HTTPRequest.post(NetworkClient.baseURL + "/api/avatar", obj);
					if (!Searching)
					{
						currentPage = 0;
						MelonCoroutines.Start(JumpToStart());
						MelonCoroutines.Start(RefreshMenu(0.1f));
					}
				}
				catch
				{
					await emmVRC.AwaitUpdate.Yield();
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Error occured while updating avatar list.", "Dismiss", delegate
					{
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
					});
					throw;
				}
			}
			else
			{
				emmVRCLoader.Logger.LogDebug("Tried to add an avatar that already exists...");
			}
		}

		public static async Task UnfavoriteAvatar(ApiAvatar avtr)
		{
			if (LoadedAvatars.Contains(avtr))
			{
				LoadedAvatars.Remove(avtr);
			}
			try
			{
				await HTTPRequest.delete(NetworkClient.baseURL + "/api/avatar", new global::emmVRC.Network.Objects.Avatar(avtr));
				if (!Searching)
				{
					await emmVRC.AwaitUpdate.Yield();
					MelonCoroutines.Start(JumpToStart());
					MelonCoroutines.Start(RefreshMenu(0.1f));
				}
			}
			catch
			{
				await emmVRC.AwaitUpdate.Yield();
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Error occured while updating avatar list.", "Dismiss", delegate
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
				});
				throw;
			}
		}

		public static async Task PopulateList()
		{
			LoadedAvatars = new List<ApiAvatar>();
			try
			{
				global::emmVRC.Network.Objects.Avatar[] avatarArray = Decoder.Decode(await HTTPRequest.get(NetworkClient.baseURL + "/api/avatar")).Make<global::emmVRC.Network.Objects.Avatar[]>();
				await emmVRC.AwaitUpdate.Yield();
				if (avatarArray != null)
				{
					global::emmVRC.Network.Objects.Avatar[] array = avatarArray;
					foreach (global::emmVRC.Network.Objects.Avatar avatar in array)
					{
						LoadedAvatars.Add(avatar.apiAvatar());
					}
					MelonCoroutines.Start(RefreshMenu(0.1f));
				}
			}
			catch
			{
				await emmVRC.AwaitUpdate.Yield();
				emmVRCNotificationsManager.AddNotification(new Notification("emmVRC Network", global::emmVRC.Functions.Core.Resources.errorSprite, "emmVRC Avatar Favorites list failed to load. Please check your internet connection.", canIgnore: true, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
				error = true;
				errorWarned = true;
				throw;
			}
		}

		public static IEnumerator RefreshMenu(float delay)
		{
			if (!(NewAvatarList.scrollRect != null))
			{
				yield break;
			}
			yield return new WaitForSeconds(delay);
			if (Searching)
			{
				if (currentPage > SearchedAvatars.get_Count() / Configuration.JSONConfig.SearchRenderLimit)
				{
					currentPage = SearchedAvatars.get_Count() / Configuration.JSONConfig.SearchRenderLimit;
				}
				if (currentPage < 0)
				{
					currentPage = 0;
				}
				List<ApiAvatar> val = new List<ApiAvatar>();
				switch (currentSortingMode)
				{
				case SortingMode.Alphabetical:
					foreach (ApiAvatar item in from x in SearchedAvatars.ToArray()
						orderby x.name
						select x)
					{
						val.Add(item);
					}
					break;
				case SortingMode.Creator:
					foreach (ApiAvatar item2 in from x in SearchedAvatars.ToArray()
						orderby x.authorName
						select x)
					{
						val.Add(item2);
					}
					break;
				case SortingMode.DateAdded:
				{
					Enumerator<ApiAvatar> enumerator = SearchedAvatars.GetEnumerator();
					while (enumerator.MoveNext())
					{
						ApiAvatar current = enumerator.get_Current();
						val.Add(current);
					}
					break;
				}
				}
				if (sortingInverse)
				{
					val.Reverse();
				}
				pageTicker.GetComponentInChildren<Text>().text = currentPage + 1 + " / " + (val.get_Count() / Configuration.JSONConfig.SearchRenderLimit + 1);
				List<ApiAvatar> range = val.GetRange(currentPage * Configuration.JSONConfig.SearchRenderLimit, Math.Abs(currentPage * Configuration.JSONConfig.SearchRenderLimit - val.get_Count()));
				if (range.get_Count() > Configuration.JSONConfig.SearchRenderLimit)
				{
					range.RemoveRange(Configuration.JSONConfig.SearchRenderLimit, range.get_Count() - Configuration.JSONConfig.SearchRenderLimit);
				}
				NewAvatarList.RenderElement(new List<ApiAvatar>());
				NewAvatarList.RenderElement(range);
				avText.GetComponentInChildren<Text>().text = "(" + val.get_Count() + ") Search Results";
				if (currentPage == 0)
				{
					backButton.GetComponent<Button>().interactable = false;
				}
				else
				{
					backButton.GetComponent<Button>().interactable = true;
				}
				if (currentPage >= val.get_Count() / Configuration.JSONConfig.SearchRenderLimit)
				{
					forwardButton.GetComponent<Button>().interactable = false;
				}
				else
				{
					forwardButton.GetComponent<Button>().interactable = true;
				}
				yield break;
			}
			if (currentPage > LoadedAvatars.get_Count() / Configuration.JSONConfig.FavoriteRenderLimit)
			{
				currentPage = LoadedAvatars.get_Count() / Configuration.JSONConfig.FavoriteRenderLimit;
			}
			if (currentPage < 0)
			{
				currentPage = 0;
			}
			List<ApiAvatar> val2 = new List<ApiAvatar>();
			switch (currentSortingMode)
			{
			case SortingMode.Alphabetical:
				foreach (ApiAvatar item3 in from x in LoadedAvatars.ToArray()
					orderby x.name
					select x)
				{
					val2.Add(item3);
				}
				break;
			case SortingMode.Creator:
				foreach (ApiAvatar item4 in from x in LoadedAvatars.ToArray()
					orderby x.authorName
					select x)
				{
					val2.Add(item4);
				}
				break;
			case SortingMode.DateAdded:
			{
				Enumerator<ApiAvatar> enumerator = LoadedAvatars.GetEnumerator();
				while (enumerator.MoveNext())
				{
					ApiAvatar current4 = enumerator.get_Current();
					val2.Add(current4);
				}
				break;
			}
			}
			if (sortingInverse)
			{
				val2.Reverse();
			}
			pageTicker.GetComponentInChildren<Text>().text = currentPage + 1 + " / " + (val2.get_Count() / Configuration.JSONConfig.FavoriteRenderLimit + 1);
			List<ApiAvatar> range2 = val2.GetRange(currentPage * Configuration.JSONConfig.FavoriteRenderLimit, Math.Abs(currentPage * Configuration.JSONConfig.FavoriteRenderLimit - val2.get_Count()));
			if (range2.get_Count() > Configuration.JSONConfig.FavoriteRenderLimit)
			{
				range2.RemoveRange(Configuration.JSONConfig.FavoriteRenderLimit, range2.get_Count() - Configuration.JSONConfig.FavoriteRenderLimit);
			}
			NewAvatarList.RenderElement(new List<ApiAvatar>());
			NewAvatarList.RenderElement(range2);
			avText.GetComponentInChildren<Text>().text = "(" + val2.get_Count() + ") emmVRC Favorites";
			if (currentPage == 0)
			{
				backButton.GetComponent<Button>().interactable = false;
			}
			else
			{
				backButton.GetComponent<Button>().interactable = true;
			}
			if (currentPage >= val2.get_Count() / Configuration.JSONConfig.FavoriteRenderLimit)
			{
				forwardButton.GetComponent<Button>().interactable = false;
			}
			else
			{
				forwardButton.GetComponent<Button>().interactable = true;
			}
		}

		public static IEnumerator JumpToStart()
		{
			if (Configuration.JSONConfig.AvatarFavoritesJumpToStart)
			{
				while (NewAvatarList.scrollRect.normalizedPosition.x > 0f)
				{
					NewAvatarList.scrollRect.normalizedPosition = new Vector2(NewAvatarList.scrollRect.normalizedPosition.x - 0.1f, 0f);
					yield return new WaitForEndOfFrame();
				}
			}
		}

		public static IEnumerator SearchAvatarsAfterDelay(string query)
		{
			yield return new WaitForSecondsRealtime(1f);
			if (Configuration.JSONConfig.AvatarFavoritesJumpToStart)
			{
				while (NewAvatarList.scrollRect.normalizedPosition.x > 0f)
				{
					NewAvatarList.scrollRect.normalizedPosition = new Vector2(NewAvatarList.scrollRect.normalizedPosition.x - 0.1f, 0f);
					yield return new WaitForEndOfFrame();
				}
			}
			SearchAvatars(query).NoAwait("SearchAvatars");
		}

		public static async Task SearchAvatars(string query)
		{
			if (!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || NetworkClient.webToken == null)
			{
				await emmVRC.AwaitUpdate.Yield();
			}
			if (waitingForSearch)
			{
				VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Please wait for your current search\nto finish before starting a new one.", "Okay", delegate
				{
					VRCUiPopupManager.prop_VRCUiPopupManager_0.HideCurrentPopup();
				});
				return;
			}
			avText.GetComponentInChildren<Text>().text = "Searching. Please wait...";
			emmVRCLoader.Logger.LogDebug("Clearing current search avatars...");
			SearchedAvatars.Clear();
			waitingForSearch = true;
			try
			{
				string text = await HTTPRequest.post(NetworkClient.baseURL + "/api/avatar/search", new Dictionary<string, string> { ["query"] = query });
				if (text.Contains("Bad Request") || text.Contains("Too Many Requests"))
				{
					await emmVRC.AwaitUpdate.Yield();
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Your search could not be processed.", "Dismiss", delegate
					{
						VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
					});
					return;
				}
				global::emmVRC.Network.Objects.Avatar[] avatarArray = Decoder.Decode(text).Make<global::emmVRC.Network.Objects.Avatar[]>();
				await emmVRC.AwaitUpdate.Yield();
				if (avatarArray != null)
				{
					global::emmVRC.Network.Objects.Avatar[] array = avatarArray;
					foreach (global::emmVRC.Network.Objects.Avatar avatar in array)
					{
						SearchedAvatars.Add(avatar.apiAvatar());
					}
				}
				currentPage = 0;
				Searching = true;
				MelonCoroutines.Start(RefreshMenu(0.1f));
			}
			catch
			{
				await emmVRC.AwaitUpdate.Yield();
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Your search could not be processed.", "Dismiss", delegate
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
				});
				throw;
			}
			finally
			{
				waitingForSearch = false;
				await emmVRC.AwaitUpdate.Yield();
				if (NewAvatarList.expandButton.gameObject.transform.Find("ToggleIcon").GetComponentInChildren<Image>().sprite == NewAvatarList.expandSprite)
				{
					NewAvatarList.ToggleExtend();
				}
			}
		}

		public void OnUpdate()
		{
			if (PublicAvatarList == null || !PublicAvatarList.activeInHierarchy || !Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || NetworkClient.webToken == null)
			{
				return;
			}
			NewAvatarList.collapsedCount = Configuration.JSONConfig.FavoriteRenderLimit + Configuration.JSONConfig.SearchRenderLimit;
			NewAvatarList.expandedCount = Configuration.JSONConfig.FavoriteRenderLimit + Configuration.JSONConfig.SearchRenderLimit;
			if (!(currPageAvatar != null) || !(currPageAvatar.field_Public_SimpleAvatarPedestal_0 != null) || currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0 == null || LoadedAvatars == null || !(FavoriteButtonNew != null))
			{
				return;
			}
			FavoriteButtonNewText.text = "<color=#FF69B4>emmVRC</color> Favorite";
			Enumerator<ApiAvatar> enumerator = LoadedAvatars.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.get_Current().id == currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0.id)
				{
					FavoriteButtonNewText.text = "<color=#FF69B4>emmVRC</color> Unfavorite";
					break;
				}
			}
		}

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1)
			{
				CheckForAvatarPedestals().NoAwait();
			}
		}

		public static async Task CheckForAvatarPedestals()
		{
			if (NetworkClient.webToken == null || APIUser.CurrentUser == null || !Configuration.JSONConfig.SubmitAvatarPedestals || !NetworkConfig.Instance.AvatarPedestalScansAllowed)
			{
				return;
			}
			while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
			{
				await emmVRC.AwaitUpdate.Yield();
			}
			if (!RoomManager.field_Internal_Static_ApiWorld_0.IsPublicPublishedWorld)
			{
				return;
			}
			foreach (AvatarPedestal pedestal in UnityEngine.Resources.FindObjectsOfTypeAll<AvatarPedestal>())
			{
				if (pedestal != null && pedestal.field_Private_ApiAvatar_0 != null && pedestal.field_Private_ApiAvatar_0.releaseStatus == "public")
				{
					await emmVRC.AwaitUpdate.Yield();
					global::emmVRC.Network.Objects.Avatar obj = new global::emmVRC.Network.Objects.Avatar(pedestal.field_Private_ApiAvatar_0);
					emmVRCLoader.Logger.LogDebug("Found pedestal " + pedestal.field_Private_ApiAvatar_0.name + ", putting...");
					try
					{
						await HTTPRequest.put(NetworkClient.baseURL + "/api/avatar", obj);
					}
					catch
					{
						await emmVRC.AwaitUpdate.Yield();
						emmVRCLoader.Logger.LogDebug("Could not put avatar");
						throw;
					}
					await Task.Delay(500);
				}
			}
		}

		public static void ExportAvatars()
		{
			List<ExportedAvatar> list = new List<ExportedAvatar>();
			Enumerator<ApiAvatar> enumerator = LoadedAvatars.GetEnumerator();
			while (enumerator.MoveNext())
			{
				ApiAvatar current = enumerator.get_Current();
				list.Add(new ExportedAvatar
				{
					avatar_id = current.id,
					avatar_name = current.name
				});
			}
			File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/ExportedList.json"), Encoder.Encode(list, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
		}
	}
}
