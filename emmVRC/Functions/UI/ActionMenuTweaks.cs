using System;
using System.Collections.Generic;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;

namespace emmVRC.Functions.UI
{
	public class ActionMenuTweaks : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("DisableOneHandMovement", Apply));
			Apply();
		}

		public static void Apply()
		{
			QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.parent.Find("ActionMenu/Container/MoveMenuL").gameObject.SetActive(!Configuration.JSONConfig.DisableOneHandMovement);
			QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.parent.Find("ActionMenu/Container/MoveMenuR").gameObject.SetActive(!Configuration.JSONConfig.DisableOneHandMovement);
		}
	}
}
