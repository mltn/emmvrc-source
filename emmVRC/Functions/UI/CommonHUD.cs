using System;
using emmVRC.Libraries;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using VRC;

namespace emmVRC.Functions.UI
{
	public class CommonHUD
	{
		public static string RenderPlayerList()
		{
			string text = "";
			if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
			{
				int num = 0;
				if (PlayerManager.field_Private_Static_PlayerManager_0 != null && PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0 != null)
				{
					try
					{
						Enumerator<Player> enumerator = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.GetEnumerator();
						while (enumerator.MoveNext())
						{
							Player current = enumerator.get_Current();
							if (current != null && current.field_Private_VRCPlayerApi_0 != null && num != 22)
							{
								text = text + (current.field_Private_VRCPlayerApi_0.isMaster ? "♕ " : "     ") + "<color=#" + ColorConversion.ColorToHex(VRCPlayer.Method_Public_Static_Color_APIUser_0(current.prop_APIUser_0)) + ">" + current.prop_APIUser_0.GetName() + "</color> - " + current._vrcplayer.prop_Int16_0 + " ms - <color=" + ColorConversion.ColorToHex(current.prop_PlayerNet_0.LerpFramerateColor(), hash: true) + ">" + ((current.prop_PlayerNet_0.GetFramerate() != -1f) ? (current.prop_PlayerNet_0.GetFramerate().ToString() ?? "") : "N/A") + "</color> fps\n";
								num++;
							}
						}
						return text;
					}
					catch (Exception)
					{
						new Exception();
						return text;
					}
				}
			}
			return text;
		}

		public static string RenderWorldInfo()
		{
			string text = "";
			if (RoomManager.field_Internal_Static_ApiWorld_0 != null && VRCPlayer.field_Internal_Static_VRCPlayer_0 != null)
			{
				text = text + "<b><color=red>X: " + Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.x * 10f) / 10f + "</color></b>  ";
				text = text + "<b><color=lime>Y: " + Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.y * 10f) / 10f + "</color></b>  ";
				text = text + "<b><color=cyan>Z: " + Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.z * 10f) / 10f + "</color></b>  ";
			}
			return text + "\n\n";
		}
	}
}
