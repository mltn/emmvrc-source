using System;
using System.IO;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.UI
{
	public class WorldNotes : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes")))
			{
				Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes"));
			}
		}

		public static void LoadNote(string worldID, string displayName)
		{
			try
			{
				WorldNote loadedNote;
				if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json")))
				{
					string text = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json"));
					if (text.Contains("emmVRC.Hacks"))
					{
						text = text.Replace("emmVRC.Hacks", "emmVRC.Functions.UI");
						File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json"), text);
					}
					loadedNote = Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json"))).Make<WorldNote>();
				}
				else
				{
					loadedNote = new WorldNote
					{
						worldID = worldID,
						NoteText = ""
					};
				}
				VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("Note for " + displayName, string.IsNullOrWhiteSpace(loadedNote.NoteText) ? "There is currently no note for this world." : loadedNote.NoteText, "Change Note", delegate
				{
					VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter a note for " + displayName + ":", loadedNote.NoteText, InputField.InputType.Standard, keypad: false, "Accept", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate(string newNoteText, List<KeyCode> keyk, Text tx)
					{
						SaveNote(new WorldNote
						{
							worldID = worldID,
							NoteText = newNoteText
						});
					}), null, "Enter note....");
				});
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError("Failed to load note: " + ex.ToString());
			}
		}

		public static void SaveNote(WorldNote note)
		{
			File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + note.worldID + ".json"), Encoder.Encode(note, EncodeOptions.PrettyPrint));
		}
	}
}
