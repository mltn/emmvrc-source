using System.Linq;
using emmVRC.Functions.ModCompatibility;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;
using UnityEngine;

namespace emmVRC.Functions.Core
{
	[Priority(0)]
	public class ModCompatibility : MelonLoaderEvents
	{
		public static bool MultiplayerDynamicBones;

		public static bool PortalConfirmation;

		public static bool MControl;

		public static bool OGTrustRank;

		public static bool UIExpansionKit;

		public static bool FBTSaver;

		public static bool IKTweaks;

		public static bool BetterLoadingScreen;

		public static bool VRCMinus;

		public static GameObject FlightButton;

		public static GameObject NoclipButton;

		public static GameObject SpeedButton;

		public static GameObject ESPButton;

		public override void OnUiManagerInit()
		{
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "MultiplayerDynamicBones" || i.Info.Name == "MultiplayerDynamicBonesMod"))
			{
				MultiplayerDynamicBones = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "Portal Confirmation"))
			{
				PortalConfirmation = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "MControl"))
			{
				MControl = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "OGTrustRanks"))
			{
				OGTrustRank = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "UI Expansion Kit"))
			{
				UIExpansionKit = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "FBT Saver"))
			{
				FBTSaver = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "IKTweaks"))
			{
				IKTweaks = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "BetterLoadingScreen"))
			{
				BetterLoadingScreen = true;
			}
			if (MelonHandler.Mods.Any((MelonMod i) => i.Info.Name == "VRC-Minus"))
			{
				VRCMinus = true;
			}
			if (MultiplayerDynamicBones)
			{
				emmVRCLoader.Logger.LogDebug("Detected MultiplayerDynamicBones");
			}
			if (PortalConfirmation)
			{
				emmVRCLoader.Logger.LogDebug("Detected PortalConfirmation");
			}
			if (MControl)
			{
				emmVRCLoader.Logger.LogDebug("Detected MControl");
			}
			if (OGTrustRank)
			{
				emmVRCLoader.Logger.LogDebug("Detected OGTrustRank");
			}
			if (UIExpansionKit)
			{
				emmVRCLoader.Logger.LogDebug("Detected UIExpansionKit");
				UIExpansionKitIntegration.Initialize();
			}
			if (FBTSaver)
			{
				emmVRCLoader.Logger.LogDebug("Detected FBTSaver");
			}
			if (IKTweaks)
			{
				emmVRCLoader.Logger.LogDebug("Detected IKTweaks");
			}
			if (BetterLoadingScreen)
			{
				emmVRCLoader.Logger.LogDebug("Detected BetterLoadingScreen");
			}
			if (VRCMinus)
			{
				emmVRCLoader.Logger.LogDebug("Detected VRCMinus");
			}
		}

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && MultiplayerDynamicBones && Configuration.JSONConfig.GlobalDynamicBonesEnabled)
			{
				Configuration.WriteConfigOption("GlobalDynamicBonesEnabled", false);
				emmVRCNotificationsManager.AddNotification(new Notification("emmVRC", null, "You are currently using MultiplayerDynamicBones. emmVRC's Global Dynamic Bones have been disabled, as only one can be used at a time.", canIgnore: true, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
			}
		}
	}
}
