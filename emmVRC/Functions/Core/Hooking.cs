using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using HarmonyLib;
using Il2CppSystem;
using MelonLoader;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using VRC;
using VRC.Core;

namespace emmVRC.Functions.Core
{
	public class Hooking : MelonLoaderEvents
	{
		private static Regex methodMatchRegex = new Regex("Method_Public_Void_\\d", RegexOptions.Compiled);

		public override void OnUiManagerInit()
		{
			try
			{
				if (!ModCompatibility.PortalConfirmation)
				{
					emmVRCLoaderMod.instance.HarmonyInstance.Patch(typeof(PortalInternal).GetMethods(BindingFlags.Instance | BindingFlags.Public).Single((MethodInfo it) => it != null && it.ReturnType == typeof(void) && it.GetParameters().Length == 0 && XrefScanner.XrefScan(it).Any(delegate(XrefInstance jt)
					{
						if (jt.Type == XrefType.Global)
						{
							Object obj = jt.ReadAsObject();
							return ((obj != null) ? obj.ToString() : null) == " was at capacity, cannot enter.";
						}
						return false;
					})), new HarmonyMethod(typeof(Hooking).GetMethod("OnPortalEntered", BindingFlags.Static | BindingFlags.NonPublic)));
				}
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError("Portal blocking failed: " + ex.ToString());
			}
			try
			{
				emmVRCLoaderMod.instance.HarmonyInstance.Patch(typeof(VRC_StationInternal).GetMethod("Method_Public_Boolean_Player_Boolean_0"), new HarmonyMethod(typeof(Hooking).GetMethod("PlayerCanUseStation", BindingFlags.Static | BindingFlags.NonPublic)));
			}
			catch (Exception ex2)
			{
				emmVRCLoader.Logger.LogError("Station patching failed: " + ex2.ToString());
			}
			if (!ModCompatibility.FBTSaver && !ModCompatibility.IKTweaks && !Environment.CurrentDirectory.Contains("vrchat-vrchat"))
			{
				try
				{
					MethodInfo[] methods = Assembly.GetAssembly(typeof(QuickMenuContextualDisplay)).GetType("VRCTrackingSteam", throwOnError: true, ignoreCase: true).GetMethods();
					foreach (MethodInfo methodInfo in methods)
					{
						if (methodInfo.GetParameters().Length == 1 && methodInfo.GetParameters().First().ParameterType == typeof(string) && methodInfo.ReturnType == typeof(bool) && methodInfo.GetRuntimeBaseDefinition() == methodInfo)
						{
							emmVRCLoaderMod.instance.HarmonyInstance.Patch(methodInfo, new HarmonyMethod(typeof(Hooking).GetMethod("IsCalibratedForAvatar", BindingFlags.Static | BindingFlags.NonPublic)));
						}
					}
					methods = Assembly.GetAssembly(typeof(QuickMenuContextualDisplay)).GetType("VRCTrackingSteam", throwOnError: true, ignoreCase: true).GetMethods();
					foreach (MethodInfo methodInfo2 in methods)
					{
						if (methodInfo2.GetParameters().Length == 3 && methodInfo2.GetParameters().First().ParameterType == typeof(Animator) && methodInfo2.ReturnType == typeof(void) && methodInfo2.GetRuntimeBaseDefinition() == methodInfo2)
						{
							emmVRCLoaderMod.instance.HarmonyInstance.Patch(methodInfo2, new HarmonyMethod(typeof(Hooking).GetMethod("PerformCalibration", BindingFlags.Static | BindingFlags.NonPublic)));
						}
					}
				}
				catch (Exception ex3)
				{
					emmVRCLoader.Logger.LogError("VRCTrackingSteam hooking failed: " + ex3.ToString());
				}
			}
			try
			{
				foreach (MethodInfo item in from x in typeof(PlayerNameplate).GetMethods(BindingFlags.Instance | BindingFlags.Public)
					where methodMatchRegex.IsMatch(x.Name)
					select x)
				{
					emmVRCLoader.Logger.LogDebug("Found target Rebuild method (" + item.Name + ")");
					emmVRCLoaderMod.instance.HarmonyInstance.Patch(item, null, new HarmonyMethod(typeof(Hooking).GetMethod("OnRebuild", BindingFlags.Static | BindingFlags.NonPublic)));
				}
			}
			catch (Exception ex4)
			{
				emmVRCLoader.Logger.LogError("Avatar OnRebuild Failed: " + ex4.ToString());
			}
		}

		private static bool PlayerCanUseStation(ref bool __result, Player __0, bool __1)
		{
			if (__0 != null && __0 == VRCPlayer.field_Internal_Static_VRCPlayer_0._player && Configuration.JSONConfig.ChairBlockingEnable)
			{
				__result = false;
				return false;
			}
			return true;
		}

		private static bool OnPortalEntered(PortalInternal __instance)
		{
			if (!Configuration.JSONConfig.PortalBlockingEnable)
			{
				return true;
			}
			return false;
		}

		private static void OnRebuild(PlayerNameplate __instance)
		{
			if (!(__instance.field_Private_VRCPlayer_0 == null) && __instance.field_Private_VRCPlayer_0._player != null && __instance.field_Private_VRCPlayer_0._player.prop_APIUser_0 != null && Configuration.JSONConfig.NameplateColorChangingEnabled && !ModCompatibility.OGTrustRank)
			{
				APIUser aPIUser = __instance.field_Private_VRCPlayer_0._player.prop_APIUser_0;
				if (aPIUser.isFriend)
				{
					__instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex);
				}
				else if (aPIUser.hasVeteranTrustLevel)
				{
					__instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex);
				}
				else if (aPIUser.hasTrustedTrustLevel)
				{
					__instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex);
				}
				else if (aPIUser.hasKnownTrustLevel)
				{
					__instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex);
				}
				else if (aPIUser.hasBasicTrustLevel)
				{
					__instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex);
				}
			}
		}

		private static bool IsCalibratedForAvatar(ref VRCTrackingSteam __instance, ref bool __result, string __0)
		{
			if (__0 != null && FBTSaving.IsPreviouslyCalibrated(__0) && RoomManager.field_Internal_Static_ApiWorld_0 != null && Configuration.JSONConfig.TrackingSaving)
			{
				emmVRCLoader.Logger.LogDebug("Avatar was previously calibrated, loading calibration data");
				__result = true;
				FBTSaving.LoadCalibrationInfo(__instance, __0);
				return false;
			}
			emmVRCLoader.Logger.LogDebug("Avatar was not previously calibrated, or tracking saving is off");
			__result = false;
			return true;
		}

		private static bool PerformCalibration(ref VRCTrackingSteam __instance, Animator __0, bool __1, bool __2)
		{
			if (__0 == null || __instance == null)
			{
				return true;
			}
			if (Configuration.JSONConfig.TrackingSaving)
			{
				emmVRCLoader.Logger.LogDebug("Saving calibration info...");
				if (VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0 != null)
				{
					MelonCoroutines.Start(FBTSaving.SaveCalibrationInfo(__instance, VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0.id));
				}
				else if (VRCPlayer.field_Internal_Static_VRCPlayer_0.field_Private_VRCAvatarManager_0.field_Private_ApiAvatar_1 != null)
				{
					MelonCoroutines.Start(FBTSaving.SaveCalibrationInfo(__instance, VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1.id));
				}
				else
				{
					emmVRCLoader.Logger.LogError("Could not fetch avatar information for this avatar");
				}
			}
			return true;
		}

		private static bool SetControllerVisibility(VRCTrackingManager __instance, bool __0)
		{
			return true;
		}
	}
}
