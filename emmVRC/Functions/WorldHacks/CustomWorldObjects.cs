using emmVRC.Components;
using emmVRC.Objects.ModuleBases;
using UnityEngine;

namespace emmVRC.Functions.WorldHacks
{
	public class CustomWorldObjects : MelonLoaderEvents
	{
		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex != -1)
			{
				return;
			}
			GameObject[] array = Resources.FindObjectsOfTypeAll<GameObject>();
			foreach (GameObject gameObject in array)
			{
				if (gameObject.name == "eVRCDisable")
				{
					gameObject.SetActive(value: false);
				}
				else if (gameObject.name == "eVRCEnable")
				{
					gameObject.SetActive(value: true);
				}
				else if (gameObject.name == "eVRCPanel")
				{
					gameObject.AddComponent<emmVRCPanel>();
				}
			}
		}
	}
}
