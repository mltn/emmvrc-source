using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRCLoader;
using MelonLoader;
using UnityEngine;

namespace emmVRC.Functions.WorldHacks
{
	public class InstanceHistory : MelonLoaderEvents
	{
		internal static List<SerializedWorld> previousInstances;

		public override void OnUiManagerInit()
		{
			if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm")))
			{
				previousInstances = new List<SerializedWorld>();
				File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"), Encoding.UTF8.GetBytes(global::emmVRC.TinyJSON.Encoder.Encode(previousInstances, EncodeOptions.PrettyPrint)));
				return;
			}
			string @string = Encoding.UTF8.GetString(File.ReadAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm")));
			try
			{
				previousInstances = global::emmVRC.TinyJSON.Decoder.Decode(@string).Make<List<SerializedWorld>>();
				previousInstances.RemoveAll((SerializedWorld a) => UnixTime.ToDateTime(a.loggedDateTime) < DateTime.Now.AddDays(-3.0));
				SaveInstances();
			}
			catch (Exception)
			{
				new Exception();
				emmVRCLoader.Logger.LogError("Your instance history file is invalid. It will be wiped.");
				File.Delete(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"));
				previousInstances = new List<SerializedWorld>();
			}
		}

		public static void SaveInstances()
		{
			File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"), Encoding.UTF8.GetBytes(global::emmVRC.TinyJSON.Encoder.Encode(previousInstances, EncodeOptions.PrettyPrint)));
		}

		public static void ClearInstances()
		{
			previousInstances.Clear();
			File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"), Encoding.UTF8.GetBytes(global::emmVRC.TinyJSON.Encoder.Encode(previousInstances, EncodeOptions.PrettyPrint)));
		}

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1)
			{
				MelonCoroutines.Start(EnteredWorld());
			}
		}

		public static IEnumerator EnteredWorld()
		{
			while (RoomManager.field_Internal_Static_ApiWorld_0 == null || RoomManager.field_Internal_Static_ApiWorldInstance_0 == null)
			{
				yield return new WaitForEndOfFrame();
			}
			try
			{
				SerializedWorld serializedWorld = new SerializedWorld
				{
					WorldID = RoomManager.field_Internal_Static_ApiWorld_0.id,
					WorldTags = RoomManager.field_Internal_Static_ApiWorldInstance_0.instanceId,
					WorldOwner = ((RoomManager.field_Internal_Static_ApiWorldInstance_0.ownerId == null) ? "" : RoomManager.field_Internal_Static_ApiWorldInstance_0.ownerId),
					WorldType = RoomManager.field_Internal_Static_ApiWorldInstance_0.type.ToString(),
					WorldName = RoomManager.field_Internal_Static_ApiWorld_0.name,
					WorldImageURL = RoomManager.field_Internal_Static_ApiWorld_0.thumbnailImageUrl
				};
				SerializedWorld serializedWorld2 = null;
				foreach (SerializedWorld previousInstance in previousInstances)
				{
					if (previousInstance.WorldID == serializedWorld.WorldID && InstanceIDUtilities.GetInstanceID(previousInstance.WorldTags) == InstanceIDUtilities.GetInstanceID(serializedWorld.WorldTags))
					{
						serializedWorld2 = previousInstance;
					}
				}
				if (serializedWorld2 != null)
				{
					previousInstances.Remove(serializedWorld2);
				}
				previousInstances.Add(serializedWorld);
				SaveInstances();
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError(ex.ToString());
			}
		}

		public static string PrettifyInstanceType(string original)
		{
			return original switch
			{
				"FriendsOfGuests" => "Friends+", 
				"FriendsOnly" => "Friends", 
				"InvitePlus" => "Invite+", 
				"InviteOnly" => "Invite", 
				_ => original, 
			};
		}
	}
}
