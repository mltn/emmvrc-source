using System;
using System.Collections.Generic;
using emmVRC.Components;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using VRC.SDK3.Components;
using VRC.SDKBase;

namespace emmVRC.Functions.WorldHacks
{
	public class Flashlight : MelonLoaderEvents
	{
		private static GameObject flashlightParent;

		private static Material flashlightParentMaterial;

		private static Light flashlight;

		private static Light headlight;

		public static bool IsHeadlightEnabled { get; private set; }

		public static bool IsFlashlightEnabled { get; private set; }

		public override void OnApplicationStart()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FlashlightColorHex", delegate
			{
				if (!(flashlight == null))
				{
					flashlight.color = ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex);
					headlight.color = ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex);
				}
			}));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FlashlightRange", delegate
			{
				if (!(flashlight == null))
				{
					flashlight.range = Configuration.JSONConfig.FlashlightRange;
					headlight.range = Configuration.JSONConfig.FlashlightRange;
				}
			}));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FlashlightPower", delegate
			{
				if (!(flashlight == null))
				{
					flashlight.intensity = Configuration.JSONConfig.FlashlightPower;
					headlight.intensity = Configuration.JSONConfig.FlashlightPower;
				}
			}));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("FlashlightAngle", delegate
			{
				if (!(flashlight == null))
				{
					flashlight.spotAngle = Configuration.JSONConfig.FlashlightAngle;
					headlight.spotAngle = Configuration.JSONConfig.FlashlightAngle;
				}
			}));
		}

		public override void OnUiManagerInit()
		{
			GameObject gameObject = new GameObject("emmVRCHeadlight");
			gameObject.transform.SetParent(HighlightsFX.field_Private_Static_HighlightsFX_0.transform, worldPositionStays: false);
			headlight = gameObject.AddComponent<Light>();
			headlight.type = LightType.Spot;
			headlight.enabled = false;
			flashlightParent = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
			UnityEngine.Object.DontDestroyOnLoad(flashlightParent);
			flashlightParent.name = "emmVRCFlashlightParent";
			flashlightParent.transform.localScale = new Vector3(0.05f, 0.125f, 0.05f);
			Renderer component = flashlightParent.GetComponent<Renderer>();
			component.material.color = ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex);
			flashlightParentMaterial = component.material;
			flashlightParent.GetComponent<Collider>().isTrigger = true;
			flashlightParent.AddComponent<Rigidbody>().isKinematic = true;
			flashlightParent.AddComponent<VRC_UdonPickupTrigger>().OnPickupUseUp = delegate
			{
				flashlight.enabled = !flashlight.enabled;
			};
			VRCPickup vRCPickup = flashlightParent.AddComponent<VRCPickup>();
			vRCPickup.AutoHold = VRC_Pickup.AutoHoldMode.Yes;
			vRCPickup.UseText = "Toggle Light";
			GameObject gameObject2 = new GameObject("Light");
			gameObject2.transform.SetParent(flashlightParent.transform, worldPositionStays: false);
			gameObject2.transform.Rotate(90f, 0f, 0f);
			flashlight = gameObject2.AddComponent<Light>();
			flashlight.type = LightType.Spot;
			GameObject gameObject3 = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
			gameObject3.transform.SetParent(flashlightParent.transform, worldPositionStays: false);
			gameObject3.transform.localPosition = new Vector3(0f, -0.75f, 0f);
			gameObject3.transform.localScale = new Vector3(1.5f, 0.25f, 1.5f);
			flashlightParent.transform.Rotate(90f, 0f, 0f);
			gameObject3.GetComponent<Renderer>().material = flashlightParentMaterial;
			gameObject3.GetComponent<Collider>().isTrigger = true;
			flashlightParent.SetActive(value: false);
		}

		public static void SetFlashlightActive(bool active)
		{
			flashlightParent.transform.position = HighlightsFX.field_Private_Static_HighlightsFX_0.transform.position + HighlightsFX.field_Private_Static_HighlightsFX_0.transform.forward;
			flashlightParent.transform.rotation = Quaternion.LookRotation(HighlightsFX.field_Private_Static_HighlightsFX_0.transform.up);
			IsFlashlightEnabled = active;
			flashlightParent.SetActive(active);
			flashlight.enabled = active;
			flashlight.color = ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex);
			flashlight.range = Configuration.JSONConfig.FlashlightRange;
			flashlight.intensity = Configuration.JSONConfig.FlashlightPower;
			flashlight.spotAngle = Configuration.JSONConfig.FlashlightAngle;
		}

		public static void SetHeadlightActive(bool active)
		{
			IsHeadlightEnabled = active;
			headlight.enabled = active;
			headlight.color = ColorConversion.HexToColor(Configuration.JSONConfig.FlashlightColorHex);
			headlight.range = Configuration.JSONConfig.FlashlightRange;
			headlight.intensity = Configuration.JSONConfig.FlashlightPower;
			headlight.spotAngle = Configuration.JSONConfig.FlashlightAngle;
		}
	}
}
