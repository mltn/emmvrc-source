using emmVRC.Hacks;
using UnityEngine;
using VRC.SDKBase;

namespace emmVRC.Functions.WorldHacks
{
	public class WorldESP
	{
		internal static void ToggleItemESP(bool toggle)
		{
			VRC_Pickup[] pickup_stored = ComponentToggle.Pickup_stored;
			for (int i = 0; i < pickup_stored.Length; i++)
			{
				Renderer component = pickup_stored[i].GetComponent<Renderer>();
				if (component != null)
				{
					HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(component, toggle);
				}
			}
		}

		internal static void ToggleTriggerESP(bool toggle)
		{
			VRC_Trigger[] trigger_stored = ComponentToggle.Trigger_stored;
			for (int i = 0; i < trigger_stored.Length; i++)
			{
				Renderer component = trigger_stored[i].GetComponent<Renderer>();
				if (component != null)
				{
					HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(component, toggle);
				}
			}
		}
	}
}
