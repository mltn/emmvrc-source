using System;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;

namespace emmVRC.Functions.Other
{
	public class AnniversaryMode : MelonLoaderEvents
	{
		public override void OnApplicationStart()
		{
			if (Environment.CommandLine.Contains("--emmvrc.anniversarymode") || (DateTime.Now.Day == 6 && DateTime.Now.Month == 5))
			{
				Logger.Log("Hello world!");
				Logger.Log("This is the beginning of a new beginning!");
				Logger.Log("Wait... have I said that before?");
			}
		}
	}
}
