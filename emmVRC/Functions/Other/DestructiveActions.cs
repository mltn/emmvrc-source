using System;
using System.Diagnostics;
using System.Threading;
using emmVRC.Network;
using UnityEngine;

namespace emmVRC.Functions.Other
{
	public class DestructiveActions
	{
		public static void ForceQuit()
		{
			NetworkClient.Logout();
			Thread thread = new Thread(QuitAfterQuit);
			thread.IsBackground = true;
			thread.Name = "emmVRC Quit Thread";
			thread.Start();
		}

		public static void ForceRestart()
		{
			if (NetworkClient.webToken != null)
			{
				HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
			}
			Thread thread = new Thread(RestartAfterQuit);
			thread.IsBackground = true;
			thread.Name = "emmVRC Restart Thread";
			thread.Start();
		}

		public static void RestartAfterQuit()
		{
			Application.Quit();
			Thread.Sleep(2500);
			try
			{
				Process.Start(Environment.CurrentDirectory + "\\VRChat.exe", Environment.CommandLine.ToString());
			}
			catch (Exception)
			{
				new Exception();
			}
			Process.GetCurrentProcess().Kill();
		}

		public static void QuitAfterQuit()
		{
			Application.Quit();
			Thread.Sleep(2500);
			Process.GetCurrentProcess().Kill();
		}
	}
}
