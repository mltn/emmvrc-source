using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using emmVRCLoader;
using VRC;

namespace emmVRC.Functions.Other
{
	public class PlayerJoinLeaveMessages : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			NetworkEvents.OnPlayerJoined += delegate(Player plr)
			{
				if (Configuration.JSONConfig.LogPlayerJoin)
				{
					Logger.Log("Player joined: " + plr.prop_APIUser_0.GetName());
				}
			};
			NetworkEvents.OnPlayerLeft += delegate(Player plr)
			{
				if (Configuration.JSONConfig.LogPlayerJoin)
				{
					Logger.Log("Player left: " + plr.prop_APIUser_0.GetName());
				}
			};
		}
	}
}
