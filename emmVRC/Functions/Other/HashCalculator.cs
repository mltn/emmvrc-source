using System;
using System.IO;
using System.Security.Cryptography;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;

namespace emmVRC.Functions.Other
{
	[Priority(0)]
	public class HashCalculator : MelonLoaderEvents
	{
		public override void OnApplicationStart()
		{
			MD5 mD = MD5.Create();
			FileStream inputStream = File.OpenRead(Path.Combine(Environment.CurrentDirectory, "Dependencies/emmVRC.dll"));
			string text = BitConverter.ToString(mD.ComputeHash(inputStream)).Replace("-", "").ToLower();
			Logger.Log("emmVRC module: " + text);
		}
	}
}
