using System;
using System.Collections.Generic;
using System.IO;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRCLoader;

namespace emmVRC.Functions.Other
{
	public class AlarmClock : MelonLoaderEvents, IWithFixedUpdate
	{
		public static readonly string alarmsFilePath = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/alarms.json");

		private static long lastInstanceTime = long.MaxValue;

		private static long lastCurrentTime = long.MaxValue;

		public static List<Alarm> Alarms { get; private set; }

		public static event Action<Alarm> OnAlarmTrigger;

		public override void OnApplicationStart()
		{
			if (!File.Exists(alarmsFilePath))
			{
				PortOldConfig();
			}
			else
			{
				LoadConfig();
			}
		}

		public void OnFixedUpdate()
		{
			long ticks = DateTime.Now.TimeOfDay.Ticks;
			long ticks2 = TimeSpan.FromSeconds(RoomManager.prop_Single_0).Ticks;
			for (int i = 0; i < Alarms.Count; i++)
			{
				if (Alarms[i].IsEnabled && ((!Alarms[i].IsSystemTime) ? (ticks2 > Alarms[i].Time && lastInstanceTime < Alarms[i].Time) : (ticks > Alarms[i].Time && lastCurrentTime < Alarms[i].Time)))
				{
					if (!Alarms[i].Repeats)
					{
						Alarms[i].IsEnabled = false;
						SaveConfig();
					}
					AlarmClock.OnAlarmTrigger?.Invoke(Alarms[i]);
				}
			}
			lastCurrentTime = ticks;
			lastInstanceTime = ticks2;
		}

		private static void PortOldConfig()
		{
			Alarms = new List<Alarm>();
			SaveConfig();
		}

		private static void LoadConfig()
		{
			Alarms = Decoder.Decode(File.ReadAllText(alarmsFilePath)).Make<List<Alarm>>();
		}

		public static void SaveConfig()
		{
			Logger.LogDebug("Saving alarms");
			File.WriteAllText(alarmsFilePath, Encoder.Encode(Alarms, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
		}
	}
}
