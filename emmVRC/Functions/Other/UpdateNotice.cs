using emmVRC.Functions.Core;
using emmVRC.Managers;
using emmVRC.Menus;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;

namespace emmVRC.Functions.Other
{
	public class UpdateNotice : MelonLoaderEvents
	{
		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			if (buildIndex == -1 && Configuration.JSONConfig.LastVersion != Attributes.Version.ToString(3))
			{
				Configuration.WriteConfigOption("LastVersion", Attributes.Version.ToString(3));
				emmVRCNotificationsManager.AddNotification(new Notification("Update Applied", Resources.alertSprite, "emmVRC has updated to version " + Attributes.Version.ToString(3) + "!", canIgnore: true, showAcceptButton: true, delegate
				{
					ChangelogMenu.changelogPage.OpenMenu();
				}, "View Changelog", "Opens the changelog for this release of emmVRC", showIgnoreButton: true, null, "Dismiss"));
			}
		}
	}
}
