using System;
using System.Collections;
using System.Collections.Generic;
using emmVRC.Components;
using emmVRC.Functions.PlayerHacks;
using emmVRC.Managers;
using MelonLoader;
using UIExpansionKit.API;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Functions.ModCompatibility
{
	public class UIExpansionKitIntegration
	{
		private static GameObject flightButton;

		private static GameObject noclipButton;

		private static GameObject speedButton;

		private static GameObject espButton;

		public static void Initialize()
		{
			ExpansionKitApi.GetExpandedMenu((ExpandedMenu)0).AddToggleButton("Flight", (Action<bool>)delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (!val && Flight.IsNoClipEnabled)
					{
						Flight.SetNoClipActive(active: false);
					}
					Flight.SetFlyActive(val);
				}
			}, (Func<bool>)GetFlightStatus, (Action<GameObject>)delegate(GameObject obj)
			{
				flightButton = obj;
				flightButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
				flightButton.AddComponent<EnableDisableListener>().OnEnabled += delegate
				{
					flightButton.GetComponent<Toggle>().isOn = Flight.IsFlyEnabled;
					noclipButton.GetComponent<Toggle>().isOn = Flight.IsNoClipEnabled;
					speedButton.GetComponent<Toggle>().isOn = Speed.IsEnabled;
					espButton.GetComponent<Toggle>().isOn = ESP.IsEnabled;
				};
			});
			ExpansionKitApi.GetExpandedMenu((ExpandedMenu)0).AddToggleButton("Noclip", (Action<bool>)delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					if (val && !Flight.IsFlyEnabled)
					{
						Flight.SetFlyActive(active: true);
					}
					Flight.SetNoClipActive(val);
				}
			}, (Func<bool>)GetNoclipStatus, (Action<GameObject>)delegate(GameObject obj)
			{
				noclipButton = obj;
				noclipButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
			});
			ExpansionKitApi.GetExpandedMenu((ExpandedMenu)0).AddToggleButton("Speed", (Action<bool>)delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					Speed.SetActive(val);
				}
			}, (Func<bool>)GetSpeedStatus, (Action<GameObject>)delegate(GameObject obj)
			{
				speedButton = obj;
				speedButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
			});
			ExpansionKitApi.GetExpandedMenu((ExpandedMenu)0).AddToggleButton("ESP", (Action<bool>)delegate(bool val)
			{
				if (RiskyFunctionsManager.AreRiskyFunctionsAllowed && Configuration.JSONConfig.RiskyFunctionsEnabled)
				{
					ESP.SetActive(val);
				}
			}, (Func<bool>)GetESPStatus, (Action<GameObject>)delegate(GameObject obj)
			{
				espButton = obj;
				espButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
			});
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIColorChangingEnabled", delegate
			{
				if (Configuration.JSONConfig.UIExpansionKitColorChangingEnabled)
				{
					MelonCoroutines.Start(ColorUIExpansionKit());
				}
			}));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIColorHex", delegate
			{
				if (Configuration.JSONConfig.UIExpansionKitColorChangingEnabled)
				{
					MelonCoroutines.Start(ColorUIExpansionKit());
				}
			}));
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIExpansionKitColorChangingEnabled", delegate
			{
				if (Configuration.JSONConfig.UIExpansionKitColorChangingEnabled)
				{
					MelonCoroutines.Start(ColorUIExpansionKit());
				}
			}));
			if (Configuration.JSONConfig.UIExpansionKitColorChangingEnabled)
			{
				ExpansionKitApi.RegisterWaitConditionBeforeDecorating(ColorUIExpansionKit());
			}
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("UIExpansionKitIntegration", delegate
			{
				flightButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
				noclipButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
				speedButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
				espButton.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
			}));
		}

		public static IEnumerator ColorUIExpansionKit()
		{
			yield return null;
			Color color = (Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : Configuration.defaultMenuColor());
			ColorBlock colorBlock = default(ColorBlock);
			colorBlock.colorMultiplier = 1f;
			colorBlock.disabledColor = Color.grey;
			colorBlock.highlightedColor = new Color(color.r * 1.5f, color.g * 1.5f, color.b * 1.5f);
			colorBlock.normalColor = color;
			colorBlock.pressedColor = Color.gray;
			colorBlock.fadeDuration = 0.1f;
			colorBlock.selectedColor = color;
			ColorBlock colors = colorBlock;
			Transform transform = ExpansionKitApi.GetUiExpansionKitBundleContents().StoredThingsParent.transform;
			foreach (Image componentsInChild in transform.GetComponentsInChildren<Image>(includeInactive: true))
			{
				if (componentsInChild.transform.parent.name != "PinToggle" && componentsInChild.transform.parent.parent.name != "PinToggle" && componentsInChild.transform.name != "Checkmark")
				{
					componentsInChild.color = new Color(color.r * 0.5f, color.g * 0.5f, color.b * 0.5f);
				}
			}
			foreach (Button componentsInChild2 in transform.GetComponentsInChildren<Button>(includeInactive: true))
			{
				componentsInChild2.colors = colors;
			}
			foreach (Toggle componentsInChild3 in transform.GetComponentsInChildren<Toggle>(includeInactive: true))
			{
				if (componentsInChild3.gameObject.name != "PinToggle")
				{
					componentsInChild3.colors = colors;
				}
			}
		}

		private static bool GetFlightStatus()
		{
			return Flight.IsFlyEnabled;
		}

		private static bool GetNoclipStatus()
		{
			return Flight.IsNoClipEnabled;
		}

		private static bool GetSpeedStatus()
		{
			return Speed.IsEnabled;
		}

		private static bool GetESPStatus()
		{
			return ESP.IsEnabled;
		}
	}
}
