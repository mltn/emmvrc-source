using emmVRC.Objects.ModuleBases;
using UnityEngine;

namespace emmVRC.Functions.View
{
	public class FOV : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			if (Configuration.JSONConfig.CustomFOV == 60)
			{
				return;
			}
			GameObject gameObject = GameObject.Find("Camera (eye)");
			if (gameObject == null)
			{
				gameObject = GameObject.Find("CenterEyeAnchor");
			}
			if (gameObject != null)
			{
				Camera component = gameObject.GetComponent<Camera>();
				if (component != null)
				{
					component.fieldOfView = Configuration.JSONConfig.CustomFOV;
				}
			}
		}
	}
}
