using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.Objects.ModuleBases;
using emmVRC.TinyJSON;
using emmVRC.Utils;
using emmVRCLoader;
using Il2CppSystem;
using Il2CppSystem.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using VRC.Core;

namespace emmVRC.Network
{
	[Priority(0)]
	public class NetworkClient : MelonLoaderEvents
	{
		private const string BaseAddress = "https://api.emmvrc.com";

		private static int Port = 443;

		public const string configURL = "https://dl.emmvrc.com";

		private static string LoginKey;

		private static string LoginToken;

		private static string _webToken;

		private static bool keyFileTried = false;

		private static bool passwordTried = false;

		public static int retries = 0;

		private static string message = "To those interested; this class is very much temporary. The entire network is going to be rewritten at some point soon.";

		public static string baseURL => "https://api.emmvrc.com:" + Port;

		public static string webToken
		{
			get
			{
				return _webToken;
			}
			set
			{
				_webToken = value;
				httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _webToken);
			}
		}

		public static HttpClient httpClient { get; set; }

		internal static event Action onLogin;

		internal static event Action onLogout;

		public override void OnUiManagerInit()
		{
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("emmVRCNetworkEnabled", delegate
			{
				if (Configuration.JSONConfig.emmVRCNetworkEnabled)
				{
					ClearAndLogin();
				}
				else
				{
					Logout();
				}
			}));
			httpClient = new HttpClient();
			httpClient.DefaultRequestHeaders.Accept.Clear();
			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("emmVRC/1.0 (Client; emmVRCClient/" + Attributes.Version?.ToString() + ", Headset; " + (XRDevice.isPresent ? XRDevice.model : "None") + ")");
			if (Configuration.JSONConfig.emmVRCNetworkEnabled)
			{
				ClearAndLogin();
			}
		}

		public static void ClearAndLogin()
		{
			retries = 0;
			fetchConfig().NoAwait("fetchConfig");
			login().NoAwait("login");
		}

		public static void Logout()
		{
			if (webToken != null)
			{
				HTTPRequest.get(baseURL + "/api/authentication/logout").NoAwait("Logout");
			}
			webToken = null;
			NetworkClient.onLogout?.DelegateSafeInvoke();
		}

		public static T DestroyClient<T>(Func<T> callback = null)
		{
			httpClient = null;
			webToken = null;
			return callback();
		}

		private static async Task fetchConfig()
		{
			while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
			{
				await emmVRC.AwaitUpdate.Yield();
			}
			await getConfigAsync();
		}

		private static async Task login(string password = "")
		{
			while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
			{
				await emmVRC.AwaitUpdate.Yield();
			}
			await sendRequest(password);
		}

		public static void PromptLogin()
		{
			OpenPasswordPrompt();
		}

		private static void OpenPasswordPrompt()
		{
			VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Please enter your pin", "", InputField.InputType.Standard, keypad: true, "Login", Action<string, List<KeyCode>, Text>.op_Implicit((Action<string, List<KeyCode>, Text>)delegate(string password, List<KeyCode> keyk, Text tx)
			{
				passwordTried = false;
				login(password).NoAwait("login");
			}), null, "Enter pin....");
		}

		private static async Task getConfigAsync()
		{
			_ = 1;
			try
			{
				NetworkConfig.Instance = Decoder.Decode(await HTTPRequest.get("https://dl.emmvrc.com/configuration.php")).Make<NetworkConfig>();
				await emmVRC.AwaitUpdate.Yield();
				if (NetworkConfig.Instance.MessageID != -1 && Configuration.JSONConfig.LastSeenStartupMessage != NetworkConfig.Instance.MessageID)
				{
					emmVRCNotificationsManager.AddNotification(new Notification("emmVRC Network Notice", global::emmVRC.Functions.Core.Resources.messageSprite, NetworkConfig.Instance.StartupMessage, canIgnore: true, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
				}
			}
			catch (Exception ex)
			{
				emmVRCLoader.Logger.LogError("Client configuration could not be fetched from emmVRC. Assuming default values. Error: " + ex);
				NetworkConfig.Instance = new NetworkConfig();
			}
		}

		private static async Task sendRequest(string password = "")
		{
			APIUser currentUser = APIUser.CurrentUser;
			bool emaExists = Authentication.Exists(currentUser.id);
			if (NetworkConfig.Instance.DeleteAndDisableAuthFile)
			{
				Authentication.DeleteTokenFile(currentUser.id);
			}
			else if (string.IsNullOrWhiteSpace(password) && emaExists)
			{
				LoginToken = Authentication.ReadTokenFile(currentUser.id);
				if (password == "")
				{
					keyFileTried = true;
				}
			}
			if (LoginToken == null)
			{
				LoginToken = "";
			}
			LoginKey = password;
			string createFile = (emaExists ? "0" : "1");
			string response2 = "undefined";
			try
			{
				response2 = await HTTPRequest.post(baseURL + "/api/authentication/login", new Dictionary<string, string>
				{
					["username"] = currentUser.id,
					["name"] = currentUser.GetName(),
					["password"] = LoginKey,
					["loginToken"] = (string.IsNullOrWhiteSpace(password) ? LoginToken : ""),
					["loginKey"] = createFile
				});
				Variant result = HTTPResponse.Serialize(response2);
				webToken = result["token"];
				await emmVRC.AwaitUpdate.Yield();
				if (createFile == "1")
				{
					try
					{
						Authentication.CreateTokenFile(currentUser.id, result["loginKey"]);
					}
					catch (Exception arg)
					{
						emmVRCLoader.Logger.Log($"There was an issue saving your emmVRC Network token.\n{arg}");
					}
				}
				keyFileTried = false;
				passwordTried = false;
				NetworkClient.onLogin?.DelegateSafeInvoke();
			}
			catch (Exception)
			{
				await emmVRC.AwaitUpdate.Yield();
				response2 = response2.ToLower();
				if (response2.Contains("unauthorized"))
				{
					if (response2.Contains("banned"))
					{
						emmVRCNotificationsManager.AddNotification(new Notification("emmVRC", global::emmVRC.Functions.Core.Resources.errorSprite, "You cannot connect to the emmVRC Network because you are banned.", canIgnore: false, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
						return;
					}
					if (keyFileTried && emaExists)
					{
						Authentication.DeleteTokenFile(currentUser.id);
					}
					if (keyFileTried && password != "" && !passwordTried && !NetworkConfig.Instance.DisableAuthFile && !NetworkConfig.Instance.DeleteAndDisableAuthFile)
					{
						passwordTried = true;
						await sendRequest(password);
					}
					else
					{
						await emmVRC.AwaitUpdate.Yield();
						emmVRCNotificationsManager.AddNotification(new Notification("emmVRC", global::emmVRC.Functions.Core.Resources.alertSprite, "You need to log in to the emmVRC Network. Please log in or enter a pin to create one. If you have forgotten your pin, or are experiencing issues, please contact us in the emmVRC Discord.", canIgnore: false, showAcceptButton: true, PromptLogin, "Login", "", showIgnoreButton: true, null, "Dismiss"));
					}
				}
				else if (response2.ToLower().Contains("forbidden"))
				{
					emmVRCNotificationsManager.AddNotification(new Notification("emmVRC", global::emmVRC.Functions.Core.Resources.errorSprite, "You have tried to log in too many times. Please try again later.", canIgnore: false, showAcceptButton: false, null, "", "", showIgnoreButton: true, null, "Dismiss"));
				}
				else if (retries++ <= NetworkConfig.Instance.NetworkAutoRetries)
				{
					await sendRequest(password);
				}
				else
				{
					emmVRCNotificationsManager.AddNotification(new Notification("emmVRC", global::emmVRC.Functions.Core.Resources.errorSprite, "The emmVRC Network is currently unavailable. Please try again later.", canIgnore: false, showAcceptButton: true, ClearAndLogin, "Reconnect", "", showIgnoreButton: true, null, "Dismiss"));
				}
			}
		}
	}
}
