using emmVRC.TinyJSON;

namespace emmVRC.Network.Objects
{
	public static class SerializedObjectExtensions
	{
		public static string Encode(this SerializedObject @object)
		{
			return Encoder.Encode(@object);
		}

		public static Variant Decode(string str)
		{
			return Decoder.Decode(str);
		}
	}
}
