using emmVRC.TinyJSON;

namespace emmVRC.Network
{
	public class HTTPResponse
	{
		public static Variant Serialize(string httpContent)
		{
			return Decoder.Decode(httpContent);
		}

		public static string Deserialize(Variant obj)
		{
			return Encoder.Encode(obj);
		}
	}
}
