using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using emmVRC.TinyJSON;
using emmVRCLoader;

namespace emmVRC.Network
{
	public class HTTPRequest
	{
		public static Task<string> get(string url)
		{
			return request(HttpMethod.Get, url);
		}

		public static Task<string> post(string url, object obj)
		{
			return request(HttpMethod.Post, url, obj);
		}

		public static Task<string> put(string url, object obj)
		{
			return request(HttpMethod.Put, url, obj);
		}

		public static Task<string> patch(string url, object obj)
		{
			return request(new HttpMethod("PATCH"), url, obj);
		}

		public static Task<string> delete(string url, object obj)
		{
			return request(HttpMethod.Delete, url, obj);
		}

		private static async Task<string> request(HttpMethod method, string url, object obj = null)
		{
			_ = 1;
			try
			{
				HttpRequestMessage httpRequestMessage = new HttpRequestMessage(method, url);
				if (obj != null)
				{
					string content = global::emmVRC.TinyJSON.Encoder.Encode(obj);
					httpRequestMessage.Content = new StringContent(content, Encoding.UTF8, "application/json");
				}
				using HttpResponseMessage responseMessage = await NetworkClient.httpClient.SendAsync(httpRequestMessage);
				if (responseMessage.IsSuccessStatusCode)
				{
					return await responseMessage.Content.ReadAsStringAsync();
				}
				Logger.LogDebug(responseMessage.ReasonPhrase);
				return responseMessage.ReasonPhrase;
			}
			catch (Exception ex)
			{
				return ex.ToString();
			}
		}
	}
}
