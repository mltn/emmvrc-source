using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using emmVRCLoader;
using UnityEngine;

namespace emmVRC
{
	public class Configuration
	{
		public static List<KeyValuePair<string, Action>> onConfigUpdated;

		public static Config JSONConfig { get; private set; }

		public static void Initialize()
		{
			onConfigUpdated = new List<KeyValuePair<string, Action>>();
			Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC"));
			if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json")))
			{
				try
				{
					JSONConfig = Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"))).Make<Config>();
					if (JSONConfig == null)
					{
						emmVRCLoader.Logger.LogError("An error occured while parsing the config file. It has been moved to config.old.json, and a new one has been created.");
						emmVRCLoader.Logger.LogError("Error: The parsed config was null...");
						File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
						JSONConfig = new Config();
					}
				}
				catch (Exception ex)
				{
					emmVRCLoader.Logger.LogError("An error occured while parsing the config file. It has been moved to config.old.json, and a new one has been created.");
					emmVRCLoader.Logger.LogError("Error: " + ex.ToString());
					if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json")))
					{
						File.Delete(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
					}
					File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
					JSONConfig = new Config();
				}
			}
			else
			{
				JSONConfig = new Config();
			}
		}

		public static void WriteConfigOption(string configOptionName, object newValue)
		{
			if (!typeof(Config).GetFields().Any((FieldInfo a) => a.Name == configOptionName))
			{
				emmVRCLoader.Logger.LogError("Invalid configuration option specified: " + configOptionName);
				return;
			}
			typeof(Config).GetField(configOptionName).SetValue(JSONConfig, newValue);
			SaveConfig();
			foreach (KeyValuePair<string, Action> item in onConfigUpdated)
			{
				if (item.Key == configOptionName)
				{
					item.Value();
				}
			}
		}

		public static void WipeConfig()
		{
			JSONConfig = new Config
			{
				WelcomeMessageShown = true
			};
			SaveConfig();
			foreach (KeyValuePair<string, Action> item in onConfigUpdated)
			{
				item.Value();
			}
		}

		private static void SaveConfig()
		{
			File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Encoder.Encode(JSONConfig, EncodeOptions.PrettyPrint));
		}

		public static Color menuColor()
		{
			return ColorConversion.HexToColor(JSONConfig.UIColorHex);
		}

		public static Color defaultMenuColor()
		{
			return new Color(0.05f, 0.65f, 0.68f);
		}
	}
}
