using System;
using System.Collections.Generic;
using emmVRC.Functions.Core;
using emmVRC.Libraries;
using emmVRC.Objects.ModuleBases;
using emmVRC.Utils;
using UnityEngine;
using UnityEngine.UI;
using VRC;
using VRC.Core;

namespace emmVRC.Hacks
{
	public class MasterCrown : MelonLoaderEvents
	{
		private static GameObject masterIconObj;

		public static Sprite crownSprite;

		public override void OnUiManagerInit()
		{
			NetworkEvents.OnPlayerJoined += delegate(Player plr)
			{
				if (plr.prop_APIUser_0 != null && plr.prop_APIUser_0.id != APIUser.CurrentUser.id && plr.prop_VRCPlayerApi_0 != null && plr.prop_VRCPlayerApi_0.isMaster && Configuration.JSONConfig.MasterIconEnabled)
				{
					InstantiateIcon(plr);
				}
			};
			NetworkEvents.OnPlayerLeft += delegate
			{
				global::emmVRC.Libraries.PlayerUtils.GetEachPlayer(delegate(Player player)
				{
					if (player.prop_VRCPlayerApi_0.isMaster && player.prop_APIUser_0.id != APIUser.CurrentUser.id && Configuration.JSONConfig.MasterIconEnabled)
					{
						InstantiateIcon(player);
					}
				});
			};
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("MasterIconEnabled", delegate
			{
				global::emmVRC.Libraries.PlayerUtils.GetEachPlayer(delegate(Player player)
				{
					if (player.prop_VRCPlayerApi_0.isMaster && player.prop_APIUser_0.id != APIUser.CurrentUser.id && Configuration.JSONConfig.MasterIconEnabled)
					{
						InstantiateIcon(player);
					}
				});
				if (!Configuration.JSONConfig.MasterIconEnabled && masterIconObj != null)
				{
					UnityEngine.Object.DestroyImmediate(masterIconObj);
				}
			}));
		}

		public static void InstantiateIcon(Player plr)
		{
			if (masterIconObj != null)
			{
				UnityEngine.Object.DestroyImmediate(masterIconObj);
			}
			GameObject gameObject = plr._vrcplayer.field_Private_Transform_0.parent.transform.Find("Player Nameplate/Canvas/Nameplate/Contents/Friend Marker").gameObject;
			masterIconObj = UnityEngine.Object.Instantiate(gameObject, gameObject.transform.parent);
			masterIconObj.GetComponent<RectTransform>().anchoredPosition += new Vector2(256f, 24f);
			masterIconObj.GetComponent<Image>().sprite = global::emmVRC.Functions.Core.Resources.crownSprite;
			masterIconObj.SetActive(value: true);
		}
	}
}
