using emmVRC.Objects.ModuleBases;
using UnityEngine;

namespace emmVRC.Hacks
{
	public class FPS : MelonLoaderEvents
	{
		public override void OnUiManagerInit()
		{
			if (Configuration.JSONConfig.UnlimitedFPSEnabled)
			{
				Application.targetFrameRate = Configuration.JSONConfig.FPSLimit;
			}
		}
	}
}
