using System.Collections.Generic;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
	public class MirrorTweaks : MelonLoaderEvents
	{
		public static List<OriginalMirror> originalMirrors = new List<OriginalMirror>();

		private static LayerMask optimizeMask = new LayerMask
		{
			value = 263680
		};

		private static LayerMask beautifyMask = new LayerMask
		{
			value = -1025
		};

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			originalMirrors = new List<OriginalMirror>();
			foreach (VRC_MirrorReflection item in Resources.FindObjectsOfTypeAll<VRC_MirrorReflection>())
			{
				originalMirrors.Add(new OriginalMirror
				{
					MirrorParent = item,
					OriginalLayers = item.m_ReflectLayers
				});
			}
		}

		public static void Optimize()
		{
			if (originalMirrors.Count == 0)
			{
				return;
			}
			foreach (OriginalMirror originalMirror in originalMirrors)
			{
				originalMirror.MirrorParent.m_ReflectLayers = optimizeMask;
			}
		}

		public static void Beautify()
		{
			if (originalMirrors.Count == 0)
			{
				return;
			}
			foreach (OriginalMirror originalMirror in originalMirrors)
			{
				originalMirror.MirrorParent.m_ReflectLayers = beautifyMask;
			}
		}

		public static void Revert()
		{
			if (originalMirrors.Count == 0)
			{
				return;
			}
			foreach (OriginalMirror originalMirror in originalMirrors)
			{
				originalMirror.MirrorParent.m_ReflectLayers = originalMirror.OriginalLayers;
			}
		}
	}
}
