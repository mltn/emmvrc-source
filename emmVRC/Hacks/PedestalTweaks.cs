using System.Collections.Generic;
using emmVRC.Objects.ModuleBases;
using UnityEngine;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
	public class PedestalTweaks : MelonLoaderEvents
	{
		public static List<OriginalPedestal> originalPedestals;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			originalPedestals = new List<OriginalPedestal>();
			foreach (VRC_AvatarPedestal item in Resources.FindObjectsOfTypeAll<VRC_AvatarPedestal>())
			{
				originalPedestals.Add(new OriginalPedestal
				{
					PedestalParent = item.gameObject,
					originalActiveStatus = item.gameObject.activeSelf
				});
			}
		}

		public static void Disable()
		{
			if (originalPedestals.Count == 0)
			{
				return;
			}
			foreach (OriginalPedestal originalPedestal in originalPedestals)
			{
				originalPedestal.PedestalParent.SetActive(value: false);
			}
		}

		public static void Enable()
		{
			if (originalPedestals.Count == 0)
			{
				return;
			}
			foreach (OriginalPedestal originalPedestal in originalPedestals)
			{
				originalPedestal.PedestalParent.SetActive(value: true);
			}
		}

		public static void Revert()
		{
			if (originalPedestals.Count == 0)
			{
				return;
			}
			foreach (OriginalPedestal originalPedestal in originalPedestals)
			{
				originalPedestal.PedestalParent.SetActive(originalPedestal.originalActiveStatus);
			}
		}
	}
}
