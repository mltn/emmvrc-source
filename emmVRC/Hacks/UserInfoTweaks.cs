using System;
using System.Collections;
using emmVRC.Components;
using emmVRC.Objects.ModuleBases;
using emmVRCLoader;
using MelonLoader;
using UnhollowerBaseLib.Attributes;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;
using VRC.UI;

namespace emmVRC.Hacks
{
	public class UserInfoTweaks : MelonLoaderEvents
	{
		public static GameObject lastSeenText;

		public static PageUserInfo userInfo;

		public static string lastCheckedId;

		public override void OnUiManagerInit()
		{
			GameObject gameObject = GameObject.Find("UserInterface/MenuContent/Screens/UserInfo");
			lastSeenText = UnityEngine.Object.Instantiate(gameObject.transform.Find("User Panel/NameText").gameObject, gameObject.transform);
			lastSeenText.GetComponent<RectTransform>().anchoredPosition = new Vector2(975f, -795f);
			lastSeenText.GetComponent<Text>().alignment = TextAnchor.MiddleRight;
			lastSeenText.GetComponent<Text>().fontSize = 30;
			lastSeenText.GetComponent<Text>().text = "";
			userInfo = gameObject.transform.GetComponent<PageUserInfo>();
			gameObject.AddComponent<EnableDisableListener>().OnEnabled += delegate
			{
				MelonCoroutines.Start(WaitForUserReady());
			};
		}

		[HideFromIl2Cpp]
		public static IEnumerator WaitForUserReady()
		{
			while (userInfo.get_field_Private_APIUser_0() == null || userInfo.get_field_Private_APIUser_0().id == lastCheckedId)
			{
				yield return new WaitForEndOfFrame();
			}
			if (!userInfo.gameObject.activeInHierarchy)
			{
				yield break;
			}
			try
			{
				lastCheckedId = userInfo.get_field_Private_APIUser_0().id;
				if (!(lastSeenText != null))
				{
					yield break;
				}
				try
				{
					if (userInfo.get_field_Private_APIUser_0().statusValue != APIUser.UserStatus.Offline || !userInfo.get_field_Private_APIUser_0().isFriend)
					{
						lastSeenText.GetComponent<Text>().text = "";
						yield break;
					}
					DateTime dateTime = DateTime.Parse(userInfo.get_field_Private_APIUser_0().last_login);
					lastSeenText.GetComponent<Text>().text = "Last login: " + dateTime.ToString();
				}
				catch (Exception ex)
				{
					emmVRCLoader.Logger.LogError("Error parsing last login: " + ex.ToString());
				}
			}
			catch (Exception ex2)
			{
				emmVRCLoader.Logger.LogError("Error while waiting for user: " + ex2.ToString());
			}
		}
	}
}
