using System;
using System.Collections;
using System.Collections.Generic;
using emmVRC.Objects.ModuleBases;
using MelonLoader;
using UnityEngine;

namespace emmVRC.Hacks
{
	public class ShortcutMenuButtons : MelonLoaderEvents
	{
		public static GameObject socialNotifications;

		public override void OnUiManagerInit()
		{
			MelonCoroutines.Start(Process());
			Configuration.onConfigUpdated.Add(new KeyValuePair<string, Action>("DisableOldInviteButtons", delegate
			{
				MelonCoroutines.Start(Process());
			}));
		}

		public static IEnumerator Process()
		{
			yield return null;
			socialNotifications = GameObject.Find("UserInterface").transform.Find("Canvas_QuickMenu(Clone)/Container/Window/QMNotificationsArea/Notifications/SocialNotificationsOverlay").gameObject;
			socialNotifications.transform.localScale = (Configuration.JSONConfig.DisableOldInviteButtons ? Vector3.zero : Vector3.one);
		}
	}
}
