using emmVRC.Objects.ModuleBases;
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.Video;
using VRC.SDK3.Components;
using VRC.SDKBase;
using VRCSDK2;

namespace emmVRC.Hacks
{
	public class ComponentToggle : MelonLoaderEvents
	{
		internal static VRC_SyncVideoPlayer[] Video_stored_sdk2;

		internal static SyncVideoPlayer[] Video_stored_sdk3;

		internal static MediaPlayer[] Video_stored_sdk3_2;

		internal static VideoPlayer[] Video_stored_sdk3_3;

		public static bool videoplayers = true;

		internal static VRC.SDKBase.VRC_Pickup[] Pickup_stored;

		public static bool pickupable = true;

		public static bool pickup_object = true;

		internal static VRC.SDKBase.VRC_Trigger[] Trigger_stored;

		public override void OnSceneWasLoaded(int buildIndex, string sceneName)
		{
			Store();
			Toggle();
		}

		private static void Store()
		{
			if (RoomManager.field_Internal_Static_ApiWorld_0 != null && Resources.FindObjectsOfTypeAll<VRCSceneDescriptor>().Count > 0)
			{
				Video_stored_sdk3 = Resources.FindObjectsOfTypeAll<SyncVideoPlayer>();
				Video_stored_sdk3_2 = Resources.FindObjectsOfTypeAll<MediaPlayer>();
				Video_stored_sdk3_3 = Resources.FindObjectsOfTypeAll<VideoPlayer>();
			}
			else
			{
				Video_stored_sdk2 = Resources.FindObjectsOfTypeAll<VRC_SyncVideoPlayer>();
			}
			Pickup_stored = Object.FindObjectsOfType<VRC.SDKBase.VRC_Pickup>();
			Trigger_stored = Object.FindObjectsOfType<VRC.SDKBase.VRC_Trigger>();
		}

		internal static void Toggle(bool tempOn = false)
		{
			if (RoomManager.field_Internal_Static_ApiWorld_0 != null && Resources.FindObjectsOfTypeAll<VRCSceneDescriptor>().Count > 0)
			{
				if (Video_stored_sdk3 == null || Video_stored_sdk3_2 == null || Video_stored_sdk3_3 == null)
				{
					Store();
				}
				SyncVideoPlayer[] video_stored_sdk = Video_stored_sdk3;
				foreach (SyncVideoPlayer syncVideoPlayer in video_stored_sdk)
				{
					if (tempOn)
					{
						syncVideoPlayer.GetComponent<SyncVideoPlayer>().enabled = true;
						syncVideoPlayer.gameObject.SetActive(value: true);
					}
					else
					{
						syncVideoPlayer.GetComponent<SyncVideoPlayer>().enabled = videoplayers;
						syncVideoPlayer.gameObject.SetActive(videoplayers);
					}
				}
				MediaPlayer[] video_stored_sdk3_ = Video_stored_sdk3_2;
				foreach (MediaPlayer mediaPlayer in video_stored_sdk3_)
				{
					if (tempOn)
					{
						mediaPlayer.GetComponent<MediaPlayer>().enabled = true;
						mediaPlayer.gameObject.SetActive(value: true);
					}
					else
					{
						mediaPlayer.GetComponent<MediaPlayer>().enabled = videoplayers;
						mediaPlayer.gameObject.SetActive(videoplayers);
					}
				}
				VideoPlayer[] video_stored_sdk3_2 = Video_stored_sdk3_3;
				foreach (VideoPlayer videoPlayer in video_stored_sdk3_2)
				{
					if (tempOn)
					{
						videoPlayer.GetComponent<VideoPlayer>().enabled = true;
						videoPlayer.gameObject.SetActive(value: true);
					}
					else
					{
						videoPlayer.GetComponent<VideoPlayer>().enabled = videoplayers;
						videoPlayer.gameObject.SetActive(videoplayers);
					}
				}
			}
			else
			{
				if (Video_stored_sdk2 == null)
				{
					Store();
				}
				VRC_SyncVideoPlayer[] video_stored_sdk2 = Video_stored_sdk2;
				foreach (VRC_SyncVideoPlayer obj in video_stored_sdk2)
				{
					obj.GetComponent<VRC_SyncVideoPlayer>().enabled = videoplayers;
					obj.gameObject.SetActive(videoplayers);
				}
			}
			if (Pickup_stored == null)
			{
				Store();
			}
			VRC.SDKBase.VRC_Pickup[] pickup_stored = Pickup_stored;
			foreach (VRC.SDKBase.VRC_Pickup vRC_Pickup in pickup_stored)
			{
				if (tempOn)
				{
					vRC_Pickup.GetComponent<VRC.SDKBase.VRC_Pickup>().pickupable = true;
					vRC_Pickup.gameObject.SetActive(value: true);
				}
				else
				{
					vRC_Pickup.GetComponent<VRC.SDKBase.VRC_Pickup>().pickupable = pickupable;
					vRC_Pickup.gameObject.SetActive(pickup_object);
				}
			}
		}
	}
}
